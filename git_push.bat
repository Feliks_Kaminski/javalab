echo off
set /p var=Enter a comment 
if "%var%"=="" (
	echo Empty
) else (
	echo on
	git add .
	git commit -m "%var%"
	git push git@bitbucket.org:Feliks_Kaminski/javalab.git master
)
pause