echo off
set /p var=Enter a comment 
if "%var%"=="" (
	echo Empty
) else (
	echo on
	git add .
	git commit -m "%var%"
)
pause