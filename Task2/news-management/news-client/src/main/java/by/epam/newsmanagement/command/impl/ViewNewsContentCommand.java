package by.epam.newsmanagement.command.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import by.epam.newsmanagement.command.CommandType;
import by.epam.newsmanagement.command.ICommand;
import by.epam.newsmanagement.entity.Comment;
import by.epam.newsmanagement.entity.News;
import by.epam.newsmanagement.entity.SearchCriteria;
import by.epam.newsmanagement.exceptions.ServiceException;
import by.epam.newsmanagement.service.IServiceAuthor;
import by.epam.newsmanagement.service.IServiceComment;
import by.epam.newsmanagement.service.IServiceExtended;
import by.epam.newsmanagement.service.IServiceNews;
import by.epam.newsmanagement.service.IServiceTag;
import static by.epam.newsmanagement.utils.ConstantsHolder.*;



/**
 * @author Felix
 * 
 * The class prepares information on single news for view;
 *
 */
public class ViewNewsContentCommand implements ICommand {

	IServiceAuthor authorService;
	IServiceTag tagService;
	IServiceNews newsService;
	IServiceComment commentService;
	IServiceExtended extendedService;
	
	{
		ClassPathXmlApplicationContext context = 
				new ClassPathXmlApplicationContext(SPRING_CONF);
		authorService = context.getBean(AUTHOR_SERVICE,IServiceAuthor.class);
		tagService = context.getBean(TAG_SERVICE,IServiceTag.class);
		newsService = context.getBean(NEWS_SERVICE,IServiceNews.class);
		commentService = context.getBean(COMMENT_SERVICE,IServiceComment.class);
		extendedService = context.getBean(EXTENDED_SERVICE,IServiceExtended.class);
		context.close();
	}
	
	@Override
	public String execute(HttpServletRequest req, HttpServletResponse resp) throws ServiceException {
		Long newsId = Long.parseLong(req.getParameter(NEWS_ID));
		
		int offset = 0;
		String offsetStr = req.getParameter(OFFSET);
		if (null!=offsetStr){
			offset = Integer.parseInt(offsetStr);
		}
		
		SearchCriteria sc = (SearchCriteria)req.getSession().getAttribute(SC);
		
		if (offset!=0){ //&&(null==sc)
			int newsCount = (int)newsService.getNewsByCriteriaCount(sc);
			int newsN = newsService.getNewsNumberByCriteria(sc, newsId);
			if (newsN>0){
				newsN += offset;
				if ((newsN>newsCount)||(newsN<0)) {
					return CommandType.VIEW_NEWS_LIST.getCommand().execute(req, resp);
				}
				newsId = newsService.getNewsIdsByCriteria(sc, newsN, newsN).get(0);
			}
		}
		
		News news = newsService.getNews(newsId);
		if (null!=news){
			req.setAttribute(NEWS, news);
			
			String authorName = authorService.getAuthor(newsService.getAuthorIdByNewsId(newsId)).getName();
			req.setAttribute(AUTHOR_NAME, authorName);
			
			List<Comment> comments = commentService.getCommentsListByNewsId(newsId, 1, 0);
			req.setAttribute(COMMENTS, comments);
			
			String error = req.getParameter(ERROR_POST_COMMENT);
			if (null!=error){
				req.setAttribute(COMMENT_TEXT, req.getParameter(COMMENT_TEXT));
				req.setAttribute(ERROR_POST_COMMENT, ERROR_POST_COMMENT);
			}
		}
		return NEWS_CONTENT;
	}

}
