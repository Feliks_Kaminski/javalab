package by.epam.newsmanagement.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;





import by.epam.newsmanagement.command.ICommand;
import by.epam.newsmanagement.exceptions.ServiceException;
import by.epam.newsmanagement.utils.CommandGetter;

//@WebServlet("/")
public class NewsServlet extends HttpServlet {
	
	private static final String PREFIX = "/WEB-INF/views/";
	private static final String SUFFIX = ".jsp";
	
	CommandGetter commandGetter;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		go(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		go(req, resp);
	}
	
	
	private void go(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException{
		ICommand command = CommandGetter.getCommand(req);
		
		RequestDispatcher requestDispatcher;
		//That block redirects to page if the result of a command is url, or sends to page if string doesn't contain /.
		try {
			String page = command.execute(req, resp);
			if (page.contains("/")){
				resp.sendRedirect(page);
			} else {
				requestDispatcher = req.getRequestDispatcher(PREFIX + page + SUFFIX);
				requestDispatcher.forward(req, resp);
			}
		} catch (ServiceException e) {
			throw new ServletException(e);
		}
	}
	
}
