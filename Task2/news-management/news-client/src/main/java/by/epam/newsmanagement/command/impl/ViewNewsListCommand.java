package by.epam.newsmanagement.command.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import by.epam.newsmanagement.command.ICommand;
import by.epam.newsmanagement.entity.Author;
import by.epam.newsmanagement.entity.NewsInfoHolder;
import by.epam.newsmanagement.entity.SearchCriteria;
import by.epam.newsmanagement.entity.Tag;
import by.epam.newsmanagement.exceptions.ServiceException;
import by.epam.newsmanagement.service.IServiceAuthor;
import by.epam.newsmanagement.service.IServiceExtended;
import by.epam.newsmanagement.service.IServiceNews;
import by.epam.newsmanagement.service.IServiceTag;
import static by.epam.newsmanagement.utils.ConstantsHolder.*;
import by.epam.newsmanagement.utils.PaginationHelper;
import by.epam.newsmanagement.utils.ParamHolder;



/**
 * @author Felix
 * 
 * The class prepares information on news list for view;
 *
 */
public class ViewNewsListCommand implements ICommand {
	
	private static final int newsOnPage = Integer.parseInt(new ParamHolder().getProperty(NEWS_ON_PAGE));
	
	IServiceAuthor authorService;
	IServiceTag tagService;
	IServiceNews newsService;
	IServiceExtended extendedService;
	
	{
		ClassPathXmlApplicationContext context = 
				new ClassPathXmlApplicationContext(SPRING_CONF);
		authorService = context.getBean(AUTHOR_SERVICE,IServiceAuthor.class);
		tagService = context.getBean(TAG_SERVICE,IServiceTag.class);
		newsService = context.getBean(NEWS_SERVICE,IServiceNews.class);
		extendedService = context.getBean(EXTENDED_SERVICE,IServiceExtended.class);
		context.close();
	}
	
	
	@Override
	public String execute(HttpServletRequest req, HttpServletResponse resp) throws ServiceException {
		List<Author> authors = authorService.getAuthorList();
		req.setAttribute(AUTHORS, authors);
		
		List <Tag> tags = tagService.getTagList();
		req.setAttribute(TAGS, tags);
		SearchCriteria sc = (SearchCriteria)req.getSession().getAttribute(SC);
		if (null!=sc){
			int newsCount = (int)newsService.getNewsByCriteriaCount(sc);
			int pageCount = (newsCount-1)/newsOnPage+1;
			req.setAttribute(PAGE_COUNT, pageCount);
			
			Integer pageN = PaginationHelper.getPageN(req);
			
			if (pageCount<pageN){
				pageN = pageCount;
			}
			
			req.getSession().setAttribute(PAGE_N, pageN);
			
			int nFrom = (pageN-1)*newsOnPage+1;
			int nTo = nFrom + newsOnPage - 1;
			
			List<Long> newsIds = newsService.getNewsIdsByCriteria(sc, nFrom, nTo);
			List <NewsInfoHolder> newsInfoHolder = extendedService.getNewsInfoHolderList(newsIds);
			req.setAttribute(NEWS_INFO_HOLDER, newsInfoHolder);
		}
		return NEWS_LIST;
	}
}
