package by.epam.newsmanagement.command.impl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.epam.newsmanagement.command.CommandType;
import by.epam.newsmanagement.command.ICommand;
import by.epam.newsmanagement.exceptions.ServiceException;
import static by.epam.newsmanagement.utils.ConstantsHolder.SC;


/**
 * @author Felix
 * 
 * The class removes search criteria entity from session scope;
 *
 */
public class ResetCommand implements ICommand {

	@Override
	public String execute(HttpServletRequest req, HttpServletResponse resp) throws ServiceException {
		req.getSession().removeAttribute(SC);
		return CommandType.VIEW_NEWS_LIST.getCommand().execute(req, resp);
	}

}
