package by.epam.newsmanagement.utils;

import java.util.ResourceBundle;



/**
 * @author Felix
 * 
 * The class is used for getting properties;
 *
 */
public class ParamHolder {
	
	static final String PROPERTY_FILE = "config/config";
	static final ResourceBundle RESOURCE_BUNDLE;
	
	static {
		RESOURCE_BUNDLE = ResourceBundle.getBundle(PROPERTY_FILE);
	}
	
	public String getProperty(String key) {
		return RESOURCE_BUNDLE.getString(key);
	}
}
