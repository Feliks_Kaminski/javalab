package by.epam.newsmanagement.command.impl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import by.epam.newsmanagement.command.CommandType;
import by.epam.newsmanagement.command.ICommand;
import by.epam.newsmanagement.exceptions.ServiceException;
import by.epam.newsmanagement.service.IServiceComment;
import static by.epam.newsmanagement.utils.ConstantsHolder.*;



/**
 * @author Felix
 * 
 * The class adds comment and redirects to news content page using newsId;
 *
 */
public class AddCommentCommand implements ICommand {
	
	static final String REDIRECT_STR ="/news-client/news?command=VIEW_NEWS&newsId=";
	static final String ERROR_STR = "&errorPostComment=true&commentText=";
	static final String ELEM_FIND = "#NCaddComment";
	
	IServiceComment commentService;
	
	{
		ClassPathXmlApplicationContext context = 
				new ClassPathXmlApplicationContext(SPRING_CONF);
		commentService = context.getBean(COMMENT_SERVICE,IServiceComment.class);
		context.close();
	}
	
	
	@Override
	public String execute(HttpServletRequest req, HttpServletResponse resp) throws ServiceException {

		String commentText = req.getParameter(COMMENT_TEXT);
		String newsIdStr = req.getParameter(NEWS_ID);
		Long newsId;
		if (null != newsIdStr){
			newsId = Long.parseLong(newsIdStr);
			if ((null != commentText)&&(commentText.length() > 0)&&(commentText.length() <= 100)){
				commentService.addComment(newsId, commentText);
				return REDIRECT_STR+newsId;
			} else {
				return REDIRECT_STR+newsId+ERROR_STR+commentText+ELEM_FIND;
			}
		} else {
			return CommandType.VIEW_NEWS_LIST.getCommand().execute(req, resp);
		}
	}

}
