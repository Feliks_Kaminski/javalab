package by.epam.newsmanagement.command;

import by.epam.newsmanagement.command.impl.*;

public enum CommandType {
	FILTER(new FilterCommand()),
	VIEW_NEWS(new ViewNewsContentCommand()),
	VIEW_NEWS_LIST(new ViewNewsListCommand()),
	RESET(new ResetCommand()),
	ADD_COMMENT(new AddCommentCommand()),
	CHANGE_LOCALE(new ChangeLocaleCommand());
	
	private ICommand command;
	
	CommandType(ICommand command){
		this.command = command;
	}
	
	public ICommand getCommand(){
		return command;
	}
}
