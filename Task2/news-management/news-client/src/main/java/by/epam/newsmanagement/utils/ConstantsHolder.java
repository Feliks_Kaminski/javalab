package by.epam.newsmanagement.utils;

public interface ConstantsHolder {

	String SPRING_CONF = "spring.xml";
	
	
	
	String NEWS_ON_PAGE = "newsOnPage";
	String COMMENT_TEXT = "commentText";
	String NEWS_ID = "newsId";
	String AUTHOR_ID = "authorId";
	String AUTHORS = "authors";
	String TAGS = "tags";
	String NEWS = "news";
	String COMMENTS = "comments";
	String AUTHOR_NAME = "authorName";
	String SC = "sc";
	String PAGE = "page";
	String PAGE_N = "pageN";
	String PAGE_COUNT = "pageCount";
	String NEWS_LIST = "newsList";
	String LANG = "lang";
	String REFERER = "referer";
	String OFFSET = "offset";
	String ERROR_POST_COMMENT = "errorPostComment";
	String NEWS_CONTENT = "newsContent";
	String NEWS_INFO_HOLDER = "newsInfoHolder";
	
	String AUTHOR_SERVICE = "spr_IServiceAuthor";
	String TAG_SERVICE = "spr_IServiceTag";
	String NEWS_SERVICE = "spr_IServiceNews";
	String COMMENT_SERVICE = "spr_IServiceComment";
	String EXTENDED_SERVICE = "spr_IServiceExtended";
	
	
	
	
}
