package by.epam.newsmanagement.command.impl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.epam.newsmanagement.command.CommandType;
import by.epam.newsmanagement.command.ICommand;
import by.epam.newsmanagement.exceptions.ServiceException;
import static by.epam.newsmanagement.utils.ConstantsHolder.*;



/**
 * @author Felix
 * 
 * The class changes locale arg in session which will be intercepted by fmt tag;
 *
 */
public class ChangeLocaleCommand implements ICommand {
	
	@Override
	public String execute(HttpServletRequest req, HttpServletResponse resp)
			throws ServiceException {
		String lang = req.getParameter(LANG);
		req.getSession().setAttribute(LANG, lang);
		
		String page = req.getHeader(REFERER);  //req.getAttribute("javax.servlet.forward.request_uri");
		return page==null?CommandType.VIEW_NEWS_LIST.getCommand().execute(req, resp):page;
	}

}
