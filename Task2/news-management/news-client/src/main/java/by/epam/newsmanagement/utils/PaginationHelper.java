package by.epam.newsmanagement.utils;

import javax.servlet.http.HttpServletRequest;



/**
 * @author Felix
 *
 *Helps to calculate pageN
 */
public class PaginationHelper {
	
	/**
	 * That method returns page number using request, session or default (1) parameters
	 * 
	 * @param req
	 * @return current page number
	 */
	public static Integer getPageN(HttpServletRequest req){
		Integer pageN = null;
		String pageStr = req.getParameter(ConstantsHolder.PAGE);
		if (null == pageStr){
			pageN = (Integer)req.getSession().getAttribute(ConstantsHolder.PAGE_N);
		} else {
			pageN = Integer.parseInt(pageStr);
		}
		if (null == pageN){
			pageN = 1;
		} 
		return pageN;
	}
}
