package by.epam.newsmanagement.utils;

import javax.servlet.http.HttpServletRequest;

import by.epam.newsmanagement.command.CommandType;
import by.epam.newsmanagement.command.ICommand;



/**
 * @author Felix
 * 
 * The class gets a command from enum by name from request;
 *
 */
public class CommandGetter {
	final static String COMMAND = "command";
	/**
	 * That method returns a command by its name from request,
	 * or VIEW_NEWS_LIST if name in request is not defined
	 * 
	 * @param req
	 * @return ICommand
	 */
	public static ICommand getCommand(HttpServletRequest req){

		ICommand command = null;
		CommandType cType=CommandType.VIEW_NEWS_LIST;
		String commandName = req.getParameter(COMMAND);
		if (commandName != null){
			try {
				cType = CommandType.valueOf(commandName);
			} catch (IllegalArgumentException e){
				
			}
		}
		command = cType.getCommand();
		return command;
	}
	
}
