package by.epam.newsmanagement.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.epam.newsmanagement.exceptions.ServiceException;

public interface ICommand {
	
	public String execute(HttpServletRequest req, HttpServletResponse resp) throws ServiceException;
}
