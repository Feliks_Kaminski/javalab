package by.epam.newsmanagement.command.impl;

import java.util.Set;
import java.util.TreeSet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.epam.newsmanagement.command.CommandType;
import by.epam.newsmanagement.command.ICommand;
import by.epam.newsmanagement.entity.SearchCriteria;
import by.epam.newsmanagement.exceptions.ServiceException;
import static by.epam.newsmanagement.utils.ConstantsHolder.*;


/**
 * @author Felix
 * 
 * The class creates search criteria entity in session scope;
 *
 */
public class FilterCommand implements ICommand {
	
	@Override
	public String execute(HttpServletRequest req, HttpServletResponse resp) throws ServiceException {
		String authorStr = req.getParameter(AUTHOR_ID);
		Long authorId = null;
		if ( (null != authorStr)&&( !("-".equals(authorStr)) ) ){
			authorId = Long.parseLong(authorStr);
		}
		Set<Long> tagIdSet = null;
		String[] tagsStr = req.getParameterValues(TAGS);
		if (tagsStr != null) {
			tagIdSet = new TreeSet<Long>();
			for (String tagId : tagsStr) {
				tagIdSet.add(Long.parseLong(tagId));
			}
		}
		SearchCriteria sc = new SearchCriteria(authorId, tagIdSet);
		req.getSession().setAttribute(SC, sc);
		req.getSession().setAttribute(PAGE_N, 1);
		return CommandType.VIEW_NEWS_LIST.getCommand().execute(req, resp);
	}

}
