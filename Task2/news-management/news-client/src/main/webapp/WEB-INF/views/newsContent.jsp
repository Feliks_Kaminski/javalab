<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<fmt:setLocale value="${lang}" scope="session"/>
<fmt:setBundle basename="localization.locale"/>

<html>
<head>
<link type="text/css" rel="stylesheet" href="resources/css/style.css" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>News</title>
</head>
<body>
<div class="border1">
		<c:import url="layouts/header.jsp"></c:import>
</div>

<div class="border1">
<div id=divMain>
	<div id="NCback"><a href="/news-client/news?command=VIEW_NEWS_LIST"><fmt:message key="newsContent.back" /></a></div>
				<div>
						 		<div id="NCnewsDetail">
									 <span id="NCnewsTitle">${news.title}</span>
									 (<fmt:message key="newsContent.by" /> ${authorName} )
								</div>
								<div id="NCnewsDate">
								<fmt:formatDate pattern="HH:mm dd/MM/yyyy" value="${news.creationDate}" />
								</div>
				</div>
				<div id="NCfullText">${news.fullText}</div>
				
				<div id="NCcomments">
					<c:forEach var="comment" items="${comments}">
							<div id="NCcommentDate">
								<fmt:formatDate pattern="HH:mm:ss dd/MM/yyyy" value="${comment.creationDate}" />
							</div>
							<div id="NCcommentText">${comment.commentText}</div>
					</c:forEach>
				</div>
				<div id="NCaddComment">
						<form method="POST" action="/news-client/news" >
						<input type="hidden" name="command" value="ADD_COMMENT"/>
						<div id="NCnewCommentText">
							<textarea name="commentText">${commentText}</textarea>
						</div>
        				<input type="hidden" name="newsId" value="${news.id}"/>
						<div id="NCpostCommentButton">
							<input type="submit" value="<fmt:message key="newsContent.postComment" />" class="butt"/></div>
						</form>
						<c:if test="${not empty errorPostComment}">
							<div id="NCerrorMessage"><fmt:message key="newsContent.commentError" /></div>
						</c:if>
				</div>
			</div>
			<div id="NCprevious">
				<a href="/news-client/news?command=VIEW_NEWS&newsId=${news.id}&offset=-1">
					<fmt:message key="newsContent.previous" />
				</a>
			</div>
			<div id="NCnext">
				<a href="/news-client/news?command=VIEW_NEWS&newsId=${news.id}&offset=1">
					<fmt:message key="newsContent.next" />
				</a>
			</div>
</div>

<div class="border2">
		<c:import url="layouts/footer.jsp"></c:import>
</div>
</body>
</html>