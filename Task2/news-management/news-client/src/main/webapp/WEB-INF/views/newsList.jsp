<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<fmt:setLocale value="${lang}" scope="session"/>
<fmt:setBundle basename="localization.locale"/>

<html>
<head>
<link type="text/css" rel="stylesheet" href="resources/css/style.css" />
<link type="text/css" rel="stylesheet" href="resources/css/multiple-select.css" />
<script src="resources/js/jquery.min.js"></script>
<script src="resources/js/jquery.multiple.select.js"></script>

<title> <fmt:message key="mainTitle" /> </title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<body>
<div class="border1">
		<c:import url="layouts/header.jsp"></c:import>
</div>

<div class="border1">
	<div id="divMain">
		<div id="NLsearchBar">
			<div>
					<form method="GET" action="/news-client/news">
					<input type="hidden" name="command" value="FILTER"/>
						<select name="authorId" id="NLauthors">
							<option value="-"><fmt:message key="newsList.selectAuthor" /></option>
							<c:forEach var="author" items="${authors}">
							<option 
							
							<c:if test="${author.id==sc.authorId}">selected="selected"</c:if>
							
							value="${author.id}">${author.name}</option>
							</c:forEach>
						</select>
						
						<select multiple="multiple" name="tags" id="NLtags">
							<c:forEach var="tag" items="${tags}">
						    <option 
						    
						    <c:if test="${sc.getTagIdSet().contains(tag.id)}">selected="selected"</c:if>
						    
						    value="${tag.id}">${tag.name}</option>
							</c:forEach>
						 </select>
						 
					    <script>
					        $('#NLtags').multipleSelect();
					    </script>
						<input type="submit" value="<fmt:message key="newsList.filter" />" class="butt"/>
 					</form>
 					<form method="GET" action="/news-client/news">
						<input type="hidden" name="command" value="RESET"/>
	 					<input type="submit" value="<fmt:message key="newsList.reset" />" class="butt"/>
 					</form>
 			</div>
 		</div>
 					<c:forEach var="newsHolder" items="${newsInfoHolder}">
 						
 						<div id="NLnewsInfo">
 							<div>
	 							<div id="NLnewsDetail">
									 <span id="NLnewsTitle">${newsHolder.news.title}</span>
									 (<fmt:message key="newsContent.by" /> ${newsHolder.author.name} )
								</div>
								<div id="NLnewsDate">
									 <fmt:formatDate pattern="HH:mm dd/MM/yyyy" value="${newsHolder.news.creationDate}" />
								</div>
							</div>
							<div id="NLnewsSText">${newsHolder.news.shortText}</div>
							<div id="NLnewsInfoFooter">
								 <span id="NLnewsTags">
									 <c:forEach var="tag" items="${newsHolder.tags}">
										 <span <c:if test="${sc.getTagIdSet().contains(tag.id)}">class="NCtagFont"</c:if>>
										 	${tag.name}
										 </span>
									 </c:forEach>
								</span>
								<span id="NLnewsCommentsCount">
									<fmt:message key="newsList.comments" />(${newsHolder.comments.size()})
								</span>
								<span id="NLnewsViewRef">
			 						<a href="/news-client/news?command=VIEW_NEWS&newsId=${newsHolder.news.id}">
			 							<fmt:message key="newsList.view" />
									</a>
			 					</span>
			 				</div>
			 			</div>
					</c:forEach>
		</div>
				<div id="NLpageCont">
 					<c:forEach var="offset" begin="0" end="4">
 						<c:if test="${(pageN+offset-2>0) and (pageN+offset-2<=pageCount)}">
							<form method="GET" id="NLpages" action="/news-client/news" >
								<input type="hidden" name="command" value="VIEW_NEWS_LIST"/>
								<input type="hidden" name="page" value="${pageN+offset-2}"/>
 								<input type="submit" value="${pageN+offset-2}" <c:if test="${offset-2==0}"> disabled=disabled </c:if> />
 							</form>
 						</c:if>
 					</c:forEach>
 				</div>
</div>

<div class="border2">
		<c:import url="layouts/footer.jsp"></c:import>
</div>
</body>	
</html>