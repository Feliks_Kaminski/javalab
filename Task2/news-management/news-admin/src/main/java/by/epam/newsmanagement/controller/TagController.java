package by.epam.newsmanagement.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import by.epam.newsmanagement.entity.Tag;
import by.epam.newsmanagement.exceptions.ServiceException;
import by.epam.newsmanagement.service.IServiceTag;


@Controller
public class TagController {
	
	private IServiceTag tagService;
	
	@Autowired
	public TagController(IServiceTag tagService){
		this.tagService=tagService;
	}
	
	
	
	
			/**
			 * Shows all tags for editing.
			 * 
			 */
	@RequestMapping(value = "/tags", method = RequestMethod.GET)
	public String view(Model model) throws ServiceException {
		List<Tag> tags = tagService.getTagList();
		model.addAttribute("tags", tags);
		return "tags";
	}
		
		
			/**
			 * Adds new tag using its name.
			 * 
			 */
	@RequestMapping(value = "/tags_add", method = RequestMethod.POST)
	public String add(@RequestParam(value = "tagName", required = true) String tagName) throws ServiceException {
		tagService.addTag(tagName);
		return "redirect:/tags";
	}
	
	
	

			/**
			 * Updates tag.
			 * 
			 */
	@RequestMapping(value = "/tags_update", method = RequestMethod.POST)
	public String update(@RequestParam(value = "tagId", required = true) Long tagId,
						 @RequestParam(value = "tagName", required = true) String tagName) throws ServiceException {
		Tag tag = tagService.getTag(tagId);
		tag.setName(tagName);
		tagService.editTag(tag);
		return "redirect:/tags";
	}
	
	
	

			/**
			 * Removes tag.
			 * 
			 */
	@RequestMapping(value = "/tags_delete", method = RequestMethod.POST)
	public String delete(@RequestParam(value = "tagId", required = true) Long tagId) throws ServiceException {
		tagService.deleteTag(tagId);
		return "redirect:/tags";
	}
}