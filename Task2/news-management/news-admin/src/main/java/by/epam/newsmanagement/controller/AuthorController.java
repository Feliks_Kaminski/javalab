package by.epam.newsmanagement.controller;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import by.epam.newsmanagement.entity.Author;
import by.epam.newsmanagement.exceptions.ServiceException;
import by.epam.newsmanagement.service.IServiceAuthor;



/**
 * @author Felix
 *
 */
@Controller
public class AuthorController {

	private IServiceAuthor authorService;
	
	@Autowired
	public AuthorController(IServiceAuthor authorService){
		this.authorService = authorService;
	}
	
	
	
	
			/**
			 * Supports authors view page.
			 * 
			 */
	@RequestMapping(value = "/authors", method = RequestMethod.GET)
	public String view(Model model) throws ServiceException {
		List<Author> authors = authorService.getAuthorList();
		model.addAttribute("authors", authors);
		return "authors";
	}
	
	
	
			/**
			 * It is for adding an author and redirecting to authors' view page.
			 * 
			 */
	@RequestMapping(value = "/authors_add", method = RequestMethod.POST)
	public String add(@RequestParam(value = "authorName", required = true) String authorName) throws ServiceException {
		authorService.addAuthor(authorName);
		return "redirect:/authors";
	}
	
	
	
			/**
			 * Upadates author's entity.
			 * 
			 */
	@RequestMapping(value = "/authors_update", method = RequestMethod.POST)
	public String update(@RequestParam(value = "authorId", required = true) Long authorId,
						 @RequestParam(value = "authorName", required = true) String authorName) throws ServiceException {
		Author author = authorService.getAuthor(authorId);
		author.setName(authorName);
		authorService.editAuthor(author);
		return "redirect:/authors";
	}

			/**
			 * Sets authors expire date.
			 * 
			 */
	@RequestMapping(value = "/authors_expire", method = RequestMethod.POST)
	public String expire(@RequestParam(value = "authorId", required = true) Long authorId) throws ServiceException {
		Author author = authorService.getAuthor(authorId);
		author.setExpired(new Timestamp(new Date().getTime()));
		authorService.editAuthor(author);
		return "redirect:/authors";
	}
	
}
