package by.epam.newsmanagement.entity;

import java.util.List;



/**
 * @author Felix
 *
 * That class is used for holding current info in session. 
 * It is also contains news ids as a cache. 
 * If next news id is requested app looks for it in that list. 
 * In that way we can avoid new database requests with search criteria and look for next news directly by its id.
 */
public class NewsPageParamHolder {

	private int newsPage;
	private int newsOnPage;
	private List<Long> newsIds;
	private int newsCount;
	
	public NewsPageParamHolder(int newsPage, int newsOnPage,
			List<Long> newsIds, int newsCount) {
		super();
		this.newsPage = newsPage;
		this.newsOnPage = newsOnPage;
		this.newsIds = newsIds;
		this.newsCount = newsCount;
	}

	public int getNewsPage() {
		return newsPage;
	}

	public void setNewsPage(int newsPage) {
		this.newsPage = newsPage;
	}

	public int getNewsOnPage() {
		return newsOnPage;
	}

	public void setNewsOnPage(int newsOnPage) {
		this.newsOnPage = newsOnPage;
	}

	public List<Long> getNewsIds() {
		return newsIds;
	}

	public void setNewsIds(List<Long> newsIds) {
		this.newsIds = newsIds;
	}

	public int getNewsCount() {
		return newsCount;
	}

	public void setNewsCount(int newsCount) {
		this.newsCount = newsCount;
	}
}
