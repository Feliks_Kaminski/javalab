package by.epam.newsmanagement.service;

import java.util.HashSet;
import java.util.Set;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import by.epam.newsmanagement.entity.User;
import by.epam.newsmanagement.exceptions.ServiceException;
import by.epam.newsmanagement.service.IServiceUser;

import org.apache.log4j.Logger;


public class UserDetailsServiceImpl implements UserDetailsService {
		
		public static Logger logger = Logger.getLogger(UserDetailsServiceImpl.class.getName());
		
		IServiceUser userService;
		
		public UserDetailsServiceImpl(IServiceUser userService){
			this.userService=userService;
		}
		
	    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
	    	
	    	User user;
	    	UserDetails userDetails;
	    	try {
	    		user = userService.getUser(userName);

		    	if (null==userName){
		    		throw new UsernameNotFoundException(userName);
		    	}
		        Set<GrantedAuthority> roles = new HashSet<GrantedAuthority>();
		        roles.add(new SimpleGrantedAuthority(userService.getUserRole(user.getId())));
		        userDetails = new org.springframework.security.core.userdetails.User(user.getLogin(),
		                                                                       user.getPassword(), 
		                                                                       roles);

	    	} catch (ServiceException e) {
	    		throw new UsernameNotFoundException("Exception in service with "+userName);
	    	}
	        return userDetails;
	    }
}

