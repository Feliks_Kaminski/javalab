package by.epam.newsmanagement.controller;

import java.sql.Date;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import by.epam.newsmanagement.entity.Author;
import by.epam.newsmanagement.entity.Comment;
import by.epam.newsmanagement.entity.News;
import by.epam.newsmanagement.entity.NewsInfoHolder;
import by.epam.newsmanagement.entity.NewsPageParamHolder;
import by.epam.newsmanagement.entity.SearchCriteria;
import by.epam.newsmanagement.entity.Tag;
import by.epam.newsmanagement.exceptions.ServiceException;
import by.epam.newsmanagement.service.IServiceAuthor;
import by.epam.newsmanagement.service.IServiceComment;
import by.epam.newsmanagement.service.IServiceExtended;
import by.epam.newsmanagement.service.IServiceNews;
import by.epam.newsmanagement.service.IServiceTag;
import by.epam.newsmanagement.utils.Converter;
import by.epam.newsmanagement.utils.Paginator;


@Controller
public class NewsController {
	
	private IServiceAuthor authorService;
	private IServiceNews newsService;
	private IServiceTag tagService;
	private IServiceComment commentService;
	private IServiceExtended extendedService;
	
	@Autowired
	public NewsController(IServiceAuthor authorService, IServiceNews newsService, IServiceTag tagService, IServiceComment commentService,
			IServiceExtended extendedService){
		this.authorService = authorService;
		this.newsService = newsService;
		this.tagService = tagService;
		this.commentService = commentService;
		this.extendedService = extendedService;
	}
	
	

			/**
			 * Shows news if a search criteria in session exists
			 * 
			 */
	@RequestMapping(value = "/newsList", method = RequestMethod.GET)
	public String viewList(Model model, HttpSession session,
			@RequestParam(value = "newsPageNext", required = false) Integer nextNewsPage) throws ServiceException {
		List <Author> authors = authorService.getAuthorList();
		model.addAttribute("authors", authors);
		
		List <Tag> tags = tagService.getTagList();
		model.addAttribute("tags", tags);
		

		NewsPageParamHolder holder = Paginator.getHolder(session);
		if (null!=holder){
			
			List <Long> newsIds = Paginator.fillNewsIds(session, nextNewsPage);
			if (null!=newsIds){
				List <NewsInfoHolder> newsInfoHolder = extendedService.getNewsInfoHolderList(newsIds);
				model.addAttribute("newsInfoHolder", newsInfoHolder);
				model.addAttribute("pages", Paginator.getPagesList(holder));
			}
		}
		return "newsList";
	}
	

			/**
			 * Creates a search criteria and puts it in session, after that redirects to news list page.
			 * Also it removes current holder from session to initiate receiving new data from DB.
			 */
	@RequestMapping(value = "/newsListFind", method = RequestMethod.POST)
	public String find(Model model, HttpSession session,
							@RequestParam(value = "authorId", required = false) Long authorId,
							@RequestParam(value = "tags", required = false) TreeSet<Long> tagIdSet) throws ServiceException {
		
		SearchCriteria sc = new SearchCriteria(authorId, tagIdSet);
		session.removeAttribute("newsParamHolder");
		session.setAttribute("searchCriteria", sc);
		return "redirect:/newsList";
	}
	
	

		/**
		 * 	Removes sc and holder from session. After that news list page has nothing to show.
		 * 
		 */
	@RequestMapping(value = "/newsListReset", method = RequestMethod.POST)
	public String reset(HttpSession session) throws ServiceException {
		session.removeAttribute("newsParamHolder");
		session.removeAttribute("searchCriteria");
		return "redirect:/newsList";
	}
	
	

		/**
		 * Removes marked news.
		 * 
		 */
	@RequestMapping(value = "/newsListDelete", method = RequestMethod.GET)
	public String deleteNews(@RequestParam(value = "idsForDel", required = true) List<Long> idsForDel) throws ServiceException {
		for(Long newsId : idsForDel){
			extendedService.deleteNewsWithDependencies(newsId);
		}
		return "redirect:/newsList";
	}
	
	

			/**
			 * Shows single news by its id for editing.
			 * 
			 */
	@RequestMapping(value = "/news_edit/{newsId}", method = RequestMethod.GET)
	public String editNews(Model model,@PathVariable int newsId) throws ServiceException {
		News news=newsService.getNews(newsId);
		
		List <Author> authors = authorService.getAuthorList();
		List <Tag> tags = tagService.getTagList();
		
		model.addAttribute("authors", authors);
		model.addAttribute("tags", tags);
		
		if (null!=news){
			model.addAttribute("news", news);
			model.addAttribute("authorId", newsService.getAuthorIdByNewsId(newsId));

			Set<Long> idsSet = Converter.TagListToLongIdsSet(newsService.getTagListByNewsId(newsId));
			
			model.addAttribute("newsTags", idsSet);
		}
		return "news_edit";
	}
	
	

			/**
			 * Creates new news if id is null, else updates news.
			 * Redirects to news edit page after inserting or to news list page after updating
			 */
	@RequestMapping(value = "/news_add_update", method = RequestMethod.POST)
	public String addNews(@RequestParam(value = "authorId", required = true) long authorId,
					  @RequestParam(value = "newsId", required = false) Long newsId,
					  @RequestParam(value = "title", required = true) String title,
					  @RequestParam(value = "shortText", required = true) String shortText,
					  @RequestParam(value = "fullText", required = true) String fullText,
					  @RequestParam(value = "modificationDate", required = true) Date modificationDate,
					  @RequestParam(value = "tags", required = false) List<Long> tags) throws ServiceException {
		
		if (null!=newsId){
			extendedService.editNewsWithDependencies(authorId, newsId, title, shortText, fullText, modificationDate, tags);
			return "redirect:/newsList";
		} else {
			newsId=extendedService.addNewsWithDependencies(authorId, title, shortText, fullText, modificationDate, tags);
			return "redirect:/news_edit/0";
		}
	}
	
	

			/**
			 * Shows news info. Has ability to add / remove comments.
			 * 
			 */
	@RequestMapping(value = "/news_comments/{newsId}", method = RequestMethod.GET)
	public String newsComments(Model model, HttpSession session, @PathVariable long newsId) throws ServiceException {

		NewsPageParamHolder holder = Paginator.getHolder(session);
		if (null!=holder){
			
			int newsN = Paginator.getNewsN(session, newsId);
			if (0==newsN){
				return "redirect:/newsList";
			}
			
			int newsPage;
			
			newsPage = (newsN-1)/holder.getNewsOnPage()+1;

			Paginator.fillNewsIds(session, newsPage);
		}
		
		
		News news=newsService.getNews(newsId);
		if (null!=news){
			model.addAttribute("news", news);
			model.addAttribute("authorName", authorService.getAuthor(newsService.getAuthorIdByNewsId(newsId)).getName());
			
			List <Comment> comments = commentService.getCommentsListByNewsId(newsId, 1, 0);
			model.addAttribute("comments", comments);
		}
		return "news_comments";
	}
	
	

			/**
			 * It is used for finding next/previous news. Needs offset to find a next number of a news.
			 * 
			 */
	@RequestMapping(value = "/news_comments_next/{newsId}/{direction}", method = RequestMethod.GET)
	public String nextNews(Model model, HttpSession session,  @PathVariable long newsId,
								@PathVariable int direction) throws ServiceException {
		long nextNewsId;
		int newsN = Paginator.getNewsN(session, newsId);
		if (0==newsN){
			return "redirect:/newsList";
		}
		newsN += direction;
		NewsPageParamHolder holder = (NewsPageParamHolder) session.getAttribute("newsParamHolder");
		if ((null==holder)||(holder.getNewsCount()<newsN)||(newsN<1)){
			return "redirect:/newsList";
		}
			int newsPage = (newsN-1)/holder.getNewsOnPage()+1;
			Paginator.fillNewsIds(session, newsPage);
			nextNewsId = Paginator.getNextNewsId(newsN, holder);
			if (0==nextNewsId){
				return "redirect:/newsList";
			}
		return "redirect:/news_comments/"+nextNewsId;
	}
	
	
				
				/**
				 * 	Removes a comment of the news.
				 * 
				 */
	@RequestMapping(value = "/delete_comment", method = RequestMethod.POST)
	public String deleteComment(@RequestParam(value = "commentId", required = true) Long commentId,
								@RequestParam(value = "newsId", required = true) Long newsId) throws ServiceException {
		commentService.deleteComment(commentId);
		return "redirect:/news_comments/"+newsId;
	}
	
	

				/**
				 * 	Adds news comment.
				 * 
				 */
	@RequestMapping(value = "/add_comment", method = RequestMethod.POST)
	public String addComment(@RequestParam(value = "newsId", required = true) Long newsId,
							 @RequestParam(value = "commentText", required = true) String commentText) throws ServiceException {
		commentService.addComment(newsId, commentText);
		return "redirect:/news_comments/"+newsId;
	}
}
