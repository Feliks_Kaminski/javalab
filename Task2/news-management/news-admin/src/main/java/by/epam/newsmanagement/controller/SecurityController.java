package by.epam.newsmanagement.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class SecurityController {
	
	
		    /**
		     * Intercepts root request and sends to login page.
		     * 
		     */
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String home() {
        return "login";
    }
    
    
    
		    /**
		     * Shows login page.
		     * 
		     */
    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String loginerror(Model model,@RequestParam(value = "logon_msg", required = false) String logon_msg) {
        if ("denied".equals(logon_msg)){
        	model.addAttribute("logon_msg", "Access denied.");
        }
        if ("logpasw".equals(logon_msg)){
        	model.addAttribute("logon_msg", "Invalid login/password.");
        }
        if ("logout".equals(logon_msg)){
        	model.addAttribute("logon_msg", "Bye.");
        }
        return "login";
    }

}
