package by.epam.newsmanagement.utils;

import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import by.epam.newsmanagement.entity.NewsPageParamHolder;
import by.epam.newsmanagement.entity.SearchCriteria;
import by.epam.newsmanagement.exceptions.ServiceException;
import by.epam.newsmanagement.service.IServiceNews;

public class Paginator {
	static final int NEWS_ON_PAGE=2;
	
	static IServiceNews newsService;
	
	static {
		ClassPathXmlApplicationContext context = 
				new ClassPathXmlApplicationContext("spring.xml");
		newsService = context.getBean("spr_IServiceNews",IServiceNews.class);
		context.close();
	}
	
	
	/**
	 * Returns a list of pages.
	 * The list may contain 1st and last page, cur page +-50,+-10,-2 - +2.
	 * 
	 */
	public static List<Integer> getPagesList(NewsPageParamHolder holder){

		List<Integer> pages = new LinkedList<Integer>();
		
		int pageCount = (holder.getNewsCount()-1)/holder.getNewsOnPage()+1;
		
		int newsPage = holder.getNewsPage();
		
		if (newsPage>50){pages.add(newsPage-50);}
		if (newsPage>10){pages.add(newsPage-10);}
		if (newsPage>2 ){pages.add(newsPage-2);}
		if (newsPage>1 ){pages.add(newsPage-1);}
						 pages.add(newsPage);
		if (newsPage+1<=pageCount){pages.add(newsPage+1);}
		if (newsPage+2<=pageCount){pages.add(newsPage+2);}
		if (newsPage+10<=pageCount){pages.add(newsPage+10);}
		if (newsPage+50<=pageCount){pages.add(newsPage+50);}
		if (pages.get(pages.size()-1)!=pageCount){pages.add(pageCount);}
		
		if(pages.get(0)!=1){pages.add(0, 1);}
		
		return pages;
	}
	
	
	
	
	/**
	 * That method tries to calculate page N using 2 sources: cache and then database.
	 * 
	 */
	public static int getNewsN(HttpSession session,long newsId) throws ServiceException{
		int newsN=0;
		NewsPageParamHolder holder = (NewsPageParamHolder) session.getAttribute("newsParamHolder");
		if (null!=holder){
			List <Long> newsIds = holder.getNewsIds();
			if (null != newsIds){
				newsN = newsIds.indexOf(newsId);
				if (-1!=newsN){
					newsN = newsN + 1 + holder.getNewsOnPage()*(holder.getNewsPage()-1);
				}
			} else {
				newsN=-1;
			}
			
			
			if (-1==newsN){
				SearchCriteria sc = (SearchCriteria)session.getAttribute("searchCriteria");
				newsN = newsService.getNewsNumberByCriteria(sc, newsId);
			}
			
			if (newsN>holder.getNewsCount()){
				newsN = holder.getNewsCount();
			}
		}
		return newsN;
	}
	
	
	/**
	 * Returns list of news ids. At first the method returns list of ids from cache if the page was not changed.
	 * Then if the page was changed or cache is empty the method gets list of ids from database. It is also calculates 
	 * range of news numbers using the page number.
	 * 
	 */
	public static List<Long> fillNewsIds(HttpSession session, Integer newsPage) throws ServiceException{
		SearchCriteria sc = (SearchCriteria)session.getAttribute("searchCriteria");
		NewsPageParamHolder holder = (NewsPageParamHolder) session.getAttribute("newsParamHolder");
		List <Long> newsIds = holder.getNewsIds();

		if ((null==newsIds)||((newsPage!=null)&&(newsPage!=holder.getNewsPage()))){
			if (newsPage!=null){
				holder.setNewsPage(newsPage);
			}
			int nFrom = (holder.getNewsPage()-1)*holder.getNewsOnPage()+1;
			int nTo = nFrom + holder.getNewsOnPage() - 1;
			newsIds = newsService.getNewsIdsByCriteria(sc, nFrom, nTo);
			holder.setNewsIds(newsIds);
		}
		return newsIds;
	}
	
	
	/**
	 * The method returns a holder from session or new one (if doesn't find it in session).
	 */
	public static NewsPageParamHolder getHolder(HttpSession session) throws ServiceException{
		SearchCriteria sc = (SearchCriteria)session.getAttribute("searchCriteria");
		NewsPageParamHolder holder = null;;
		if (null!=sc){
			holder = (NewsPageParamHolder) session.getAttribute("newsParamHolder");
			if (null==holder){
				holder = new NewsPageParamHolder(1, NEWS_ON_PAGE, null, (int)newsService.getNewsByCriteriaCount(sc));
				session.setAttribute("newsParamHolder", holder);
			}
		}
		return holder;
	}
	
	
	/**
	 * Calculates next news id using current news id from cache.
	 * 
	 */
	public static long getNextNewsId(int newsN, NewsPageParamHolder holder){
		long newsId = 0;
		
		List <Long> newsIds = holder.getNewsIds();
		if (null!=newsIds){
			int index = (newsN-1)%holder.getNewsOnPage()+1-1;
			if ((index>=0)&&(index<newsIds.size())){
				newsId = newsIds.get(index);
			}
		}
		return newsId;
	}
}
