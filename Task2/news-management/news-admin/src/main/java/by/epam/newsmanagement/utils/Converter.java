package by.epam.newsmanagement.utils;

import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import by.epam.newsmanagement.entity.Tag;

/**
 * @author Felix
 *
 */
public class Converter {
	
	/**
	 * The class is used for packing news ids in set from list of Tag entity.
	 * The TreeSet located in search criteria and is used for marking Tags which has been chosen.
	 * 
	 */
	public static Set<Long> TagListToLongIdsSet(List<Tag> tags){
		TreeSet<Long> set = new TreeSet<Long>();
		for(Tag tag:tags){
			set.add(tag.getId());
		}
		return set;
	}
}
