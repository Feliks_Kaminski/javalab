function doSubmit(elem) {
	while (elem.tagName != 'FORM') {
		elem = elem.parentNode;
	}
	elem.submit();
}
function show(id) {
		var i=1;
		while (document.getElementById("e"+i) !== null) {
			hide(i);
			i++;
		}
		document.getElementById("u"+id).style.visibility = "visible";
		document.getElementById("d"+id).style.visibility = "visible";
		document.getElementById("c"+id).style.visibility = "visible";
		document.getElementById("e"+id).style.visibility = "hidden";
		document.getElementById("t"+id).disabled="";
}
function hide(id) {
		document.getElementById("u"+id).style.visibility = "hidden";
		document.getElementById("d"+id).style.visibility = "hidden";
		document.getElementById("c"+id).style.visibility = "hidden";
		document.getElementById("e"+id).style.visibility = "visible";
		document.getElementById("t"+id).disabled="disabled";
}