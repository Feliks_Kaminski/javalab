<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<html>
<body>
	<h1><spring:message code="news_comments.header" /></h1>
        		<table>
				<tr>
 					<td>
 						${news.title}
					</td>
 					<td>
 						${authorName}
 					</td>
 					<td>
						${news.creationDate}
 					</td>
 				</tr>
				<tr>
 					<td>
						${news.fullText}
					</td>
				</tr>
				
				<c:forEach var="comment" items="${comments}" varStatus="status">
				<tr>	
					<td>
						<form method="POST" action="<spring:url value="\news-admin\delete_comment"/>" >
						<p>${comment.creationDate}
						<input type="submit" value="x"/></p>
						<p>${comment.commentText}</p>
        				<input type="hidden" name="commentId" value="${comment.id}"/>
        				<input type="hidden" name="newsN" value="${newsN}"/>
						<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
						</form>
					</td>
				</tr>
				</c:forEach>
				<tr>
					<td>
						<form method="POST" action="<spring:url value="\news-admin\add_comment"/>" >
						<input type="text" required="required" name="commentText">
        				<input type="hidden" name="newsId" value="${news.id}"/>
						<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
						<input type="submit" value="Post comment"/></p>
						</form>
					</td>
				</tr>
        	</table>
        	<form method="GET" action="<spring:url value="/news_comments_next/${news.id}/-1"/>" >
						<input type="submit" value="Previous"/></p>
			</form>
        	<form method="GET" action="<spring:url value="/news_comments_next/${news.id}/1"/>" >
						<input type="submit" value="Next"/></p>
			</form>
        	
</body>
</html>
