<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<html>
<body>
	<h1><spring:message code="tags.header" /></h1>
<table>
<c:forEach var="tag" items="${tags}" varStatus="stat">
	<tr>
		<td>
			<form method="POST" action="\news-admin\tags_update">
				<input type="hidden" name="tagId" value="${tag.id}"/>
				<input id="t${stat.getCount()}" type="text" name="tagName" value="${tag.name}">
				<a id="e${stat.getCount()}" href="javascript:;" onclick="show(${stat.getCount()})">Edit</a>
				<a id="u${stat.getCount()}" href="javascript:;" onclick="doSubmit(parentNode)" style="visibility:hidden">Update</a>
				<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
				</form>
		</td>
		<td>
			<form method="POST" action="\news-admin\tags_delete">
				<input type="hidden" name="tagId" value="${tag.id}"/>
				<a id="d${stat.getCount()}" href="javascript:;" onclick="parentNode.submit()" style="visibility:hidden">Delete</a>
				<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                		</form>
		</td>
		<td>
			<a id="c${stat.getCount()}" href="javascript:;" onclick="hide(${stat.getCount()})" style="visibility:hidden">Cancel</a>
		</td>
	</tr>
</c:forEach>
	<tr>
		<td>
			<form method="POST" action="<spring:url value="\news-admin\tags_add"/>" >
				<input type="text" name="tagName">
				<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
				<a href="javascript:;" onclick="parentNode.submit()">Save</a>
			</form>
		</td>
	</tr>
</table>
</body>
</html>
