<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<html>
<body>


	<h1><spring:message code="newsList.header" /></h1>
				<table>
				<tr>
					<form method="POST" action="<spring:url value="\news-admin\newsListFind"/>" >
					<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
 					<td>
						<select name="authorId">
							<c:forEach var="author" items="${authors}" varStatus="status">
							<option 
							
							<c:if test="${author.id==searchCriteria.authorId}">selected="selected"</c:if>
							
							value="${author.id}">${author.name}</option>
							</c:forEach>
						</select>
 					</td>
 					<td>
						<select size="3" multiple="multiple" name="tags" id="tags">
							<c:forEach var="tag" items="${tags}" varStatus="status">
						    <option 
						    
						    <c:if test="${searchCriteria.getTagIdSet().contains(tag.id)}">selected="selected"</c:if>
						    
						    value="${tag.id}">${tag.name}</option>
							</c:forEach>
						 </select>
 					</td>
 					<td>
						<input type="submit" value="Filter"/>
 					</td>
 					</form>
 					<td>
						<form method="POST" action="<spring:url value="\news-admin\newsListReset"/>" >
							<input type="submit" value="Reset"/>
							<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
						</form>
 					</td>
				</tr>
				<form method="GET" action="<spring:url value="\news-admin\newsListDelete"/>" >
 					<c:forEach var="newsHolder" items="${newsInfoHolder}" varStatus="status">
	 					<tr>
	 						<td>
								 <a href="/news-admin/news_edit/${newsHolder.news.id}">${newsHolder.news.title}</a>
								 ${newsHolder.author.name}
								 ${newsHolder.news.creationDate}
								 ${newsHolder.news.shortText}
								 <p>
									 <c:forEach var="tag" items="${newsHolder.tags}">
									 	${tag.name}
									 </c:forEach>
								 </p>
								Comments(${newsHolder.comments.size()})
								
			 					<input type="checkbox" name="idsForDel" value="${newsHolder.news.id}" />
			 					<a href="/news-admin/news_comments/${newsHolder.news.id}">Edit</a>
	 						</td>
	 					</tr>
					</c:forEach>
 					<tr>
 						<td>
							<input type="submit" value="Delete"/>
 						</td>
 					</tr>
	 			</form>
		</table>
		<table>
			<tr>
				<c:forEach var="page" items="${pages}" varStatus="status">
				 
			 		<td>
						<form method="GET" action="<spring:url value="\news-admin\newsList"/>" >
							<input type="hidden" name="newsPageNext" value="${page}"/>
							<input type="submit" value="${page}" <c:if test="${page==newsParamHolder.newsPage}"> disabled=disabled </c:if> />
						</form>
					</td>
 				</c:forEach>
			</tr>
 		</table>
</body>
</html>

