<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
 <head>
 	<title><tiles:getAsString name="title"/></title>
 	<script src="<c:url value="resources/js/js.js"/>"></script>
 </head>
 <body>
 	<table height="100%" width="100%" border="1"">
		<tr>
			<td height="100">
				<tiles:insertAttribute name="header" />
			</td>
		</tr>
		<tr valign="top">
			<td>
				<table width="100%">
					<tr>
						<td height="250" width="50" valign="top">
							<tiles:insertAttribute name="menu" />
						</td>
						<td width="350" valign="top">
							<tiles:insertAttribute name="body" />
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td height="20" valign="baseline" align="center">
				<tiles:insertAttribute name="footer" />
			</td>
		</tr>
	</table>
</body>
</html>