<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page session="true"%>
<html>
  <body>
		<table height="100%" width="100%">
			<tr>
				<td width="80%">
					<h1 align="left">News portal - Administration</h1>
				</td>
				<td width="20%" align="right">
					<table>
						<tr>
							<c:if test="${pageContext.request.userPrincipal.name != null}">
							<td align="right">
								Welcome : ${pageContext.request.userPrincipal.name}  
							</td>
							<td align="right">
								<c:url value="/j_spring_security_logout" var="logoutUrl" />
								<!-- csrt for log out-->
								<form action="${logoutUrl}" method="post" id="logoutForm">
								  <input type="hidden" 
									name="${_csrf.parameterName}"
									value="${_csrf.token}" />
									<input type="submit" value="Logout"/>
								</form>
							</td>
							</c:if>
						</tr>
						<tr>
							<td align="right" colspan="2">
								<a href="?locale=en">EN</a>  <a href="?locale=ru">RU</a>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
  </body>
</html>