declare
q_author NUMBER := 100 ;
q_tag NUMBER := 30 ;
q_news NUMBER := 1000 ;
q_users NUMBER := 20;
begin
delete from NEWS_AUTHOR;
delete from NEWS_TAG;
delete from COMMENTS;
delete from NEWS;
delete from TAG;
delete from AUTHOR;
delete from ROLES;
delete from USERS;

insert into AUTHOR (AUTHOR_ID,AUTHOR_NAME,EXPIRED)
select rownum id,'Author'||rownum name,case when rownum>20 then null else SYSDATE-rownum end exp
from dual
CONNECT BY rownum<=q_author;

insert into TAG (TAG_ID,TAG_NAME)
select rownum id,'Tag'||rownum name
from dual
CONNECT BY rownum<=q_tag;


insert into NEWS (NEWS_ID,TITLE,SHORT_TEXT,FULL_TEXT,CREATION_DATE,MODIFICATION_DATE)
select rownum id,
'NewsT'||rownum title,
'NewsST'||rownum short_text,
'NewsFT'||rownum full_text,
SYSDATE-rownum*2 crDate,
SYSDATE-rownum modDate
from dual
CONNECT BY rownum<=q_news;

insert into COMMENTS (COMMENT_ID,NEWS_ID,COMMENT_TEXT,CREATION_DATE)
select rownum id,n.NEWS_ID,'Comment'||n.NEWS_ID||'-'||rownum||'-'||nom.nom text,sysdate-rownum crDate
from NEWS n
join (select rownum nom from dual CONNECT BY rownum<=50) nom on nom.nom<=51-mod(round(n.NEWS_ID/2)*2,50)
where n.NEWS_ID between 20 and q_news-20;

insert into NEWS_AUTHOR (NEWS_ID,AUTHOR_ID)
select n.NEWS_ID,a.AUTHOR_ID
from NEWS n
join AUTHOR a on a.AUTHOR_ID=mod(q_news-n.NEWS_ID+1,q_author)+1;


insert into NEWS_TAG (NEWS_ID,TAG_ID)
select n.NEWS_ID,t.TAG_ID
from NEWS n
join (select rownum nom from dual CONNECT BY rownum<=q_tag/3) nom on nom<=mod(1001-n.NEWS_ID,q_tag/3+1)
join TAG t on t.TAG_ID=mod(nom.nom+nom.nom*n.NEWS_ID,q_tag+1);

insert into USERS (USER_ID,USER_NAME,LOGIN,PASSWORD)
select rownum USER_ID, 'User'||rownum USER_NAME, 'Login'||rownum LOGIN, 'Password'||rownum PASSWORD
from dual
CONNECT BY rownum<=q_users;

insert into ROLES (USER_ID,ROLE_NAME)
select rownum USER_ID, 'Role_Name'||(mod(rownum,4)+1) USER_NAME
from USERS;

commit;

end;
