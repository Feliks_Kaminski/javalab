package by.epam.newsmanagement.service;

import java.util.List;

import by.epam.newsmanagement.entity.Author;
import by.epam.newsmanagement.exceptions.ServiceException;

/**
 * This is an interface for operations with IDAOAuthor.
 * 
 * @author Feliks_Kaminski
 *
 */
public interface IServiceAuthor {
	/**
	 * The method inserts an Author entity and returns it's id.
	 * 
	 * @param name of the Author
	 * @return Author's id
	 * @throws ServiceException
	 */
	public long addAuthor(String name) throws ServiceException;

	/**
	 * The method gets an Author entity and updates it's fields using it's id.
	 * 
	 * @param Author entity
	 * @throws ServiceException
	 */
	public void editAuthor(Author author) throws ServiceException;

	/**
	 * The method returns an Author entity using it's id.
	 * 
	 * @param authorId
	 * @throws ServiceException
	 */
	public Author getAuthor(long authorId) throws ServiceException;

	/**
	 * The method returns an Author entity using it's unique name.
	 * 
	 * @param name
	 * @return
	 * @throws ServiceException
	 */
	public Author getAuthor(String name) throws ServiceException;

	/**
	 * The method removes an Author using it's id.
	 * 
	 * @param authorId
	 * @throws ServiceException
	 */
	public void deleteAuthor(long authorId) throws ServiceException;

	/**
	 * The method returns a list of non-expired Author entities ordered by name.
	 * 
	 * @return list with Author entities
	 * @throws ServiceException
	 */
	public List<Author> getAuthorList() throws ServiceException;
}
