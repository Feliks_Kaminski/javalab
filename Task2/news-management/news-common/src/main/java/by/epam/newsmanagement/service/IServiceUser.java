package by.epam.newsmanagement.service;

import by.epam.newsmanagement.entity.User;
import by.epam.newsmanagement.exceptions.ServiceException;

/**
 * This is an interface for operations on User entity.
 * 
 * @author Feliks_Kaminski
 *
 */
public interface IServiceUser {
	
	/**
	 * The method inserts a User entity and returns it's id.
	 * 
	 * @param userName
	 * @param login
	 * @param password
	 * @return User id
	 * @throws ServiceException
	 */
	public long addUser(String userName, String login, String password)
			throws ServiceException;

	/**
	 * The method gets a User entity and updates it's fields using it's id.
	 * 
	 * @param user
	 * @throws ServiceException
	 */
	public void editUser(User user) throws ServiceException;

	/**
	 * The method returns a User entity using it's name.
	 * 
	 * @param login
	 * @return User entity
	 * @throws ServiceException
	 */
	public User getUser(String login) throws ServiceException;

	/**
	 * The method returns a User entity using it's id.
	 * 
	 * @param userId
	 * @return User entity
	 * @throws ServiceException
	 */
	public User getUserById(long userId) throws ServiceException;

	/**
	 * The method removes a User from the ds using it's id.
	 * 
	 * @param userId
	 * @throws ServiceException
	 */
	public void deleteUser(long userId) throws ServiceException;

	/**
	 * The method returns a User Role using it's id.
	 * 
	 * @param userId
	 * @return String userRole
	 * @throws ServiceException
	 */
	public String getUserRole(long userId) throws ServiceException;

	/**
	 *  The method inserts a User Role using it's id.
	 * 
	 * @param userId
	 * @param role
	 * @throws ServiceException
	 */
	public void addUserRole(long userId, String role) throws ServiceException;

	/**
	 *  The method updates a User Role using it's id.
	 * 
	 * @param userId
	 * @param role
	 * @throws ServiceException
	 */
	public void updateUserRole(long userId, String role)
			throws ServiceException;

	/**
	 *  The method deletes a User Role using it's id.
	 * 
	 * @param userId
	 * @throws ServiceException
	 */
	public void deleteUserRole(long userId) throws ServiceException;
}
