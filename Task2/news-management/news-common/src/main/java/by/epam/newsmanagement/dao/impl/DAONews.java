package by.epam.newsmanagement.dao.impl;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.sql.Date;
import java.util.List;






import java.util.Set;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import by.epam.newsmanagement.dao.IDAONews;
import by.epam.newsmanagement.entity.Author;
import by.epam.newsmanagement.entity.News;
import by.epam.newsmanagement.entity.SearchCriteria;
import by.epam.newsmanagement.entity.Tag;
import by.epam.newsmanagement.exceptions.DAOException;


@Transactional(propagation = Propagation.MANDATORY)
public class DAONews implements IDAONews {
	private static final String ADD_NEWS="insert into NEWS (NEWS_ID,TITLE,SHORT_TEXT,FULL_TEXT,MODIFICATION_DATE,CREATION_DATE) values (seq_news_id.nextval,?,?,?,?,?)";
	private static final String EDIT_NEWS="update NEWS set TITLE=?,SHORT_TEXT=?,FULL_TEXT=?,MODIFICATION_DATE=? where NEWS_ID=?";
	private static final String GET_NEWS="select TITLE,SHORT_TEXT,FULL_TEXT,CREATION_DATE,MODIFICATION_DATE from NEWS where NEWS_ID=?";
	private static final String DELETE_NEWS="delete from NEWS where NEWS_ID=?";
	
	private static final String GET_NEWS_BY_CRITERIA_NEWS_H="select big.* from";
	private static final String GET_NEWS_BY_CRITERIA_COUNT_H="select news_count from";
	private static final String GET_NEWS_BY_CRITERIA_NEWS_IDS="select NEWS_ID,news_count from";
	private static final String GET_NEWS_BY_CRITERIA_NEWS_NUMBER="select num from";
	
	private static final String GET_NEWS_BY_CRITERIA_P1= " (select sml.*,rownum num from "+
														 "   (select n.NEWS_ID,n.TITLE,n.SHORT_TEXT,n.FULL_TEXT,n.CREATION_DATE,n.MODIFICATION_DATE, "+
														 "     count(*) tag_count,  "+
														 "     COUNT(*) OVER () news_count, "+
														 "     nvl((select count(*) from COMMENTS cm where cm.NEWS_ID=n.NEWS_ID),0) comm_count "+
														 "    from NEWS n ";
	private static final String GET_NEWS_BY_CRITERIA_P2= "    join NEWS_AUTHOR na on na.NEWS_ID=n.NEWS_ID and na.AUTHOR_ID=? ";
	private static final String GET_NEWS_BY_CRITERIA_P3= "    join NEWS_TAG nt on nt.NEWS_ID=n.NEWS_ID and nt.TAG_ID in(";      //tag_ids) ";
	private static final String GET_NEWS_BY_CRITERIA_P4= "    group by n.NEWS_ID,n.TITLE,n.SHORT_TEXT,n.FULL_TEXT,n.CREATION_DATE,n.MODIFICATION_DATE "+
														 "    order by tag_count desc,comm_count desc,n.CREATION_DATE desc "+
														 "   ) sml "+
														 "   where rownum<=decode (?,0,rownum,?) "+
														 " ) big "+
														 "where num >= ? ";
			
			
			
			
			
			/*
	  		"select big.* "+
			"from(select n.*,sml.tag_count,comm_count, "+
            "COUNT(*) OVER () news_count, "+
            "ROW_NUMBER() OVER (ORDER BY tag_count desc,comm_count desc, CREATION_DATE desc) numb "+
			"       from (select nt.NEWS_ID,count(*) tag_count,(select count(*) from COMMENTS cm where cm.NEWS_ID=nt.NEWS_ID) comm_count "+
			"             from NEWS_TAG nt "+
			"             where nt.TAG_ID in ( "+*/
		  //"             0/*ids*/ "+
		  /*"              ) and (?=0 or nt.NEWS_ID in "+
			"                 (select NEWS_ID from NEWS_AUTHOR na where na.AUTHOR_ID=?)) "+
			"             group by nt.NEWS_ID "+
			"            ) sml "+
			"       join NEWS n on n.NEWS_ID = sml.NEWS_ID "+
			"     ) big  "+
			"where numb between ? and decode (?,0,numb,?)  "+
			"order by numb";*/

	//NewsAuthor
	private static final String ADD_NEWS_AUTHOR = "insert into NEWS_AUTHOR (NEWS_ID,AUTHOR_ID) values (?,?)";
	private static final String GET_AUTHOR_BY_NEWS = "select AUTHOR_ID from NEWS_AUTHOR where NEWS_ID=?";
	private static final String DELETE_NEWS_AUTHOR_BY_NEWS_ID = "delete from NEWS_AUTHOR where NEWS_ID=?";
	private static final String GET_NEWS_LIST_BY_AUTHOR = "select NEWS_ID from NEWS_AUTHOR where AUTHOR_ID=?";
	//News_Tag
	private static final String ADD_NEWS_TAG = "insert into NEWS_TAG (NEWS_ID,TAG_ID) values (?,?)";
	private static final String DOES_NEWS_TAG_EXIST = "select 1 from NEWS_TAG where NEWS_ID=? and TAG_ID=?";
	private static final String GET_NEWS_LIST_BY_TAG = "select NEWS_ID from NEWS_TAG where TAG_ID=?";
	private static final String DELETE_NEWS_TAG = "delete from NEWS_TAG where NEWS_ID=? and TAG_ID=?";
	private static final String DELETE_NEWS_TAG_BY_NEWS_ID = "delete from NEWS_TAG where NEWS_ID=?";
	private static final String DELETE_NEWS_TAG_BY_TAG_ID = "delete from NEWS_TAG where TAG_ID=?";
	private static final String GET_TAG_LIST_BY_NEWS = "select t.TAG_ID,t.TAG_NAME from NEWS_TAG nt "+
													   "join TAG t on t.TAG_ID=nt.TAG_ID where NEWS_ID=?";
	
	private ADAOKey key;
	
	public DAONews(ADAOKey key){
		this.key=key;
	}
	
	public long addNews(String title, String shortText, String fullText, Date modificationDate) throws DAOException {
		Connection con=key.getConnection();    	
        PreparedStatement ps = null;
        ResultSet rs = null;
        long id = 0;
        try {
            ps = con.prepareStatement(ADD_NEWS,new String[] { "NEWS_ID","CREATION_DATE","MODIFICATION_DATE" });
            ps.setString(1, title);
            ps.setString(2, shortText);
            ps.setString(3, fullText);
            ps.setDate(4, modificationDate);
            ps.setTimestamp(5, new Timestamp(modificationDate.getTime()));
            ps.executeUpdate();
            rs = ps.getGeneratedKeys();
            rs.next();
            id=rs.getLong(1);
        } catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			key.releaseDAOKey(con, ps, rs);
		}
        return id;
	}

	public void editNews(News news) throws DAOException {
		Connection con=key.getConnection();    	
        PreparedStatement ps = null;
        try {
            ps = con.prepareStatement(EDIT_NEWS);
            ps.setString(1, news.getTitle());
            ps.setString(2, news.getShortText());
            ps.setString(3, news.getFullText());
            ps.setDate(4, news.getModificationDate());
            ps.setLong(5, news.getId());
            ps.executeUpdate();
        } catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			key.releaseDAOKey(con, ps, null);
		}
	}

	public News getNews(long newsId) throws DAOException {
		Connection con=key.getConnection();    	
        PreparedStatement ps = null;
        ResultSet rs = null;
        News news = null;
        try {
            ps = con.prepareStatement(GET_NEWS);
            ps.setLong(1, newsId);
            rs = ps.executeQuery();
            if (rs.next()) {
            	news = new News(newsId, rs.getString("TITLE"),rs.getString("SHORT_TEXT"),rs.getString("FULL_TEXT"),
            			rs.getTimestamp("CREATION_DATE"),rs.getDate("MODIFICATION_DATE"));
            }
        } catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			key.releaseDAOKey(con, ps, rs);
		}
        return news;
	}

	public void deleteNews(long newsId) throws DAOException {
		Connection con=key.getConnection();    	
        PreparedStatement ps = null;
        try {
            ps = con.prepareStatement(DELETE_NEWS);
            ps.setLong(1, newsId);
            ps.executeUpdate();
        } catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			key.releaseDAOKey(con, ps, null);
		}
	}

	
	public List<News> getNewsListByCriteria(SearchCriteria sc,long newsNFirst,
			long newsNLast) throws DAOException {
		Connection con=key.getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        News news = null;
        List<News> newsList = null;
        try {
        	String SQLText = getSQLByCriteriaAllFields(sc);
        	ps = con.prepareStatement(SQLText);
        	Long authorId=0L;
        	if (null!=sc){
            	authorId=sc.getAuthorId();
            	if (null==authorId){
            		authorId=0L;
            	}
        	};
        	
        	int move=0;
        	if (0!=authorId){
	        	ps.setLong(1, authorId);
	        	move+=1;
        	}
	        ps.setLong(1+move, newsNLast);
	        ps.setLong(2+move, newsNLast);
	        ps.setLong(3+move, newsNFirst);
            rs = ps.executeQuery();
            if (rs.next()) {
            	if (newsNLast>0){
            		newsList = new ArrayList<News>((int)(newsNLast-newsNFirst+1));
            	} else {
            		newsList = new ArrayList<News>(rs.getInt("NEWS_COUNT")-(int)newsNFirst+1);
            	}
               	news = new News(rs.getLong("NEWS_ID"), rs.getString("TITLE"),rs.getString("SHORT_TEXT"),
            			rs.getString("FULL_TEXT"),rs.getTimestamp("CREATION_DATE"),rs.getDate("MODIFICATION_DATE"));
            	newsList.add(news);
            };
            while (rs.next()) {
            	
            	news = new News(rs.getLong("NEWS_ID"), rs.getString("TITLE"),rs.getString("SHORT_TEXT"),
            			rs.getString("FULL_TEXT"),rs.getTimestamp("CREATION_DATE"),rs.getDate("MODIFICATION_DATE"));
            	newsList.add(news);
            }
        } catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			key.releaseDAOKey(con, ps, rs);
		}
        return newsList;
	}
	
	public long getNewsByCriteriaCount(SearchCriteria sc) throws DAOException {
		Connection con=key.getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        long count = 0;
        try {
        	String SQLText = getSQLByCriteriaCount(sc);
        	ps = con.prepareStatement(SQLText);


        	Long authorId=0L;
        	if (null!=sc){
            	authorId=sc.getAuthorId();
            	if (null==authorId){
            		authorId=0L;
            	}
        	};
        	
        	int move=0;
        	if (0!=authorId){
	        	ps.setLong(1, authorId);
	        	move+=1;
        	}
	        ps.setLong(1+move, 1L);
	        ps.setLong(2+move, 1L);
	        ps.setLong(3+move, 1L);
            rs = ps.executeQuery();
            if (rs.next()) {
            	count=rs.getLong("NEWS_COUNT");
            }
        } catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			key.releaseDAOKey(con, ps, rs);
		}
        return count;
	}
	
	

	public List<Long> getNewsIdsByCriteria(SearchCriteria sc,long newsNFirst,
			long newsNLast) throws DAOException {
		Connection con=key.getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<Long> newsList = null;
        try {
        	String SQLText = getSQLByCrineriaNewsIds(sc);
        	ps = con.prepareStatement(SQLText);
        	Long authorId = 0L;
        	if (null!=sc){
            	authorId=sc.getAuthorId();
            	if (null==authorId){
            		authorId=0L;
            	}
        	};
        	
        	int move=0;
        	if (0!=authorId){
	        	ps.setLong(1, authorId);
	        	move+=1;
        	}
	        ps.setLong(1+move, newsNLast);
	        ps.setLong(2+move, newsNLast);
	        ps.setLong(3+move, newsNFirst);
            rs = ps.executeQuery();
            if (rs.next()) {
	            if (newsNLast>0){
	        		newsList = new ArrayList<Long>((int)(newsNLast-newsNFirst+1));
	        	} else {
	        		newsList = new ArrayList<Long>(rs.getInt("NEWS_COUNT")-(int)newsNFirst+1);
	        	}
            	newsList.add(rs.getLong("NEWS_ID"));
            };
            while (rs.next()) {
            	newsList.add(rs.getLong("NEWS_ID"));
            }
        } catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			key.releaseDAOKey(con, ps, rs);
		}
        return newsList;
	}
	
	
	public int getNewsNumberByCriteria(SearchCriteria sc, long newsId) throws DAOException {
		Connection con=key.getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        int num = 0;
        try {
        	String SQLText = getSQLByCrineriaNewsNumber(sc);
        	ps = con.prepareStatement(SQLText);
        	Long authorId = 0L;
        	if (null!=sc){
            	authorId=sc.getAuthorId();
            	if (null==authorId){
            		authorId=0L;
            	}
        	};
        	
        	int move=0;
        	if (0!=authorId){
	        	ps.setLong(1, authorId);
	        	move+=1;
        	}
	        ps.setLong(1+move, 0);
	        ps.setLong(2+move, 0);
	        ps.setLong(3+move, 1);
	        ps.setLong(4+move, newsId);
            rs = ps.executeQuery();
            if (rs.next()) {
            	num = rs.getInt("NUM");
            };
        } catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			key.releaseDAOKey(con, ps, rs);
		}
        return num;
	}
	
	private String getSQLByCriteriaAllFields(SearchCriteria sc){
		return createSQLByCriteria(sc,GET_NEWS_BY_CRITERIA_NEWS_H);
	}
	
	private String getSQLByCriteriaCount(SearchCriteria sc){
		return createSQLByCriteria(sc,GET_NEWS_BY_CRITERIA_COUNT_H);
	}
	
	private String getSQLByCrineriaNewsIds(SearchCriteria sc){
		return createSQLByCriteria(sc,GET_NEWS_BY_CRITERIA_NEWS_IDS);
	}
	
	private String getSQLByCrineriaNewsNumber(SearchCriteria sc){
		return createSQLByCriteria(sc,GET_NEWS_BY_CRITERIA_NEWS_NUMBER)+" and NEWS_ID=?";
	}
	
	
	private String createSQLByCriteria(SearchCriteria sc,String header){
		
		
		StringBuilder sb=new StringBuilder(header);
		sb.append(GET_NEWS_BY_CRITERIA_P1);
    	Set<Long> tagIdList=null;
    	Long authorId=0L;
    	
    	if (null!=sc){
        	authorId=sc.getAuthorId();
        	if (null==authorId){
        		authorId=0L;
        	}
	    	tagIdList = sc.getTagIdSet();
    	}
    	
		if (0!=authorId){
			sb.append(GET_NEWS_BY_CRITERIA_P2);
			
		}
		if ((null!=tagIdList)&&(tagIdList.size()>0)){
			sb.append(GET_NEWS_BY_CRITERIA_P3);
	    	for (Long tagId : tagIdList) {
				sb.append(tagId+","); 
			}
	    	sb.delete(sb.length()-1, sb.length());
	    	sb.append(")");
		}

		sb.append(GET_NEWS_BY_CRITERIA_P4);
    	
    	return sb.toString();
	}
	
	
	
	//NewsAuthor
	
	public void addNewsAuthor(long newsId, long authorId) throws DAOException {
		Connection con = key.getConnection();
		PreparedStatement ps = null;
		try {
			ps = con.prepareStatement(ADD_NEWS_AUTHOR);
			ps.setLong(1, newsId);
			ps.setLong(2, authorId);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			key.releaseDAOKey(con, ps, null);
		}
	}

	public long getAuthorIdByNewsId(long newsId) throws DAOException {
		Connection con = key.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		long authorId=0;
		try {
			ps = con.prepareStatement(GET_AUTHOR_BY_NEWS);
			ps.setLong(1, newsId);
			rs = ps.executeQuery();
			if (rs.next()) {
				authorId=rs.getLong("AUTHOR_ID");
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			key.releaseDAOKey(con, ps, rs);
		}
		return authorId;
	}

	public void deleteNewsAuthor(long newsId) throws DAOException {
		Connection con = key.getConnection();
		PreparedStatement ps = null;
		try {
			ps = con.prepareStatement(DELETE_NEWS_AUTHOR_BY_NEWS_ID);
			ps.setLong(1, newsId);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			key.releaseDAOKey(con, ps, null);
		}
	}

	public List<Long> getNewsIdListByAuthorId(long authorId) throws DAOException {
		Connection con = key.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<Long> newsList = new ArrayList<Long>();
		try {
			ps = con.prepareStatement(GET_NEWS_LIST_BY_AUTHOR);
			ps.setLong(1, authorId);
			rs = ps.executeQuery();
			while (rs.next()) {
				newsList.add(rs.getLong("NEWS_ID"));
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			key.releaseDAOKey(con, ps, rs);
		}
		return newsList;
	}

	
	//News_Tag
	
	public void addNewsTag(long newsId, List<Long> tagIds) throws DAOException {
		Connection con = key.getConnection();
		PreparedStatement ps = null;
		try {
			ps = con.prepareStatement(ADD_NEWS_TAG);
			//ps.setLong(1, newsId);
			for(long tagId:tagIds){

				ps.setLong(1, newsId);
				ps.setLong(2, tagId);
				ps.addBatch();
				//ps.setLong(2, tagId);
				//ps.executeUpdate();
			}
			ps.executeBatch();
			
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			key.releaseDAOKey(con, ps, null);
		}
	}
	
	public boolean getNewsTagExistance(long newsId,long tagId) throws DAOException {
		Connection con = key.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		boolean exists = false;
		try {
			ps = con.prepareStatement(DOES_NEWS_TAG_EXIST);
			ps.setLong(1, newsId);
			ps.setLong(2, tagId);
			rs = ps.executeQuery();
			if (rs.next()) {
				exists = true;
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			key.releaseDAOKey(con, ps, rs);
		}
		return exists;
	}

	public List<Long> getNewsIdListByTagId(long tagId) throws DAOException {
		Connection con = key.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<Long> newsList = new ArrayList<Long>();
		try {
			ps = con.prepareStatement(GET_NEWS_LIST_BY_TAG);
			ps.setLong(1, tagId);
			rs = ps.executeQuery();
			while (rs.next()) {
				newsList.add(rs.getLong("NEWS_ID"));
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			key.releaseDAOKey(con, ps, rs);
		}
		return newsList;
	}

	public void deleteNewsTag(long newsId, long tagId) throws DAOException {
		Connection con = key.getConnection();
		PreparedStatement ps = null;
		try {
			ps = con.prepareStatement(DELETE_NEWS_TAG);
			ps.setLong(1, newsId);
			ps.setLong(2, tagId);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			key.releaseDAOKey(con, ps, null);
		}
	}

	public void deleteNewsTagByNewsId(long newsId) throws DAOException {
		Connection con = key.getConnection();
		PreparedStatement ps = null;
		try {
			ps = con.prepareStatement(DELETE_NEWS_TAG_BY_NEWS_ID);
			ps.setLong(1, newsId);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			key.releaseDAOKey(con, ps, null);
		}
	}
	
	
	public void deleteNewsTagByTagId(long tagId) throws DAOException {
		Connection con = key.getConnection();
		PreparedStatement ps = null;
		try {
			ps = con.prepareStatement(DELETE_NEWS_TAG_BY_TAG_ID);
			ps.setLong(1, tagId);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			key.releaseDAOKey(con, ps, null);
		}
	}
	
	public List<Tag> getTagListByNewsId(long newsId) throws DAOException {
		Connection con = key.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		Tag tag;
		List<Tag> tagList = new ArrayList<Tag>();
		try {
			ps = con.prepareStatement(GET_TAG_LIST_BY_NEWS);
			ps.setLong(1, newsId);
			rs = ps.executeQuery();
			while (rs.next()) {
				tag = new Tag(rs.getLong("TAG_ID"),rs.getString("TAG_NAME"));
				tagList.add(tag);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			key.releaseDAOKey(con, ps, rs);
		}
		return tagList;
	}
}
