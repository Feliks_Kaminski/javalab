package by.epam.newsmanagement.service.impl;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;

import by.epam.newsmanagement.App;
import by.epam.newsmanagement.dao.IDAOAuthor;
import by.epam.newsmanagement.dao.IDAOComment;
import by.epam.newsmanagement.dao.IDAONews;
import by.epam.newsmanagement.dao.IDAOTag;
import by.epam.newsmanagement.dao.IDAOUser;
import by.epam.newsmanagement.entity.Author;
import by.epam.newsmanagement.entity.Comment;
import by.epam.newsmanagement.entity.News;
import by.epam.newsmanagement.entity.NewsInfoHolder;
import by.epam.newsmanagement.entity.Tag;
import by.epam.newsmanagement.exceptions.DAOException;
import by.epam.newsmanagement.exceptions.ServiceException;
import by.epam.newsmanagement.service.IServiceExtended;

@Transactional
public class ServiceExtended implements IServiceExtended {
    public static Logger logger = Logger.getLogger(App.class.getName());
	private IDAOAuthor authorDAO;
	private IDAOComment commentDAO;
	private IDAONews newsDAO;
	private IDAOTag tagDAO;
	private IDAOUser userDAO;
	
	public void setAuthorDAO(IDAOAuthor authorDAO) {
		this.authorDAO = authorDAO;
	}
	public void setCommentDAO(IDAOComment commentDAO) {
		this.commentDAO = commentDAO;
	}
	public void setNewsDAO(IDAONews newsDAO) {
		this.newsDAO = newsDAO;
	}
	public void setTagDAO(IDAOTag tagDAO) {
		this.tagDAO = tagDAO;
	}
	
	public void setUserDAO(IDAOUser userDAO) {
		this.userDAO = userDAO;
	}
	
	@Transactional(readOnly=true)
	public List<NewsInfoHolder> getNewsInfoHolderList(List<Long> newsIds) throws ServiceException {
		List<NewsInfoHolder> info = new ArrayList<NewsInfoHolder>();
		try {
			Author author;
			News news;
			List<Comment> comments;
			List<Tag> tags;
			NewsInfoHolder holder;
			for(Long newsId : newsIds){
				author = authorDAO.getAuthor(newsDAO.getAuthorIdByNewsId(newsId));
				news = newsDAO.getNews(newsId);
				comments= commentDAO.getCommentsListByNewsId(newsId, 1, 0);
				tags = newsDAO.getTagListByNewsId(newsId);
				holder = new NewsInfoHolder(author, news, comments, tags);
				info.add(holder);
			}
		} catch (DAOException e){
			logger.error(e);
			throw(new ServiceException(e));
		}
		return info;
	}
	
	
	public long addNewsWithDependencies(long authorId,String title, String shortText, 
			String fullText, Date modificationDate, List<Long> tags) throws ServiceException {
		long idN;
		try {
			idN=newsDAO.addNews(title, shortText, fullText, modificationDate);
			newsDAO.addNewsAuthor(idN, authorId);
			newsDAO.addNewsTag(idN, tags);
		} catch (DAOException e){
			logger.error(e);
			throw(new ServiceException(e));
		}
		return idN;
	}
	
	public void editNewsWithDependencies(long authorId, long newsId, String title, String shortText, 
			String fullText, Date modificationDate, List<Long> tags) throws ServiceException {
		try {
			News news = newsDAO.getNews(newsId);
			
			news.setTitle(title);
			news.setShortText(shortText);
			news.setFullText(fullText);
			news.setModificationDate(modificationDate);

			newsDAO.editNews(news);
			
			newsDAO.deleteNewsAuthor(newsId);
			newsDAO.addNewsAuthor(newsId, authorId);
			
			newsDAO.deleteNewsTagByNewsId(newsId);
			newsDAO.addNewsTag(newsId, tags);
		} catch (DAOException e){
			logger.error(e);
			throw(new ServiceException(e));
		}
	}
	
	
	
	public void deleteNewsWithDependencies(long newsId) throws ServiceException{
		try {
			newsDAO.deleteNewsAuthor(newsId);
			
			newsDAO.deleteNewsTagByNewsId(newsId);
			
			commentDAO.deleteCommentsByNewsId(newsId);
			
			newsDAO.deleteNews(newsId);
		} catch (DAOException e){
			logger.error(e);
			throw(new ServiceException(e));
		}
	}
	

	public void deleteTagWithDependencies(long tagId) throws ServiceException {
		try {
			
			newsDAO.deleteNewsTagByTagId(tagId);
			tagDAO.deleteTag(tagId);
		} catch (DAOException e){
			logger.error(e);
			throw(new ServiceException(e));
		}
	}
	
	
	public void deleteUserWithDependencies(long userId) throws ServiceException {
		try {
			userDAO.deleteUserRole(userId);
			userDAO.deleteUser(userId);
		} catch (DAOException e){
			logger.error(e);
			throw(new ServiceException(e));
		}
	}
	
}
