package by.epam.newsmanagement.dao;

import java.util.List;

import by.epam.newsmanagement.entity.Tag;
import by.epam.newsmanagement.exceptions.DAOException;

/**
 * This is an interface for basic one-step operations with defined data storage (ds) on Tag entity.
 * 
 * @author Feliks_Kaminski
 *
 */
public interface IDAOTag{

	/**
	 * The method inserts a Tag entity into the ds and returns it's id.
	 * 
	 * @param name
	 * @return id of the Tag entity
	 * @throws DAOException
	 */
	public long addTag(String name) throws DAOException;
	
	/**
	 * The method gets a Tag entity and updates it's fields in the ds using it's id.
	 * 
	 * @param Tag entity
	 * @throws DAOException
	 */
	public void editTag(Tag tag) throws DAOException;

	/**
	 * The method returns a Tag entity using it's unique name.
	 * 
	 * @param name
	 * @return Tag entity
	 * @throws DAOException
	 */
	public Tag getTag(String name) throws DAOException;
	
	/**
	 * The method returns a Tag entity using it's id.
	 * 
	 * @param tagId
	 * @return Tag entity
	 * @throws DAOException
	 */
	public Tag getTag(long tagId) throws DAOException;

	/**
	 * The method removes a Tag from the ds using it's id.
	 * 
	 * @param tagId
	 * @throws DAOException
	 */
	public void deleteTag(long tagId) throws DAOException;

	/**
	 * The method returns a list of Tag entities.
	 * 
	 * @return list with Tag entities
	 * @throws DAOException
	 */
	public List<Tag> getTagList() throws DAOException;
}
