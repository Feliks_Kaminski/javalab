package by.epam.newsmanagement.dao;

import java.util.List;

import by.epam.newsmanagement.entity.Author;
import by.epam.newsmanagement.exceptions.DAOException;

/**
 * This is an interface for basic one-step operations with defined data storage (ds) on Author entity.
 * 
 * @author Feliks_Kaminski
 *
 */
public interface IDAOAuthor {
		/**
		 * The method inserts an Author entity into the ds and returns it's id.
		 * 
		 * @param name of the Author
		 * @return Author's id
		 * @throws DAOException
		 */
	public long addAuthor(String name) throws DAOException;

		/**
		 * The method gets an Author entity and updates it's fields in the ds using it's id.
		 * 
		 * @param Author entity
		 * @throws DAOException
		 */
	public void editAuthor(Author author) throws DAOException;

		/**
		 * The method returns an Author entity using it's id.
		 * 
		 * @param authorId
		 * @throws DAOException
		 */
	public Author getAuthor(long authorId) throws DAOException;

		/**
		 * The method returns an Author entity using it's unique name.
		 * 
		 * @param name
		 * @return
		 * @throws DAOException
		 */
	public Author getAuthor(String name) throws DAOException;

		/**
		 * The method removes an Author from the ds using it's id.
		 * 
		 * @param authorId
		 * @throws DAOException
		 */
	public void deleteAuthor(long authorId) throws DAOException;

		/**
		 * The method returns a list of non-expired Author entities ordered by name.
		 * 
		 * @return list with Author entities
		 * @throws DAOException
		 */
	public List<Author> getAuthorList() throws DAOException;
}
