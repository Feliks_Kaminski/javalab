package by.epam.newsmanagement.dao.impl;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;



import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import by.epam.newsmanagement.dao.IDAOTag;
import by.epam.newsmanagement.entity.Tag;
import by.epam.newsmanagement.exceptions.DAOException;


@Transactional(propagation = Propagation.MANDATORY)
public class DAOTag implements IDAOTag{
	private static final String ADD_TAG = "insert into TAG (TAG_ID,TAG_NAME) values (seq_tag_id.nextval,?)";
	private static final String UPDATE_TAG = "update TAG set TAG_NAME=? where TAG_ID=?";
	private static final String GET_TAG_BY_ID = "select TAG_NAME from TAG where TAG_ID=?";
	private static final String GET_TAG_BY_NAME = "select TAG_ID from TAG where TAG_NAME=?";
	private static final String DELETE_TAG = "delete from TAG where TAG_ID=?";
	private static final String GET_TAG_LIST = "select TAG_ID,TAG_NAME from TAG";
	
	private ADAOKey key;
	
	public DAOTag(ADAOKey key){
		this.key=key;
	}
	
	public long addTag(String name) throws DAOException {
		Connection con = key.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		long id = 0;
		try {
			ps = con.prepareStatement(ADD_TAG, new String[] { "TAG_ID" });
			ps.setString(1, name);
			ps.executeUpdate();
			rs = ps.getGeneratedKeys();
			if (rs.next()) {
				id=rs.getLong(1);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			key.releaseDAOKey(con, ps, rs);
		}
		return id;
	}
	

	public void editTag(Tag tag) throws DAOException{
		Connection con = key.getConnection();
		PreparedStatement ps = null;
		try {
			ps = con.prepareStatement(UPDATE_TAG);
			ps.setString(1, tag.getName());
			ps.setLong(2, tag.getId());
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			key.releaseDAOKey(con, ps, null);
		}
	}

	public Tag getTag(String name) throws DAOException {
		Connection con = key.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		Tag tag = null;
		try {
			ps = con.prepareStatement(GET_TAG_BY_NAME);
			ps.setString(1, name);
			rs = ps.executeQuery();
			if (rs.next()) {
				tag = new Tag(rs.getLong("TAG_ID"),name);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			key.releaseDAOKey(con, ps, rs);
		}
		return tag;
	}

	public Tag getTag(long tagId) throws DAOException {
		Connection con = key.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		Tag tag = null;
		try {
			ps = con.prepareStatement(GET_TAG_BY_ID);
			ps.setLong(1, tagId);
			rs = ps.executeQuery();
			if (rs.next()) {
				tag = new Tag(tagId,rs.getString("TAG_NAME"));
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			key.releaseDAOKey(con, ps, rs);
		}
		return tag;
	}

	public void deleteTag(long tagId) throws DAOException {
		Connection con = key.getConnection();
		PreparedStatement ps = null;
		try {
			ps = con.prepareStatement(DELETE_TAG);
			ps.setLong(1, tagId);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			key.releaseDAOKey(con, ps, null);
		}
	}

	public List<Tag> getTagList() throws DAOException {
		Connection con = key.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		Tag tag = null;
		List<Tag> tagList = new ArrayList<Tag>();
		try {
			ps = con.prepareStatement(GET_TAG_LIST);
			rs = ps.executeQuery();
			while (rs.next()) {
				tag = new Tag(rs.getLong("TAG_ID"),rs.getString("TAG_NAME"));
				tagList.add(tag);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			key.releaseDAOKey(con, ps, rs);
		}
		return tagList;
	}

}
