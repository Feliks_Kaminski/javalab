package by.epam.newsmanagement.entity;

import java.util.List;

public class NewsInfoHolder {
	private Author author;
	private News news;
	private List<Comment> comments;
	private List<Tag> tags;
	
	public NewsInfoHolder(Author author, News news, List<Comment> comments, List<Tag> tags){
		this.author=author;
		this.news = news;
		this.comments=comments;
		this.tags=tags;
	}
	
	public Author getAuthor(){
		return author;
	}
	
	public void setAuhor(Author author){
		this.author=author;
	}
	
	
	public News getNews(){
		return news;
	}
	
	public void setNews(News news){
		this.news=news;
	}
	

	public List<Comment> getComments(){
		return comments;
	}
	
	public void setComments(List<Comment> comments){
		this.comments = comments;;
	}


	public List<Tag> getTags(){
		return tags;
	}
	
	public void setTags(List<Tag> tags){
		this.tags=tags;
	}
	
}
