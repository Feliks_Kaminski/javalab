package by.epam.newsmanagement.service;

import java.sql.Date;
import java.util.List;

import by.epam.newsmanagement.entity.NewsInfoHolder;
import by.epam.newsmanagement.exceptions.ServiceException;

/**
 * This Service is used for providing complex multiDAO commands.
 * 
 * @author Feliks_Kaminski
 *
 */
public interface IServiceExtended {
	
	
	
	/**
	 * The method returs full information about news by news ids.
	 * 
	 * @param newsIds
	 * @return List of NewsInfoHolder entities
	 * @throws ServiceException
	 */
	public List<NewsInfoHolder> getNewsInfoHolderList(List<Long> newsIds) throws ServiceException;
	
	/**
	 * This method creates News entity with Author and Tag dependencies in one step and returns it's id.
	 * 
	 * @param authorId
	 * @param title
	 * @param shortText
	 * @param fullText
	 * @param tags
	 * @param modificationDate
	 * @return News id
	 * @throws ServiceException
	 */
	public long addNewsWithDependencies(long authorId,String title, String shortText, 
			String fullText, Date modificationDate, List<Long> tags) throws ServiceException;
	
	
	
	
	/**
	 * This method edits News entity with Author and Tag dependencies in one step.
	 * 
	 * @param authorId
	 * @param newsId
	 * @param title
	 * @param shortText
	 * @param fullText
	 * @param modificationDate
	 * @param tags
	 * @throws ServiceException
	 */
	public void editNewsWithDependencies(long authorId, long newsId, String title, String shortText, 
			String fullText, Date modificationDate, List<Long> tags) throws ServiceException;
	
	
	
	/**
	 * The method deletes a News entity with all dependent Comment entities and Tag and Author dependencies.
	 * 
	 * @param newsId
	 * @throws ServiceException
	 */
	public void deleteNewsWithDependencies(long newsId) throws ServiceException;
	

	/**
	 * This method deletes Tag entity and NewsTag dependencies.
	 * 
	 * @param tagId
	 * @throws ServiceException
	 */
	public void deleteTagWithDependencies(long tagId) throws ServiceException;
	
	
	
	/**
	 * This method deletes User entity and his role.
	 * 
	 * @param userId
	 * @throws ServiceException
	 */
	public void deleteUserWithDependencies(long userId) throws ServiceException;
}
