package by.epam.newsmanagement.service.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;

import by.epam.newsmanagement.App;
import by.epam.newsmanagement.dao.IDAOAuthor;
import by.epam.newsmanagement.entity.Author;
import by.epam.newsmanagement.exceptions.DAOException;
import by.epam.newsmanagement.exceptions.ServiceException;
import by.epam.newsmanagement.service.IServiceAuthor;

@Transactional
public class ServiceAuthor implements IServiceAuthor{
    public static Logger logger = Logger.getLogger(App.class.getName());
	private IDAOAuthor authorDAO;
	
	public ServiceAuthor(IDAOAuthor authorDAO){
		this.authorDAO=authorDAO;
	}
	
	public long addAuthor(String name) throws ServiceException{
		try {
			return authorDAO.addAuthor(name);
		} catch (DAOException e){
			logger.error(e);
			throw(new ServiceException(e));
		}
	}

	public void editAuthor(Author author) throws ServiceException{
		try {
			authorDAO.editAuthor(author);
		} catch (DAOException e){
			logger.error(e);
			throw(new ServiceException(e));
		}
	}

	@Transactional(readOnly=true)
	public Author getAuthor(long authorId) throws ServiceException{
		try {
			return authorDAO.getAuthor(authorId);
		} catch (DAOException e){
			logger.error(e);
			throw(new ServiceException(e));
		}
	}

	@Transactional(readOnly=true)
	public Author getAuthor(String name) throws ServiceException{
		try {
			return authorDAO.getAuthor(name);
		} catch (DAOException e){
			logger.error(e);
			throw(new ServiceException(e));
		}
	}

	public void deleteAuthor(long authorId) throws ServiceException{
		try {
			authorDAO.deleteAuthor(authorId);
		} catch (DAOException e){
			logger.error(e);
			throw(new ServiceException(e));
		}
	}

	@Transactional(readOnly=true)
	public List<Author> getAuthorList() throws ServiceException{
		try {
			return authorDAO.getAuthorList();
		} catch (DAOException e){
			logger.error(e);
			throw(new ServiceException(e));
		}
	}
	
}
