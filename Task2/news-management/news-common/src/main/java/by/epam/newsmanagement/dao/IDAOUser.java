package by.epam.newsmanagement.dao;

import by.epam.newsmanagement.entity.User;
import by.epam.newsmanagement.exceptions.DAOException;

/**
 * This is an interface for basic one-step operations with defined data storage (ds) on User entity.
 * 
 * @author Feliks_Kaminski
 *
 */
public interface IDAOUser {
	
	
	/**
	 * The method inserts a User entity into the ds and returns it's id.
	 * 
	 * @param userName
	 * @param login
	 * @param password
	 * @return User id
	 * @throws DAOException
	 */
	public long addUser(String userName, String login, String password)
			throws DAOException;

	/**
	 * The method gets a User entity and updates it's fields in the ds using it's id.
	 * 
	 * @param user
	 * @throws DAOException
	 */
	public void editUser(User user) throws DAOException;

	/**
	 * The method returns a User entity using it's name.
	 * 
	 * @param login
	 * @return User entity
	 * @throws DAOException
	 */
	public User getUser(String login) throws DAOException;

	/**
	 * The method returns a User entity using it's id.
	 * 
	 * @param userId
	 * @return User entity
	 * @throws DAOException
	 */
	public User getUserById(long userId) throws DAOException;

	/**
	 * The method removes a User from the ds using it's id.
	 * 
	 * @param userId
	 * @throws DAOException
	 */
	public void deleteUser(long userId) throws DAOException;

	/**
	 * The method returns a User Role using it's id.
	 * 
	 * @param userId
	 * @return String userRole
	 * @throws DAOException
	 */
	public String getUserRole(long userId) throws DAOException;

	/**
	 *  The method inserts a User Role using it's id.
	 * 
	 * @param userId
	 * @param role
	 * @throws DAOException
	 */
	public void addUserRole(long userId, String role) throws DAOException;

	/**
	 *  The method updates a User Role using it's id.
	 * 
	 * @param userId
	 * @param role
	 * @throws DAOException
	 */
	public void updateUserRole(long userId, String role) throws DAOException;

	/**
	 *  The method deletes a User Role using it's id.
	 * 
	 * @param userId
	 * @throws DAOException
	 */
	public void deleteUserRole(long userId) throws DAOException;

}
