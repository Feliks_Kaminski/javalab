package by.epam.newsmanagement.service;

import java.sql.Date;
import java.util.List;

import by.epam.newsmanagement.entity.News;
import by.epam.newsmanagement.entity.SearchCriteria;
import by.epam.newsmanagement.entity.Tag;
import by.epam.newsmanagement.exceptions.ServiceException;

/**
 * This is an interface for operations on News entity.
 * 
 * @author Feliks_Kaminski
 *
 */
public interface IServiceNews {
	
	/**
	 * The method inserts a News entity and returns it's id.
	 * 
	 * @param title
	 * @param shortText
	 * @param fullText
	 * @return news id
	 * @throws ServiceException
	 */
	public long addNews(String title, String shortTtext, String fullText, Date modificationDate) throws ServiceException;

	/**
	 * The method gets a News entity and updates it's fields using it's id.
	 * 
	 * @param News entity
	 * @throws DAOException
	 */
	public void editNews(News news) throws ServiceException;

	/**
	 * The method returns a News entity using it's id.
	 * 
	 * @param newsId
	 * @return News entity
	 * @throws DAOException
	 */
	public News getNews(long newsId) throws ServiceException;

	/**
	 * The method removes a News using it's id.
	 * 
	 * @param newsId
	 * @throws DAOException
	 */
	public void deleteNews(long newsId) throws ServiceException;


	/**
	 * The method returns a list of News entities from list of all news ordered by by tags count,count of comments desc and creationDate desc.
	 * Requires number of the first and last (0 - News at the end of the list) News number you want.
	 * 
	 * @param SearchCriteria entity or null
	 * @param newsNFirst
	 * @param newsNLast
	 * @return list of News
	 * @throws ServiceException
	 */
	public List<News> getNewsListByCriteria(SearchCriteria sc,long newsNFirst,
			long newsNLast) throws ServiceException;
	
	/**
	 * The method returns the count of News entities from list of all news ordered by count of comments desc and creationDate desc
	 * 
	 * @param SearchCriteria entity
	 * @return count of News
	 * @throws DAOException
	 */
	public long getNewsByCriteriaCount(SearchCriteria sc) throws ServiceException;
	
	/**
	 * The method returns a list of News ids from list of all news ordered by tags count,count of comments desc and creationDate desc
	 * Requires number of the first and last (0 - News at the end of the list) News number you want.
	 * 
	 * 
	 * @param SearchCriteria entity or null
	 * @param newsNFirst
	 * @param newsNLast
	 * @return list of News ids
	 * @throws DAOException
	 */
	public List<Long> getNewsIdsByCriteria(SearchCriteria sc,long newsNFirst,
			long newsNLast) throws ServiceException;
	
	
	/**
	 * The method returns a number of News from list of all news ordered by tags count,count of comments desc and creationDate desc
	 * Requires id of the news.
	 * 
	 * 
	 * @param SearchCriteria entity or null
	 * @param newsId
	 * @return list of News ids
	 * @throws DAOException
	 */
	public int getNewsNumberByCriteria(SearchCriteria sc, long newsId) throws ServiceException;
	
	//NewsAuthor
	
	/**
	 * This method adds dependency between News and Author
	 * 
	 * @param newsId
	 * @param authorId
	 * @throws ServiceException
	 */
	public void addNewsAuthor(long newsId, long authorId) throws ServiceException;

	/**
	 * This method gets Author id by News id
	 * 
	 * @param newsId
	 * @return Author id
	 * @throws ServiceException
	 */
	public long getAuthorIdByNewsId(long newsId) throws ServiceException;

	/**
	 * This method deletes dependency between News and Author
	 * 
	 * @param newsId
	 * @param authorId
	 * @throws ServiceException
	 */
	public void deleteNewsAuthor(long newsId) throws ServiceException;

	/**
	 * This method returns list of News entity by Author id
	 * 
	 * @param authorId
	 * @return list of News
	 * @throws ServiceException
	 */
	public List<Long> getNewsIdListByAuthorId(long authorId) throws ServiceException;
	
	//News_Tag
	
	/**
	 * This method adds dependency between News and Tag
	 * 
	 * @param newsId
	 * @param tagIds
	 * @throws ServiceException
	 */
	public void addNewsTag(long newsId, List<Long> tagIds) throws ServiceException;

	/**
	 * This method checks dependent between news and tag
	 * 
	 * @param newsId
	 * @param tagId
	 * @return true if combination exists
	 * @throws DAOException
	 */
	public boolean getNewsTagExistance(long newsId,long tagId) throws ServiceException;
	
	
	/**
	 * This method gets News ids by Tag id
	 * 
	 * @param tagId
	 * @return list of News ids
	 * @throws ServiceException
	 */
	public List<Long> getNewsIdListByTagId(long tagId) throws ServiceException;

	/**
	 * The method removes a News-Tag dependency using it's ids.
	 * 
	 * @param newsId
	 * @param tagId
	 * @throws ServiceException
	 */
	public void deleteNewsTag(long newsId, long tagId) throws ServiceException;

	/**
	 * The method removes News-Tag dependencies from the ds using news id.
	 * 
	 * @param newsId
	 * @throws DAOException
	 */
	public void deleteNewsTagByNewsId(long newsId) throws ServiceException;
	
	/**
	 * The method removes News-Tag dependencies from the ds using tag id.
	 * 
	 * @param tagId
	 * @throws DAOException
	 */
	public void deleteNewsTagByTagId(long tagId) throws ServiceException;
	
	
	/**
	 * This method returns list of Tags ids by News id
	 * 
	 * @param newsId
	 * @return List of Tag ids
	 * @throws ServiceException
	 */
	public List<Tag> getTagListByNewsId(long newsId) throws ServiceException;
}
