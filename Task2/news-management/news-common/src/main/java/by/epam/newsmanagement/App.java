package by.epam.newsmanagement;

/**
 * Hello world!
 *
 */
import java.security.*;
import java.math.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.log4j.LogManager;
import org.apache.log4j.xml.DOMConfigurator;
import org.apache.log4j.Logger;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import by.epam.newsmanagement.entity.SearchCriteria;
import by.epam.newsmanagement.entity.Tag;
import by.epam.newsmanagement.exceptions.ServiceException;
import by.epam.newsmanagement.service.IServiceAuthor;
import by.epam.newsmanagement.service.IServiceExtended;
import by.epam.newsmanagement.service.IServiceNews;


public class App 
{
    public static Logger logger = Logger.getLogger(App.class.getName());

    static {
        //new DOMConfigurator().doConfigure("src/main/resources/log4j.xml", LogManager.getLoggerRepository());
    }
    public static void main( String[] args ) throws ServiceException, NoSuchAlgorithmException
    {
    	//Locale.setDefault(Locale.ENGLISH);
    	/*
    	System.out.print(Locale.getDefault());
		ClassPathXmlApplicationContext context = 
				new ClassPathXmlApplicationContext("spring.xml");
		IServiceNews sn = context.getBean("spr_IServiceNews",IServiceNews.class);

    	System.out.print(sn.getNewsNumberByCriteria(null, 0));
		context.close();
		
*/

    	ArrayList<Long> a1 = new ArrayList<Long>();
        ArrayList<Long> a2 = new ArrayList<Long>();
        a1.add(1L);
        a1.add(2L);
        a2.add(3L);
        a2.add(4L);
        
        a1.ensureCapacity(a1.size()*2);
        for (int i = 1;i<a1.size()*2;i=i-1){
            a1.set(i<<1, a1.get(i-1));
        }
        System.out.println(a1);
    	
		/*String s="This is a test";
        MessageDigest m=MessageDigest.getInstance("MD5");
        m.update(s.getBytes(),0,s.length());
        System.out.println("MD5: "+new BigInteger(1,m.digest()).toString(16));
        */
    }
    
}
