package by.epam.newsmanagement.entity;

import java.sql.Timestamp;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class Comment {
	private final long id;
	private final long newsId;

	@NotNull
	@Size(min = 1, max = 30)
	@Pattern(regexp = "[A-Za-z0-9]")
	private String commentText;
	@NotNull
	private final Timestamp creationDate;

	public Comment(long id, long newsId, String commentText,
			Timestamp creationDate) {
		this.id = id;
		this.newsId = newsId;
		this.commentText = commentText;
		this.creationDate = creationDate;
	}

	public long getId() {
		return id;
	}

	public long getNewsId() {
		return newsId;
	}

	public String getCommentText() {
		return commentText;
	}

	public void setCommentText(String commentText) {
		this.commentText = commentText;
	}

	public Timestamp getCreationDate() {
		return creationDate;
	}

	public String toString() {
		return commentText + "(" + id + ")"
				+ (creationDate == null ? "" : creationDate);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((commentText == null) ? 0 : commentText.hashCode());
		result = prime * result
				+ ((creationDate == null) ? 0 : creationDate.hashCode());
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + (int) (newsId ^ (newsId >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Comment other = (Comment) obj;
		if (commentText == null) {
			if (other.commentText != null)
				return false;
		} else if (!commentText.equals(other.commentText))
			return false;
		if (creationDate == null) {
			if (other.creationDate != null)
				return false;
		} else if (!creationDate.equals(other.creationDate))
			return false;
		if (id != other.id)
			return false;
		if (newsId != other.newsId)
			return false;
		return true;
	}

}
