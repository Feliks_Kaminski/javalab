package by.epam.newsmanagement.service.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;

import by.epam.newsmanagement.App;
import by.epam.newsmanagement.dao.IDAOComment;
import by.epam.newsmanagement.entity.Comment;
import by.epam.newsmanagement.exceptions.DAOException;
import by.epam.newsmanagement.exceptions.ServiceException;
import by.epam.newsmanagement.service.IServiceComment;

@Transactional
public class ServiceComment implements IServiceComment{
    public static Logger logger = Logger.getLogger(App.class.getName());
	private IDAOComment commentDAO;
	
	public ServiceComment(IDAOComment commentDAO){
		this.commentDAO=commentDAO;
	}
	
	public long addComment(long newsId, String comment_text) throws ServiceException{
		try {
			return commentDAO.addComment(newsId, comment_text);
		} catch (DAOException e){
			logger.error(e);
			throw(new ServiceException(e));
		}
	}

	public void editComment(Comment comment) throws ServiceException{
		try {
			commentDAO.editComment(comment);
		} catch (DAOException e){
			logger.error(e);
			throw(new ServiceException(e));
		}
	}


	@Transactional(readOnly=true)
	public Comment getComment(long commentId) throws ServiceException{
		try {
			return commentDAO.getComment(commentId);
		} catch (DAOException e){
			logger.error(e);
			throw(new ServiceException(e));
		}
	}

	public void deleteComment(long commentId) throws ServiceException{
		try {
			commentDAO.deleteComment(commentId);
		} catch (DAOException e){
			logger.error(e);
			throw(new ServiceException(e));
		}
	}


	public void deleteCommentsByNewsId(long newsId) throws ServiceException{
		try {
			commentDAO.deleteCommentsByNewsId(newsId);
		} catch (DAOException e){
			logger.error(e);
			throw(new ServiceException(e));
		}
	}

	@Transactional(readOnly=true)
	public List<Comment> getCommentsListByNewsId(long newsId,
			long commentNFirst, long commentNLast) throws ServiceException{
		try {
			return commentDAO.getCommentsListByNewsId(newsId, commentNFirst, commentNLast);
		} catch (DAOException e){
			logger.error(e);
			throw(new ServiceException(e));
		}
	}
}
