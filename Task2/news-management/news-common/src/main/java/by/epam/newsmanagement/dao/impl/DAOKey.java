package by.epam.newsmanagement.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.DataSourceUtils;

import by.epam.newsmanagement.exceptions.DAOException;

class DAOKey extends ADAOKey {
	private DataSource dataSource;

	DAOKey(DataSource dataSource) {
		this.dataSource=dataSource;
	}

	void releaseDAOKey(Connection con,PreparedStatement ps,ResultSet rs) throws DAOException {
		try{
			if (null!=rs) rs.close();
			if (null!=ps) ps.close();
		} catch (SQLException e){
			throw new DAOException(e);
		}
		finally{
			DataSourceUtils.releaseConnection(con, dataSource);
		}
	}

	Connection getConnection() {
		return DataSourceUtils.getConnection(dataSource);
	}
	
	
}
