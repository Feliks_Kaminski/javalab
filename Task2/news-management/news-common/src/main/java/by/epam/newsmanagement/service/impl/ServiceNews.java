package by.epam.newsmanagement.service.impl;

import java.sql.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;

import by.epam.newsmanagement.App;
import by.epam.newsmanagement.dao.IDAONews;
import by.epam.newsmanagement.entity.News;
import by.epam.newsmanagement.entity.SearchCriteria;
import by.epam.newsmanagement.entity.Tag;
import by.epam.newsmanagement.exceptions.DAOException;
import by.epam.newsmanagement.exceptions.ServiceException;
import by.epam.newsmanagement.service.IServiceNews;

@Transactional
public class ServiceNews implements IServiceNews{
    public static Logger logger = Logger.getLogger(App.class.getName());
	private IDAONews newsDAO;

	public ServiceNews(IDAONews newsDAO){
		this.newsDAO=newsDAO;
	}

	public long addNews(String title, String shortText, String fullText, Date modificationDate) throws ServiceException{
		try {
			return newsDAO.addNews(title, shortText, fullText, modificationDate);
		} catch (DAOException e){
			logger.error(e);
			throw(new ServiceException(e));
		}
	}

	public void editNews(News news) throws ServiceException{
		try {
			newsDAO.editNews(news);
		} catch (DAOException e){
			logger.error(e);
			throw(new ServiceException(e));
		}
	}

	@Transactional(readOnly=true)
	public News getNews(long newsId) throws ServiceException{
		try {
			return newsDAO.getNews(newsId);
		} catch (DAOException e){
			logger.error(e);
			throw(new ServiceException(e));
		}
	}

	public void deleteNews(long newsId) throws ServiceException{
		try {
			newsDAO.deleteNews(newsId);
		} catch (DAOException e){
			logger.error(e);
			throw(new ServiceException(e));
		}
	}


	@Transactional(readOnly=true)
	public List<News> getNewsListByCriteria(SearchCriteria sc,long newsNFirst,
			long newsNLast) throws ServiceException{
		try {
			return newsDAO.getNewsListByCriteria(sc,newsNFirst,newsNLast);
		} catch (DAOException e){
			logger.error(e);
			throw(new ServiceException(e));
		}
	}

	@Transactional(readOnly=true)
	public long getNewsByCriteriaCount(SearchCriteria sc) throws ServiceException {
		try {
			return newsDAO.getNewsByCriteriaCount(sc);
		} catch (DAOException e){
			logger.error(e);
			throw(new ServiceException(e));
		}
	}
	
	
	@Transactional(readOnly=true)
	public List<Long> getNewsIdsByCriteria(SearchCriteria sc,long newsNFirst,long newsNLast) throws ServiceException{
		try {
			return newsDAO.getNewsIdsByCriteria(sc, newsNFirst, newsNLast);
		} catch (DAOException e){
			logger.error(e);
			throw(new ServiceException(e));
		}
	}

	@Transactional(readOnly=true)
	public int getNewsNumberByCriteria(SearchCriteria sc, long newsId) throws ServiceException{
		try {
			return newsDAO.getNewsNumberByCriteria(sc, newsId);
		} catch (DAOException e){
			logger.error(e);
			throw(new ServiceException(e));
		}
	}
			
	//NewsAuthor
	
	public void addNewsAuthor(long newsId, long authorId) throws ServiceException{
		try {
			newsDAO.addNewsAuthor(newsId, authorId);
		} catch (DAOException e){
			logger.error(e);
			throw(new ServiceException(e));
		}
	}

	@Transactional(readOnly=true)
	public long getAuthorIdByNewsId(long newsId) throws ServiceException{
		try {
			return newsDAO.getAuthorIdByNewsId(newsId);
		} catch (DAOException e){
			logger.error(e);
			throw(new ServiceException(e));
		}
	}

	public void deleteNewsAuthor(long newsId) throws ServiceException{
		try {
			newsDAO.deleteNewsAuthor(newsId);
		} catch (DAOException e){
			logger.error(e);
			throw(new ServiceException(e));
		}
	}

	@Transactional(readOnly=true)
	public List<Long> getNewsIdListByAuthorId( long authorId) throws ServiceException{
		try {
			return newsDAO.getNewsIdListByAuthorId(authorId);
		} catch (DAOException e){
			logger.error(e);
			throw(new ServiceException(e));
		}
	}
	
	//News_Tag
	
	public void addNewsTag(long newsId, List<Long> tagIds) throws ServiceException{
		try {
			newsDAO.addNewsTag(newsId, tagIds);
		} catch (DAOException e){
			logger.error(e);
			throw(new ServiceException(e));
		}
	}


	@Transactional(readOnly=true)
	public boolean getNewsTagExistance(long newsId,long tagId) throws ServiceException{
		try {
			return newsDAO.getNewsTagExistance(newsId, tagId);
		} catch (DAOException e){
			logger.error(e);
			throw(new ServiceException(e));
		}
	}

	@Transactional(readOnly=true)
	public List<Long> getNewsIdListByTagId(long tagId) throws ServiceException{
		try {
			return newsDAO.getNewsIdListByTagId(tagId);
		} catch (DAOException e){
			logger.error(e);
			throw(new ServiceException(e));
		}
	}

	public void deleteNewsTag(long newsId, long tagId) throws ServiceException{
		try {
			newsDAO.deleteNewsTag(newsId, tagId);
		} catch (DAOException e){
			logger.error(e);
			throw(new ServiceException(e));
		}
	}
	
	public void deleteNewsTagByNewsId(long newsId) throws ServiceException{
		try {
			newsDAO.deleteNewsTagByNewsId(newsId);
		} catch (DAOException e){
			logger.error(e);
			throw(new ServiceException(e));
		}
	}
	

	public void deleteNewsTagByTagId(long tagId) throws ServiceException{
		try {
			newsDAO.deleteNewsTagByTagId(tagId);
		} catch (DAOException e){
			logger.error(e);
			throw(new ServiceException(e));
		}
	}

	@Transactional(readOnly=true)
	public List<Tag> getTagListByNewsId(long newsId) throws ServiceException{
		try {
			return newsDAO.getTagListByNewsId(newsId);
		} catch (DAOException e){
			logger.error(e);
			throw(new ServiceException(e));
		}
	}
	
}
