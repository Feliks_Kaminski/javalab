package by.epam.newsmanagement.service;

import java.util.List;

import by.epam.newsmanagement.entity.Tag;
import by.epam.newsmanagement.exceptions.ServiceException;
/**
 * This is an interface for operations on Tag entity.
 * 
 * @author Feliks_Kaminski
 *
 */
public interface IServiceTag {
	
	/**
	 * The method inserts a Tag entity and returns it's id.
	 * 
	 * @param name
	 * @return id of the Tag entity
	 * @throws ServiceException
	 */
	public long addTag(String name) throws ServiceException;
	
	/**
	 * The method gets a Tag entity and updates it's fields using it's id.
	 * 
	 * @param Tag entity
	 * @throws ServiceException
	 */
	public void editTag(Tag tag) throws ServiceException;

	
	/**
	 * The method returns a Tag entity using it's unique name.
	 * 
	 * @param name
	 * @return Tag entity
	 * @throws ServiceException
	 */
	public Tag getTag(String name) throws ServiceException;
	
	/**
	 * The method returns a Tag entity using it's id.
	 * 
	 * @param tagId
	 * @return Tag entity
	 * @throws ServiceException
	 */
	public Tag getTag(long tag_id) throws ServiceException;

	/**
	 * The method removes a Tag from the ds using it's id.
	 * 
	 * @param tagId
	 * @throws ServiceException
	 */
	public void deleteTag(long tag_id) throws ServiceException;

	/**
	 * The method returns a list of Tag entities.
	 * 
	 * @return list with Tag entities
	 * @throws ServiceException
	 */
	public List<Tag> getTagList() throws ServiceException;
}
