package by.epam.newsmanagement.entity;

import java.util.Set;

public class SearchCriteria {
	private Long authorId;
	private Set<Long> tagIdSet;
	
	public SearchCriteria(Long authorId,Set<Long> tagIdSet){
		this.authorId=authorId;
		this.tagIdSet=tagIdSet;
	}
	
	
	
	public Long getAuthorId(){
		return authorId;
	}
	
	public void setAuthorId(Long authorId){
		this.authorId=authorId;
	}
	

	
	public void addTagId(Long tagId){
		tagIdSet.add(tagId);
	}

	public void setTagIdSet(Set<Long> tagIdSet){
		this.tagIdSet=tagIdSet;
	}
	
	public Set<Long> getTagIdSet(){
		return tagIdSet;
	}
	
	
	@Override
	public boolean equals(Object obj) {
		if  ((null!=obj)&&(obj instanceof SearchCriteria)){
			SearchCriteria sc = (SearchCriteria)obj;
			if (this.authorId.equals(sc.authorId)&&(this.tagIdSet.equals(sc.tagIdSet))){
				return true;
			}
		}
		return false;
	}
}
