package by.epam.newsmanagement.service.impl;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import by.epam.newsmanagement.dao.IDAONews;
import by.epam.newsmanagement.entity.News;
import by.epam.newsmanagement.entity.SearchCriteria;
import by.epam.newsmanagement.entity.Tag;
import by.epam.newsmanagement.exceptions.DAOException;
import by.epam.newsmanagement.exceptions.ServiceException;
import by.epam.newsmanagement.service.impl.ServiceNews;

@RunWith(MockitoJUnitRunner.class)
public class ServiceNewsTest {

	@Mock
	private IDAONews newsDAO;
	
	@Mock
	private News news;
	
	@InjectMocks private ServiceNews newsService;

	@Test
	public void addNewsTest() throws DAOException,ServiceException {
		long nId= 123L;
		String title = "testT";
		String shortText = "testST";
		String fullText = "testFT";
		Date date = new Date(12345L);
		
		when(newsDAO.addNews(title, shortText, fullText, date)).thenReturn(nId);
		
		assertEquals(nId,newsDAO.addNews(title, shortText, fullText, date));
		
		verify(newsDAO).addNews(title, shortText, fullText, date);
		verifyNoMoreInteractions(newsDAO);
	}

	@Test
	public void editNewsTest() throws DAOException,ServiceException {
		doNothing().when(newsDAO).editNews(news);
		
		newsService.editNews(news);
		
		verify(newsDAO).editNews(news);
		verifyNoMoreInteractions(newsDAO);
		verifyNoMoreInteractions(news);
	}

	@Test
	public void getNewsTest() throws DAOException,ServiceException {
		long nId= 123L;
		
		when(newsDAO.getNews(nId)).thenReturn(news);
		
		assertEquals(news,newsService.getNews(nId));
		
		verify(newsDAO).getNews(nId);
		verifyNoMoreInteractions(newsDAO);
		verifyNoMoreInteractions(news);
	}

	@Test
	public void deleteNewsTest() throws DAOException,ServiceException {
		long nId= 123L;
		
		doNothing().when(newsDAO).deleteNews(nId);
		
		newsService.deleteNews(nId);
		
		verify(newsDAO).deleteNews(nId);
		verifyNoMoreInteractions(newsDAO);
	}


	@Test
	public void getNewsListByCriteriaTest() throws DAOException,ServiceException {
		SearchCriteria sc = mock(SearchCriteria.class);
		List <News> nList = mock(List.class);

		when(newsDAO.getNewsListByCriteria(sc,1,0)).thenReturn(nList);
		
		assertTrue(newsDAO.getNewsListByCriteria(sc,1,0)==nList);
		
		verify(newsDAO).getNewsListByCriteria(sc,1,0);
		verifyNoMoreInteractions(newsDAO);
		verifyNoMoreInteractions(nList);
		verifyNoMoreInteractions(sc);
	}
	
	@Test
	public void getNewsByCriteriaCountTest() throws DAOException,ServiceException {
		SearchCriteria sc = mock(SearchCriteria.class);

		when(newsDAO.getNewsByCriteriaCount(sc)).thenReturn(23L);
		
		assertEquals(newsDAO.getNewsByCriteriaCount(sc),23L);
		
		verify(newsDAO).getNewsByCriteriaCount(sc);
		verifyNoMoreInteractions(newsDAO);
		verifyNoMoreInteractions(sc);
	}
	
	//NewsAuthor

	@Test
	public void addNewsAuthorTest() throws DAOException,ServiceException {
		long nId= 123L;
		long aId= 543L;
		
		doNothing().when(newsDAO).addNewsAuthor(nId,aId);
		
		newsService.addNewsAuthor(nId,aId);
		
		verify(newsDAO).addNewsAuthor(nId,aId);
		verifyNoMoreInteractions(newsDAO);
	}

	@Test
	public void getAuthorIdByNewsIdTest() throws DAOException,ServiceException {
		long nId= 123L;
		long aId= 543L;
		
		when(newsDAO.getAuthorIdByNewsId(nId)).thenReturn(aId);
		
		assertEquals(newsService.getAuthorIdByNewsId(nId),aId);
		
		verify(newsDAO).getAuthorIdByNewsId(nId);
		verifyNoMoreInteractions(newsDAO);
	}

	@Test
	public void deleteNewsAuthorTest() throws DAOException,ServiceException {
		long nId= 123L;
		
		doNothing().when(newsDAO).deleteNewsAuthor(nId);
		
		newsService.deleteNewsAuthor(nId);
		
		verify(newsDAO).deleteNewsAuthor(nId);
		verifyNoMoreInteractions(newsDAO);
	}

	@Test
	public void getNewsIdListByAuthorIdTest() throws DAOException,ServiceException {
		List <Long> nList= mock(List.class);
		long aId = 123L;

		when(newsDAO.getNewsIdListByAuthorId(aId)).thenReturn(nList);
		
		assertTrue(newsService.getNewsIdListByAuthorId(aId)==nList);
		
		verify(newsDAO).getNewsIdListByAuthorId(aId);
		verifyNoMoreInteractions(newsDAO);
		verifyNoMoreInteractions(nList);
	}
	
	//News_Tag

	@Test
	public void addNews_TagTest() throws DAOException,ServiceException {
		long nId= 123L;
		
		List <Long> tagIds = new ArrayList<Long>();
		tagIds.add(543L);
		
		doNothing().when(newsDAO).addNewsTag(nId,tagIds);
		
		newsService.addNewsTag(nId,tagIds);
		
		verify(newsDAO).addNewsTag(nId,tagIds);
		verifyNoMoreInteractions(newsDAO);
	}

	@Test
	public void getNewsTagExistanceTest() throws DAOException,ServiceException {
		long tId= 23L;
		long nId= 13L;
		
		when(newsDAO.getNewsTagExistance(nId,tId)).thenReturn(true);
		
		assertTrue(newsService.getNewsTagExistance(nId, tId));
		
		verify(newsDAO).getNewsTagExistance(nId, tId);
		verifyNoMoreInteractions(newsDAO);
	}
	
	
	@Test
	public void getNewsIdListByTagIdTest() throws DAOException,ServiceException {
		long tId= 543L;
		List <Long> nIds = mock(List.class);
		
		
		when(newsDAO.getNewsIdListByTagId(tId)).thenReturn(nIds);
		
		assertTrue(newsService.getNewsIdListByTagId(tId)==nIds);
		
		verify(newsDAO).getNewsIdListByTagId(tId);
		verifyNoMoreInteractions(newsDAO);
	}

	@Test
	public void deleteNewsTagTest() throws DAOException,ServiceException {
		long nId= 123L;
		long tId= 543L;
		
		doNothing().when(newsDAO).deleteNewsTag(nId,tId);
		
		newsService.deleteNewsTag(nId,tId);
		
		verify(newsDAO).deleteNewsTag(nId,tId);
		verifyNoMoreInteractions(newsDAO);
	}
	
	@Test
	public void deleteNewsTagByNewsIdTest() throws DAOException,ServiceException {
		long nId= 123L;
		
		doNothing().when(newsDAO).deleteNewsTagByNewsId(nId);
		
		newsService.deleteNewsTagByNewsId(nId);
		
		verify(newsDAO).deleteNewsTagByNewsId(nId);
		verifyNoMoreInteractions(newsDAO);
	}
	
	@Test
	public void deleteNewsTagByTagIdTest() throws DAOException,ServiceException {
		long tId= 543L;
		
		doNothing().when(newsDAO).deleteNewsTagByTagId(tId);
		
		newsService.deleteNewsTagByTagId(tId);
		
		verify(newsDAO).deleteNewsTagByTagId(tId);
		verifyNoMoreInteractions(newsDAO);
	}

	@Test
	public void getTagListByNewsIdTest() throws DAOException,ServiceException {
		List <Tag> tList= mock(List.class);
		long newsId = 123L;

		when(newsDAO.getTagListByNewsId(newsId)).thenReturn(tList);
		
		assertTrue(newsService.getTagListByNewsId(newsId)==tList);
		
		verify(newsDAO).getTagListByNewsId(newsId);
		verifyNoMoreInteractions(newsDAO);
		verifyNoMoreInteractions(tList);
	}

	
}
