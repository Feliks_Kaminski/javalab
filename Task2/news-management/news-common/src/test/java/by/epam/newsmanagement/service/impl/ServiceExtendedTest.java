package by.epam.newsmanagement.service.impl;

import static org.mockito.Mockito.*;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import static org.junit.Assert.*;

import by.epam.newsmanagement.dao.IDAOAuthor;
import by.epam.newsmanagement.dao.IDAOComment;
import by.epam.newsmanagement.dao.IDAONews;
import by.epam.newsmanagement.dao.IDAOTag;
import by.epam.newsmanagement.dao.IDAOUser;
import by.epam.newsmanagement.entity.News;
import by.epam.newsmanagement.exceptions.DAOException;
import by.epam.newsmanagement.exceptions.ServiceException;
import by.epam.newsmanagement.service.impl.ServiceExtended;

@RunWith(MockitoJUnitRunner.class)
public class ServiceExtendedTest {

	@Mock
	private IDAOAuthor authorDAO;
	@Mock
	private IDAOComment commentDAO;
	@Mock
	private IDAONews newsDAO;
	@Mock
	private IDAOTag tagDAO;
	@Mock
	private IDAOUser userDAO;
	
	
	@InjectMocks private ServiceExtended extendedService;
	
	@Test
	public void addNewsWithDependenciesTest() throws DAOException,ServiceException{
		
		long idN=123L;
		long idA=234L;
		String title = "T";
		String shortText = "LT";
		String fullText = "FT";
		Date date = new Date(12345L);
		
		List <Long> tagsIds = new ArrayList<Long>();
		
		tagsIds.add(456L);
		tagsIds.add(567L);
		tagsIds.add(987L);
		

		when(newsDAO.addNews(title, shortText, fullText, date)).thenReturn(idN);
		
		extendedService.addNewsWithDependencies(idA, title, shortText, fullText, date, tagsIds);
		
		
		
		verify(newsDAO).addNews(title, shortText, fullText, date);
		verify(newsDAO).addNewsAuthor(idN, idA);
		verify(newsDAO).addNewsTag(idN, tagsIds);

	}
	
	@Test
	public void editNewsWithDependenciesTest() throws DAOException,ServiceException{
		long newsId=123L;
		long authorId=234L;
		
		String title = "T";
		String shortText = "LT";
		String fullText = "FT";
		Date date = new Date(12L);
		News news = new News(newsId, title, shortText, fullText, new Timestamp(34L), date);
		
		String titleN = "TN";
		String shortTextN = "LTN";
		String fullTextN = "FTN";
		Date dateN = new Date(145L);
		
		
		List <Long> tags = new ArrayList<Long>();		
		tags.add(456L);
		tags.add(567L);
		tags.add(987L);
		
		when(newsDAO.getNews(newsId)).thenReturn(news);
		
		
		
		extendedService.editNewsWithDependencies(authorId, newsId, titleN, shortTextN, fullTextN, dateN, tags);
		
		assertEquals(news.getTitle(), titleN);
		assertEquals(news.getShortText(), shortTextN);
		assertEquals(news.getFullText(), fullTextN);
		assertEquals(news.getModificationDate(), dateN);
		
		verify(newsDAO).getNews(newsId);
		verify(newsDAO).editNews(news);
		verify(newsDAO).deleteNewsAuthor(newsId);
		verify(newsDAO).addNewsAuthor(newsId, authorId);
		verify(newsDAO).deleteNewsTagByNewsId(newsId);
		verify(newsDAO).addNewsTag(newsId, tags);
		verifyNoMoreInteractions(newsDAO);
	}
	
	
	
	@Test
	public void deleteNewsWithDependenciesTest() throws DAOException,ServiceException{
		long newsId = 123L;		
		
		doNothing().when(newsDAO).deleteNewsAuthor(newsId);
		doNothing().when(newsDAO).deleteNewsTagByNewsId(anyLong());

		doNothing().when(commentDAO).deleteComment(newsId);

		extendedService.deleteNewsWithDependencies(newsId);
		
		verify(newsDAO).deleteNewsAuthor(newsId);
		verify(newsDAO).deleteNewsTagByNewsId(newsId);
		verify(newsDAO).deleteNews(newsId);
		verifyNoMoreInteractions(newsDAO);
	}
	


	@Test
	public void deleteTagWithDependenciesTest() throws DAOException,ServiceException{
		
		long tagId = 123L;
		
		doNothing().when(newsDAO).deleteNewsTagByTagId(anyLong());
		doNothing().when(tagDAO).deleteTag(tagId);
		

		extendedService.deleteTagWithDependencies(tagId);
		
		verify(newsDAO).deleteNewsTagByTagId(tagId);
		verify(tagDAO).deleteTag(tagId);
		verifyNoMoreInteractions(tagDAO);
		verifyNoMoreInteractions(newsDAO);
	}
	
	
	@Test
	public void deleteUserWithDependenciesTest() throws DAOException,ServiceException{
		long userId=123L;
		extendedService.deleteUserWithDependencies(userId);
		verify(userDAO).deleteUserRole(userId);
		verify(userDAO).deleteUser(userId);
		verifyNoMoreInteractions(userDAO);
	}
}
