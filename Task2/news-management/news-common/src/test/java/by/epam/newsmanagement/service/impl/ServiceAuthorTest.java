package by.epam.newsmanagement.service.impl;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import by.epam.newsmanagement.dao.IDAOAuthor;
import by.epam.newsmanagement.entity.Author;
import by.epam.newsmanagement.exceptions.DAOException;
import by.epam.newsmanagement.exceptions.ServiceException;
import by.epam.newsmanagement.service.impl.ServiceAuthor;


@RunWith(MockitoJUnitRunner.class)

public class ServiceAuthorTest {
	
	@Mock
	private IDAOAuthor authorDAO;
	
	@Mock
	private Author author;
	
	@InjectMocks private ServiceAuthor authorService;
	
	@Test
	public void addAuthorTest() throws DAOException,ServiceException {
		long aId= 123L;
		String name = "NewName";
		
		when(authorDAO.addAuthor(name)).thenReturn(aId);
		
		assertEquals(aId,authorService.addAuthor(name));
		
		verify(authorDAO).addAuthor(name);
		verifyNoMoreInteractions(authorDAO);
	}
	
	@Test (expected=ServiceException.class)
	public void addAuthorTestEx() throws DAOException,ServiceException {
		String name = "NewName";
		
		try {
			when(authorDAO.addAuthor(name)).thenThrow(new DAOException("Test"));
			authorService.addAuthor(name);
		} finally {
			verify(authorDAO).addAuthor(name);
			verifyNoMoreInteractions(authorDAO);
		}
	}
	
	@Test
	public void editAuthorTest() throws DAOException,ServiceException {
		
		doNothing().when(authorDAO).editAuthor(author);
		
		authorService.editAuthor(author);
		
		verify(authorDAO).editAuthor(author);
		verifyNoMoreInteractions(authorDAO);
		verifyNoMoreInteractions(author);
	}

	@Test
	public void getAuthorTest() throws DAOException,ServiceException {
		long aId= 123L;
		String name = "Pushkin";

		when(authorDAO.getAuthor(aId)).thenReturn(author);
		when(authorDAO.getAuthor(name)).thenReturn(author);
		
		assertEquals(author,authorService.getAuthor(aId));
		assertEquals(author,authorService.getAuthor(name));
		
		verify(authorDAO).getAuthor(aId);
		verify(authorDAO).getAuthor(name);
		verifyNoMoreInteractions(authorDAO);
		verifyNoMoreInteractions(author);
	}

	@Test
	public void deleteAuthorTest() throws DAOException,ServiceException {
		long aId= 123L;

		doNothing().when(authorDAO).deleteAuthor(aId);
		
		authorService.deleteAuthor(aId);
		
		verify(authorDAO).deleteAuthor(aId);
		verifyNoMoreInteractions(authorDAO);
	}

	@Test
	public void getAuthorListTest() throws DAOException,ServiceException {
		List <Author> aList= mock(List.class);
		

		when(authorDAO.getAuthorList()).thenReturn(aList);
		
		assertTrue(authorService.getAuthorList()==aList);
		
		verify(authorDAO).getAuthorList();
		verifyNoMoreInteractions(authorDAO);
		verifyNoMoreInteractions(aList);
	}

	
}
