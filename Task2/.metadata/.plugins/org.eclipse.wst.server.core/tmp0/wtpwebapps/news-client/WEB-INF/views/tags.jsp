<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<link type="text/css" rel="stylesheet" href="resources/css/style.css" />
<link type="text/css" rel="stylesheet" href="resources/css/multiple-select.css" />

<script src="resources/js/jquery.min.js"></script>
<script src="resources/js/jquery.multiple.select.js"></script>

</head>
<body>
<h1 id="gg">${path}</h1>

	<select multiple="multiple" id="ss">
        <option value="1">January</option>
        <option value="2">Feb</option>
        <option value="3">Mar</option>
        <option value="12">December</option>
    </select>
    <script src="multiple-select.js"></script>
    <script>
        $('select').multipleSelect();
    </script>
</body>
</html>
