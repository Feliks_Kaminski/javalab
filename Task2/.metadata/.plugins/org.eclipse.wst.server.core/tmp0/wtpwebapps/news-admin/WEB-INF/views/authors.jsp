<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<html>
<head>

<!--<link type="text/css" rel="stylesheet" href="resources/css/style.css" />-->
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style>
   form {
    margin: 0;
   }
</style>
</head>
<body>
<h1><spring:message code="authors.header" /></h1>
<table>
<c:forEach var="author" items="${authors}" varStatus="stat">
	<tr>
		<td>
			<form method="POST" action="\news-admin\authors_update" >
				<input type="hidden" name="authorId" value="${author.id}" />
				<input id="t${stat.getCount()}" type="text" name="authorName" value="${author.name}" disabled="disabled"/>
				<a id="e${stat.getCount()}" href="javascript:;" onclick="show(${stat.getCount()})">Edit</a>
				<a id="u${stat.getCount()}" href="javascript:;" onclick="doSubmit(parentNode)" style="visibility:hidden">Update</a>
				<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
			</form>
		</td>
		<td>
			<form method="POST" action="\news-admin\authors_expire">
				<input type="hidden" name="authorId" value="${author.id}"/>
				<a id="d${stat.getCount()}" href="javascript:;" onclick="parentNode.submit()" style="visibility:hidden">Expire</a>
				<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
			</form>
		</td>
		<td>
			<a id="c${stat.getCount()}" href="javascript:;" onclick="hide(${stat.getCount()})" style="visibility:hidden">Cancel</a>
		</td>
	</tr>
	<tr><td></td></tr>
</c:forEach>
    
<tr>
	<td>
		<form method="POST" action="<spring:url value="\news-admin\authors_add"/>" >
	     	<input type="text" name="authorName">
	     	<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
			<a href="javascript:;" onclick="parentNode.submit()">Save</a>
		</form>
	</td>
</tr>
</table>
</body>
</html>
