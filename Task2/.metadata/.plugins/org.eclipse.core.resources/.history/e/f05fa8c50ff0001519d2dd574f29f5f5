package by.epam.newsmanagement.dao.impl;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;

import by.epam.newsmanagement.dao.IDAOComment;
import by.epam.newsmanagement.dao.IDAONews;
import by.epam.newsmanagement.entity.Author;
import by.epam.newsmanagement.entity.News;
import by.epam.newsmanagement.entity.SearchCriteria;
import by.epam.newsmanagement.entity.Tag;
import by.epam.newsmanagement.exceptions.DAOException;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations= {"classpath:test_spring.xml"})
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class })


public class DAONewsTest {

	@Autowired
	IDAONews newsDAO;
	@Autowired
	IDAOComment commentDAO;
	
    @DatabaseSetup({"classpath:NewsTestDS.xml"})
    @DatabaseTearDown(value = {"classpath:NewsTestDS.xml"},type = DatabaseOperation.DELETE_ALL)  
    @Test
	public void addNewsTest() throws DAOException {
			String title = "T1";
			String shortText = "ST1";
			String fullText = "FT1";
			Date date = new Date(12345L);
			long nId = newsDAO.addNews("T1", "ST1", "FT1",date);
			News news = newsDAO.getNews(nId);
			
			assertEquals(title, news.getTitle());
			assertEquals(shortText, news.getShortText());
			assertEquals(fullText, news.getFullText());
			//assert(date.toString().equals(news.getModificationDate().toString()));  
			//assertEquals(date.toString(), news.getCreationDate().toString());
	}
    
    @DatabaseSetup({"classpath:NewsTestDS.xml"})
    @DatabaseTearDown(value = {"classpath:NewsTestDS.xml"},type = DatabaseOperation.DELETE_ALL)  
    @Test
	public void editNewsTest() throws DAOException {
		long nId=11;
		assertNotNull(newsDAO.getNews(nId));
		News news = newsDAO.getNews(nId);
		
		String title = "T1";
		String shortText = "ST1";
		String fullText = "FT1";
		Date date = new Date(12345678L);
		
		assertNotEquals(news.getTitle(), title);
		assertNotEquals(news.getShortText(), shortText);
		assertNotEquals(news.getFullText(), fullText);
		assertNotEquals(news.getModificationDate(), date);
		
		news.setTitle(title);
		news.setShortText(shortText);
		news.setFullText(fullText);
		news.setModificationDate(date);
		
		newsDAO.editNews(news);
		
		News newsE = newsDAO.getNews(nId);
		//assert(news.equals(newsE));
	}
    
    
	
    @DatabaseSetup({"classpath:NewsTestDS.xml"})
    @DatabaseTearDown(value = {"classpath:NewsTestDS.xml"},type = DatabaseOperation.DELETE_ALL)  
    @Test
	public void getNewsTest() throws DAOException {
			assertNotNull(newsDAO.getNews(11));
			newsDAO.deleteNews(11);
			assertNull(newsDAO.getNews(11));
	}
    
    
    @DatabaseSetup({"classpath:NewsTestDS.xml","classpath:AuthorTestDS.xml",
    	"classpath:TagTestDS.xml","classpath:CommentTestDS.xml"})
    @DatabaseTearDown(value = {"classpath:NewsTestDS.xml","classpath:AuthorTestDS.xml",
    		"classpath:News_AuthorTestDS.xml","classpath:TagTestDS.xml",
    		"classpath:News_TagTestDS.xml","classpath:CommentTestDS.xml"}
    	,type = DatabaseOperation.DELETE_ALL) 
    @Test
	public void deleteNewsTest() throws DAOException {
				assertNotNull(newsDAO.getNews(11));
				for(int i=1;i<5;i++){
					switch (i) {
					case 2:
						commentDAO.deleteComment(13);
						List<Long> tagList = new ArrayList<Long>();
						tagList.add(5L);
						newsDAO.addNewsTag(11, tagList);
						break;
					case 3:
						newsDAO.deleteNewsTag(11, 5);
						newsDAO.addNewsAuthor(11, 16);
						break;
					case 4:
						newsDAO.deleteNewsAuthor(11);
						break;
					default:
						break;
					}
					
					try {
							newsDAO.deleteNews(11);
							if (4>i){
								fail("News has been deleted before dependencies!");
							}
					} catch (DAOException e){
						
					} catch (Exception e){
						fail(e.toString());
					}
				}
				
				assertNull(newsDAO.getNews(11));
	}

    
	
    @DatabaseSetup({"classpath:NewsTestDS.xml","classpath:AuthorTestDS.xml","classpath:News_AuthorTestDS.xml","classpath:TagTestDS.xml","classpath:News_TagTestDS.xml"})
    @DatabaseTearDown(value = {"classpath:NewsTestDS.xml","classpath:AuthorTestDS.xml","classpath:News_AuthorTestDS.xml","classpath:TagTestDS.xml","classpath:News_TagTestDS.xml"}
    	,type = DatabaseOperation.DELETE_ALL)  
    @Test
	public void getNewsListByCriteriaTest() throws DAOException {
    	List<Tag> tagList = new ArrayList<Tag>();
    	tagList.add(new Tag(10,"TestTag10"));
    	tagList.add(new Tag(9,"TestTag9"));
    	SearchCriteria sc=null;
    	assertEquals(8, newsDAO.getNewsListByCriteria(sc,1,0).size());
    	Author a = new Author(11, "TestAuthor11");
			sc = new SearchCriteria(null,tagList);
			assertEquals(6, newsDAO.getNewsListByCriteria(sc,1,0).size());
			sc.setAuhor(a);
			assertEquals(2, newsDAO.getNewsListByCriteria(sc,1,0).size());
	}
    
    @DatabaseSetup({"classpath:NewsTestDS.xml","classpath:AuthorTestDS.xml","classpath:News_AuthorTestDS.xml","classpath:TagTestDS.xml","classpath:News_TagTestDS.xml"})
    @DatabaseTearDown(value = {"classpath:NewsTestDS.xml","classpath:AuthorTestDS.xml","classpath:News_AuthorTestDS.xml","classpath:TagTestDS.xml","classpath:News_TagTestDS.xml"}
    	,type = DatabaseOperation.DELETE_ALL)  
    @Test
	public void getNewsByCriteriaCountTest() throws DAOException {
    	List<Tag> tagList = new ArrayList<Tag>();
    	tagList.add(new Tag(10,"TestTag10"));
    	tagList.add(new Tag(9,"TestTag9"));
    	SearchCriteria sc;
    	Author a = new Author(11, "TestAuthor11");
			sc = new SearchCriteria(null,tagList);
			assertEquals(6, newsDAO.getNewsByCriteriaCount(sc));
			sc.setAuhor(a);
			assertEquals(2, newsDAO.getNewsByCriteriaCount(sc));
	}
	
	//NewsAuthor
    @DatabaseSetup({"classpath:NewsTestDS.xml","classpath:AuthorTestDS.xml"})
    @DatabaseTearDown(value = {"classpath:NewsTestDS.xml","classpath:AuthorTestDS.xml","classpath:News_AuthorTestDS.xml"}
    	,type = DatabaseOperation.DELETE_ALL)
    @Test
	public void addNewsAuthorTest() throws DAOException {
			assertEquals(newsDAO.getAuthorIdByNewsId(10),0);
			newsDAO.addNewsAuthor(10,16);
			assertEquals(newsDAO.getAuthorIdByNewsId(10),16);
	}

    @DatabaseSetup({"classpath:NewsTestDS.xml","classpath:AuthorTestDS.xml","classpath:News_AuthorTestDS.xml"})
    @DatabaseTearDown(value = {"classpath:NewsTestDS.xml","classpath:AuthorTestDS.xml","classpath:News_AuthorTestDS.xml"}
    	,type = DatabaseOperation.DELETE_ALL)  
    @Test
	public void getAuthorIdByNewsIdTest() throws DAOException {
    		assertEquals(newsDAO.getAuthorIdByNewsId(10),16);
			newsDAO.deleteNewsAuthor(10);
			assertEquals(newsDAO.getAuthorIdByNewsId(10),0);
	}

    @DatabaseSetup({"classpath:NewsTestDS.xml","classpath:AuthorTestDS.xml","classpath:News_AuthorTestDS.xml"})
    @DatabaseTearDown(value = {"classpath:NewsTestDS.xml","classpath:AuthorTestDS.xml","classpath:News_AuthorTestDS.xml"}
    	,type = DatabaseOperation.DELETE_ALL)
    @Test
	public void deleteNewsAuthorTest() throws DAOException {
		assertEquals(newsDAO.getAuthorIdByNewsId(10),16);
		newsDAO.deleteNewsAuthor(10);
		assertEquals(newsDAO.getAuthorIdByNewsId(10),0);
	}
    
    @DatabaseSetup({"classpath:NewsTestDS.xml","classpath:AuthorTestDS.xml","classpath:News_AuthorTestDS.xml"})
    @DatabaseTearDown(value = {"classpath:NewsTestDS.xml","classpath:AuthorTestDS.xml","classpath:News_AuthorTestDS.xml"}
    	,type = DatabaseOperation.DELETE_ALL)
    @Test
	public void getNewsIdListByAuthorIdTest() throws DAOException {
			long authorId = 16;
			assertEquals(4,newsDAO.getNewsIdListByAuthorId(authorId).size());
			newsDAO.deleteNewsAuthor(10);
			newsDAO.deleteNewsAuthor(11);
			assertEquals(2,newsDAO.getNewsIdListByAuthorId(authorId).size());
	}
	
	//News_Tag
	@DatabaseSetup({"classpath:NewsTestDS.xml","classpath:TagTestDS.xml"})
    @DatabaseTearDown(value = {"classpath:NewsTestDS.xml","classpath:TagTestDS.xml","classpath:News_TagTestDS.xml"}
    	,type = DatabaseOperation.DELETE_ALL)
    @Test
	public void addNews_TagTest() throws DAOException {
		assertEquals(newsDAO.getTagListByNewsId(10).size(),0);

		List<Long> tagList = new ArrayList<Long>();
		tagList.add(6L);
		newsDAO.addNewsTag(10,tagList);
		assertEquals(newsDAO.getTagListByNewsId(10).size(),1);
	}
	
    @DatabaseSetup({"classpath:NewsTestDS.xml","classpath:TagTestDS.xml","classpath:News_TagTestDS.xml"})
    @DatabaseTearDown(value = {"classpath:NewsTestDS.xml","classpath:TagTestDS.xml","classpath:News_TagTestDS.xml"}
    	,type = DatabaseOperation.DELETE_ALL)
    @Test
	public void getNewsTagExistanceTest() throws DAOException {
			assertTrue(newsDAO.getNewsTagExistance(10, 6));
			newsDAO.deleteNewsTag(10,6);
			assertFalse(newsDAO.getNewsTagExistance(10, 6));
	}
	
    @DatabaseSetup({"classpath:NewsTestDS.xml","classpath:TagTestDS.xml","classpath:News_TagTestDS.xml"})
    @DatabaseTearDown(value = {"classpath:NewsTestDS.xml","classpath:TagTestDS.xml","classpath:News_TagTestDS.xml"}
    	,type = DatabaseOperation.DELETE_ALL)
    @Test
	public void getNewsIdListByTagIdTest() throws DAOException {
			assertEquals(newsDAO.getNewsIdListByTagId(6).size(),2);
			newsDAO.deleteNewsTag(10,6);
			assertEquals(newsDAO.getNewsIdListByTagId(6).size(),1);
	}
	
    @DatabaseSetup({"classpath:NewsTestDS.xml","classpath:TagTestDS.xml","classpath:News_TagTestDS.xml"})
    @DatabaseTearDown(value = {"classpath:NewsTestDS.xml","classpath:TagTestDS.xml","classpath:News_TagTestDS.xml"}
    	,type = DatabaseOperation.DELETE_ALL)
    @Test
	public void deleteNewsTagByNewsIdTest() throws DAOException {
		assertEquals(newsDAO.getTagListByNewsId(10).size(),3);
			newsDAO.deleteNewsTagByNewsId(10);
			assertEquals(newsDAO.getTagListByNewsId(10).size(),0);
	}

    @DatabaseSetup({"classpath:NewsTestDS.xml","classpath:TagTestDS.xml","classpath:News_TagTestDS.xml"})
    @DatabaseTearDown(value = {"classpath:NewsTestDS.xml","classpath:TagTestDS.xml","classpath:News_TagTestDS.xml"}
    	,type = DatabaseOperation.DELETE_ALL)
    @Test
	public void deleteNewsTagByTagIdTest() throws DAOException {
		assertEquals(newsDAO.getTagListByNewsId(10).size(),3);
			newsDAO.deleteNewsTagByTagId(6);
			assertEquals(newsDAO.getTagListByNewsId(10).size(),2);
	}
    

    @DatabaseSetup({"classpath:NewsTestDS.xml","classpath:TagTestDS.xml","classpath:News_TagTestDS.xml"})
    @DatabaseTearDown(value = {"classpath:NewsTestDS.xml","classpath:TagTestDS.xml","classpath:News_TagTestDS.xml"}
    	,type = DatabaseOperation.DELETE_ALL)
    @Test
	public void getTagListByNewsIdTest() throws DAOException {
			List<Tag> l = newsDAO.getTagListByNewsId(10);
			assertNotNull(l);
			assertEquals(l.size(),3);
			newsDAO.deleteNewsTag(10,5);
			newsDAO.deleteNewsTag(10,6);
			newsDAO.deleteNewsTag(10,7);
			l = newsDAO.getTagListByNewsId(10);
			assertEquals(l.size(),0);
	}
}
