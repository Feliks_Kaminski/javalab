package by.epam.newsmanagement.controller;

import java.sql.Date;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import by.epam.newsmanagement.entity.Author;
import by.epam.newsmanagement.entity.News;
import by.epam.newsmanagement.entity.NewsInfoHolder;
import by.epam.newsmanagement.entity.SearchCriteria;
import by.epam.newsmanagement.entity.Tag;
import by.epam.newsmanagement.exceptions.ServiceException;
import by.epam.newsmanagement.service.IServiceAuthor;
import by.epam.newsmanagement.service.IServiceExtended;
import by.epam.newsmanagement.service.IServiceNews;
import by.epam.newsmanagement.service.IServiceTag;
import by.epam.newsmanagement.utils.Converter;



@Controller
public class NewsController {
	
	private IServiceAuthor authorService;
	private IServiceNews newsService;
	private IServiceTag tagService;
	private IServiceExtended extendedService;
	
	@Autowired
	public NewsController(IServiceAuthor authorService, IServiceNews newsService, IServiceTag tagService, 
			IServiceExtended extendedService){
		this.authorService = authorService;
		this.newsService = newsService;
		this.tagService = tagService;
		this.extendedService = extendedService;
	}
	
	@RequestMapping(value = "/newsList", method = RequestMethod.GET)
	public String viewList(Model model) throws ServiceException {
		List <Author> authors = authorService.getAuthorList();
		model.addAttribute("authors", authors);
		
		List <Tag> tags = tagService.getTagList();
		model.addAttribute("tags", tags);
		
		
		//SearchCriteria sc = new SearchCriteria(author, tagList);
		
		
		List <Long> newsIds = newsService.getNewsIdsByCriteria(null, 1, 4);
		List <NewsInfoHolder> newsInfo = extendedService.getNewsInfoHolderList(newsIds);
		model.addAttribute("newsInfo", newsInfo);
		
		return "newsList";
	}
	
	
	@RequestMapping(value = "/newsListFind", method = RequestMethod.POST)
	public String viewListFind(Model model, HttpSession session,
							@RequestParam(value = "authorId", required = false) Long authorId,
							@RequestParam(value = "tags", required = false) TreeSet<Long> tagIdSet) throws ServiceException {
		
		SearchCriteria sc = new SearchCriteria(authorId, tagIdSet);
		session.removeAttribute("newsParamHolder");
		session.setAttribute("searchCriteria", sc);
		return "redirect:/newsList";
	}
	
	@RequestMapping(value = "/newsListDelete", method = RequestMethod.GET)
	public String viewListDelete(@RequestParam(value = "idsForDel", required = true) List<Long> idsForDel) throws ServiceException {
		for(Long newsId : idsForDel){
			extendedService.deleteNewsWithDependencies(newsId);
		}
		return "redirect:/newsList";
	}
	
	
	@RequestMapping(value = "/news/{newsId}", method = RequestMethod.GET)
	public String view(Model model,@PathVariable int newsId) throws ServiceException {
		News news=newsService.getNews(newsId);
		
		List <Author> authors = authorService.getAuthorList();
		List <Tag> tags = tagService.getTagList();
		
		model.addAttribute("authors", authors);
		model.addAttribute("tags", tags);
		
		if (null!=news){
			model.addAttribute("news", news);
			model.addAttribute("authorId", newsService.getAuthorIdByNewsId(newsId));

			Set<Long> idsSet = Converter.TagListToLongIdsSet(newsService.getTagListByNewsId(newsId));
			
			model.addAttribute("newsTags", idsSet);
		}
		return "news";
	}
	
	@RequestMapping(value = "/news_add_update", method = RequestMethod.POST)
	public String add(@RequestParam(value = "authorId", required = true) long authorId,
					  @RequestParam(value = "newsId", required = false) Long newsId,
					  @RequestParam(value = "title", required = true) String title,
					  @RequestParam(value = "shortText", required = true) String shortText,
					  @RequestParam(value = "fullText", required = true) String fullText,
					  @RequestParam(value = "modificationDate", required = true) Date modificationDate,
					  @RequestParam(value = "tags", required = false) List<Long> tags) throws ServiceException {
		
		if (null!=newsId){
			extendedService.editNewsWithDependencies(authorId, newsId, title, shortText, fullText, modificationDate, tags);
			return "redirect:/newsList";
		} else {
			newsId=extendedService.addNewsWithDependencies(authorId, title, shortText, fullText, modificationDate, tags);

			System.out.println(newsId);
			return "redirect:/news/0";
		}
	}
}
