package by.epam.feliks.kaminski.newsmanagement.dao;

import java.util.List;

import by.epam.feliks.kaminski.newsmanagement.entity.Comment;
import by.epam.feliks.kaminski.newsmanagement.exceptions.DAOException;

/**
 * This is an interface for basic one-step operations with defined data storage (ds) on Comment entity.
 * 
 * @author Feliks_Kaminski
 *
 */
public interface IDAOComment {

	
		/**
		 * The method inserts Comment entity into the ds and returns it's id.
		 * 
		 * @param newsId
		 * @param comment_text
		 * @return id of the inserted comment
		 * @throws DAOException
		 */
	public long addComment(long newsId, String comment_text) throws DAOException;

		/**
		 * The method gets a Comment entity and updates it's fields in the ds using it's id.
		 * 
		 * @param Comment entity
		 * @throws DAOException
		 */
	public void editComment(Comment comment) throws DAOException;

		/**
		 * The method returns a Comment entity using it's id.
		 * 
		 * @param commentId
		 * @return Comment entity
		 * @throws DAOException
		 */
	public Comment getComment(long commentId) throws DAOException;

		/**
		 * The method removes a Comment entity from the ds using it's id.
		 * 
		 * @param commentId
		 * @throws DAOException
		 */
	public void deleteComment(long commentId) throws DAOException;

	/**
	 * The method removes a Comment entities from the ds using News id.
	 * 
	 * @param newsId
	 * @throws DAOException
	 */
	public void deleteCommentByNewsId(long newsId) throws DAOException;
	
	
		/**
		 * The method returns a list of Comment entities.
		 * Requires 3 numbers.
		 * The first number is a News entity's id.
		 * The second number is a position of the 1st Comment in the list ordered by creation date desc.
		 * The third number is a position of the last Comment in list. 0 means that it will be the last Comment in list.
		 * getCommentsIdsListByNewsId(x,1, 0) returns list of all Comments of the News with id = x.
		 * 
		 * @param newsId
		 * @param commentNFirst
		 * @param commentNLast
		 * @return list with comment entities
		 * @throws DAOException
		 */
	public List<Comment> getCommentsIdsListByNewsId(long newsId,
			long commentNFirst, long commentNLast) throws DAOException;
}
