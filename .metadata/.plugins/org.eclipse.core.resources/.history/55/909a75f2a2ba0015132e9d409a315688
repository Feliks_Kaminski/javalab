package by.epam.feliks.kaminski.newsmanagement.service.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import by.epam.feliks.kaminski.newsmanagement.dao.IDAOAuthor;
import by.epam.feliks.kaminski.newsmanagement.dao.IDAOComment;
import by.epam.feliks.kaminski.newsmanagement.dao.IDAONews;
import by.epam.feliks.kaminski.newsmanagement.dao.IDAOTag;
import by.epam.feliks.kaminski.newsmanagement.dao.IDAOUser;
import by.epam.feliks.kaminski.newsmanagement.exceptions.DAOException;
import by.epam.feliks.kaminski.newsmanagement.exceptions.ServiceException;
import by.epam.feliks.kaminski.newsmanagement.service.IServiceExtended;

/**
 * This Service id used for complex multiDAO commands.
 * 
 * @author Feliks_Kaminski
 *
 */
@Transactional
public class ServiceExtended implements IServiceExtended {
	
	private IDAOAuthor authorDAO;
	private IDAOComment commentDAO;
	private IDAONews newsDAO;
	private IDAOTag tagDAO;
	private IDAOUser userDAO;
	
	public void setAuthorDAO(IDAOAuthor authorDAO) {
		this.authorDAO = authorDAO;
	}
	public void setCommentDAO(IDAOComment commentDAO) {
		this.commentDAO = commentDAO;
	}
	public void setNewsDAO(IDAONews newsDAO) {
		this.newsDAO = newsDAO;
	}
	public void setTagDAO(IDAOTag tagDAO) {
		this.tagDAO = tagDAO;
	}
	
	public void setUserDAO(IDAOUser userDAO) {
		this.userDAO = userDAO;
	}
	
	
	
	
	/* (non-Javadoc)
	 * @see by.epam.feliks.kaminski.newsmanagement.service.IServiceExtended#addNewsWithDependencies(long, java.lang.String, java.lang.String, java.lang.String, java.util.List)
	 */
	public long addNewsWithDependencies(long author_id,String title, String short_text, 
			String full_text, List<Long> tags) throws ServiceException {
		long idN;
		try {
			idN=newsDAO.addNews(title, short_text, full_text);
			newsDAO.addNews_author(idN, author_id);
			for (long tag_id:tags){
				newsDAO.addNews_tag(idN, tag_id);
			}
		} catch (DAOException e){
			throw(new ServiceException(e));
		}
		return idN;
	}
	
	public void deleteNewsWithDependencies(long news_id) throws ServiceException{
		try {
			long author_id = newsDAO.getAuthorIdByNewsId(news_id);
			newsDAO.deleteNews_author(news_id, author_id);
			
			List <Long> tagsIds= newsDAO.getTagIdListByNewsId(news_id);
			for (long tag_id:tagsIds){
				newsDAO.deleteNews_tag(news_id, tag_id);
			}
			
			this.deleteCommentsByNewsId(news_id);
			
			newsDAO.deleteNews(news_id);
		} catch (DAOException e){
			throw(new ServiceException(e));
		}
	}
	
	public void deleteAuthorWithDependencies(long author_id) throws ServiceException{
		try {
			List <Long> newsIds = newsDAO.getNewsIdListByAuthorId(author_id);
			for (long news_id:newsIds){
				this.deleteNewsWithDependencies(news_id);
			}
			
			authorDAO.deleteAuthor(author_id);
		} catch (DAOException e){
			throw(new ServiceException(e));
		}
	}

	public void deleteTagWithDependencies(long tag_id) throws ServiceException {
		try {
			
			List <Long> newsIds= newsDAO.getNewsIdListByTagId(tag_id);
			for (long news_id:newsIds){
				newsDAO.deleteNews_tag(news_id, tag_id);
			}
			
			tagDAO.deleteTag(tag_id);
		} catch (DAOException e){
			throw(new ServiceException(e));
		}
	}
	

	public void deleteCommentsByNewsId(long news_id) throws ServiceException{
		try {
			List <Long> commentsIds = commentDAO.getCommentsIdsListByNewsId(news_id, 1, 0);
			for (long comment_id:commentsIds){
				commentDAO.deleteComment(comment_id);
			}
		} catch (DAOException e){
			throw(new ServiceException(e));
		}
	}
	
	public void deleteUserWithDependencies(long userId) throws ServiceException {
		try {
			userDAO.deleteUserRole(userId);
			userDAO.deleteUser(userId);
		} catch (DAOException e){
			throw(new ServiceException(e));
		}
	}
	
}
