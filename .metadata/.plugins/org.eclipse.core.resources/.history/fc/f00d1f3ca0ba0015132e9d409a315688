package by.epam.feliks.kaminski.newsmanagement.dao;

import java.util.List;

import by.epam.feliks.kaminski.newsmanagement.entity.News;
import by.epam.feliks.kaminski.newsmanagement.entity.SeachCriteria;
import by.epam.feliks.kaminski.newsmanagement.exceptions.DAOException;



/**
 * This is an interface for basic one-step operations with defined data storage (ds) on News entity.
 * 
 * @author Feliks_Kaminski
 *
 */
public interface IDAONews {
	
	/**
	 * The method inserts a News entity into the ds and returns it's id.
	 * 
	 * @param title
	 * @param short_text
	 * @param full_text
	 * @return news id
	 * @throws DAOException
	 */
	public long addNews(String title, String short_text, String full_text) throws DAOException;

	/**
	 * The method gets a News entity and updates it's fields in the ds using it's id.
	 * 
	 * @param News entity
	 * @throws DAOException
	 */
	public void editNews(News news) throws DAOException;

	/**
	 * The method returns a News entity using it's id.
	 * 
	 * @param news_id
	 * @return News entity
	 * @throws DAOException
	 */
	public News getNews(long news_id) throws DAOException;

	/**
	 * The method removes a News from the ds using it's id.
	 * 
	 * @param news_id
	 * @throws DAOException
	 */
	public void deleteNews(long news_id) throws DAOException;

	/**
	 * The method returns a list of News entities from list of all news ordered by comment count desc and creationDate desc
	 * using the positions of the first and last news. 0 in second param means the last position in list of all news.
	 * 
	 * @param newsNFirst
	 * @param newsNLast
	 * @return list of News
	 * @throws DAOException
	 */
	public List<News> getBestNewsList(long newsNFirst, long newsNLast) throws DAOException;
	
	/**
	 * The method returns a list of News entities from list of all news ordered by count of comments desc and creationDate desc
	 * 
	 * @param SearchCriteria entity
	 * @return list of News
	 * @throws DAOException
	 */
	public List<News> getNewsListByCriteria(SeachCriteria SC) throws DAOException;
	
	//News_author
	
	/**
	 * This method adds dependency between News and Author
	 * 
	 * @param news_id
	 * @param author_id
	 * @throws DAOException
	 */
	public void addNews_author(long news_id, long author_id) throws DAOException;

	/**
	 * This method gets Author id by News id
	 * 
	 * @param news_id
	 * @return Author id
	 * @throws DAOException
	 */
	public long getAuthorIdByNewsId(long news_id) throws DAOException;

	/**
	 * This method deletes dependency between News and Author
	 * 
	 * @param news_id
	 * @param author_id
	 * @throws DAOException
	 */
	public void deleteNews_author(long news_id, long author_id) throws DAOException;

	/**
	 * This method returns list of News entity by Author id
	 * 
	 * @param author_id
	 * @return list of News
	 * @throws DAOException
	 */
	public List<Long> getNewsIdListByAuthorId(long author_id) throws DAOException;
	
	//News_tag
	
	/**
	 * This method adds dependency between News and Tag
	 * 
	 * @param news_id
	 * @param tag_id
	 * @throws DAOException
	 */
	public void addNews_tag(long news_id, long tag_id) throws DAOException;

	/**
	 * This method gets Tag ids by News id
	 * 
	 * @param tag_id
	 * @return list of Tag ids
	 * @throws DAOException
	 */
	public List<Long> getNewsIdListByTagId(long tag_id) throws DAOException;

	/**
	 * The method removes a Tag from the ds using it's id.
	 * 
	 * @param news_id
	 * @param tag_id
	 * @throws DAOException
	 */
	public void deleteNews_tag(long news_id, long tag_id) throws DAOException;

	/**
	 * This method returns list of Tags entity by News id
	 * 
	 * @param news_id
	 * @return List of Tag ids
	 * @throws DAOException
	 */
	public List<Long> getTagIdListByNewsId(long news_id) throws DAOException;
}
