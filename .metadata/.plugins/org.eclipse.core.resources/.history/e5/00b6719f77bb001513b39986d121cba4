package by.epam.feliks.kaminski.newsmanagement.service;

import java.util.List;

import by.epam.feliks.kaminski.newsmanagement.entity.News;
import by.epam.feliks.kaminski.newsmanagement.entity.SeachCriteria;
import by.epam.feliks.kaminski.newsmanagement.exceptions.DAOException;
import by.epam.feliks.kaminski.newsmanagement.exceptions.ServiceException;

/**
 * This is an interface for operations on News entity.
 * 
 * @author Feliks_Kaminski
 *
 */
public interface IServiceNews {
	
	/**
	 * The method inserts a News entity and returns it's id.
	 * 
	 * @param title
	 * @param short_text
	 * @param full_text
	 * @return news id
	 * @throws DAOException
	 */
	public long addNews(String title, String short_text, String full_text) throws ServiceException;

	public void editNews(News news) throws ServiceException;

	public News getNews(long newsId) throws ServiceException;

	public void deleteNews(long newsId) throws ServiceException;

	/**
	 * The method returns a list of News entities from list of all news ordered by comment count desc and creationDate desc
	 * using the positions of the first and last news. 0 in second param means the last position in list of all news.
	 * 
	 * @param newsNFirst
	 * @param newsNLast
	 * @return list of News
	 * @throws DAOException
	 */
	public List<News> getBestNewsList(long newsNFirst, long newsNLast) throws ServiceException;

	/**
	 * The method returns a list of News entities from list of all news ordered by count of comments desc and creationDate desc
	 * 
	 * @param SearchCriteria entity
	 * @return list of News
	 * @throws DAOException
	 */
	public List<News> getNewsListByCriteria(SeachCriteria SC) throws ServiceException;
	
	//News_author
	
	/**
	 * This method adds dependency between News and Author
	 * 
	 * @param newsId
	 * @param authorId
	 * @throws DAOException
	 */
	public void addNews_author(long newsId, long authorId) throws ServiceException;

	/**
	 * This method gets Author id by News id
	 * 
	 * @param newsId
	 * @return Author id
	 * @throws DAOException
	 */
	public long getAuthorIdByNewsId(long newsId) throws ServiceException;

	/**
	 * This method deletes dependency between News and Author
	 * 
	 * @param newsId
	 * @param authorId
	 * @throws DAOException
	 */
	public void deleteNews_author(long newsId, long authorId) throws ServiceException;

	/**
	 * This method returns list of News entity by Author id
	 * 
	 * @param authorId
	 * @return list of News
	 * @throws DAOException
	 */
	public List<Long> getNewsIdListByAuthorId( long authorId) throws ServiceException;
	
	//News_tag
	
	/**
	 * This method adds dependency between News and Tag
	 * 
	 * @param newsId
	 * @param tagId
	 * @throws DAOException
	 */
	public void addNews_tag(long newsId, long tagId) throws ServiceException;

	/**
	 * This method gets News ids by Tag id
	 * 
	 * @param tagId
	 * @return list of News ids
	 * @throws DAOException
	 */
	public List<Long> getNewsIdListByTagId(long tagId) throws ServiceException;

	/**
	 * The method removes a Tag using it's id.
	 * 
	 * @param newsId
	 * @param tagId
	 * @throws DAOException
	 */
	public void deleteNews_tag(long newsId, long tagId) throws ServiceException;

	/**
	 * This method returns list of Tags entity by News id
	 * 
	 * @param newsId
	 * @return List of Tag ids
	 * @throws DAOException
	 */
	public List<Long> getTagIdListByNewsId(long newsId) throws ServiceException;
}
