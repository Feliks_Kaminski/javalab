package by.epam.feliks.kaminski.newsmanagement.dao.impl;


import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.beans.factory.annotation.Autowired;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;

import by.epam.feliks.kaminski.newsmanagement.dao.IDAOComment;
import by.epam.feliks.kaminski.newsmanagement.entity.Comment;
import by.epam.feliks.kaminski.newsmanagement.exceptions.DAOException;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations= {"classpath:test_spring.xml"})
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class })
@DatabaseSetup({"classpath:NewsTestDS.xml","classpath:CommentTestDS.xml"})
@DatabaseTearDown(value = {"classpath:NewsTestDS.xml","classpath:CommentTestDS.xml"},type = DatabaseOperation.DELETE_ALL)

public class DAOCommentTest {
	@Autowired
	IDAOComment commentDAO;
	
	@Test
	public void testAddComment() throws DAOException {
			String commentText = "NewTestComment";
			long newsId = 11;
			long cId = commentDAO.addComment(newsId,commentText);
			Comment c1 = commentDAO.getComment(cId);
			
			assertEquals(c1.getComment_text(), commentText);
			assertEquals(c1.getNewsId(), newsId);
	}
	
	@Test
	public void testEditComment() throws DAOException {
			Comment c1 = commentDAO.getComment(4);
			c1.setComment_text("EditedCT");
			commentDAO.editComment(c1);
			Comment c2 = commentDAO.getComment(4);
			assertEquals(c1, c2);
	}
	
	@Test
	public void testGetComment() throws DAOException {
			Comment c1 = commentDAO.getComment(11);
			assertNotNull(c1);
			commentDAO.deleteComment(11);
			c1 = commentDAO.getComment(11);
			assertNull(c1);
	}

	@Test
	public void testDeleteComment() throws DAOException {
			Comment c = commentDAO.getComment(13);
			assertNotNull(c);
			commentDAO.deleteComment(13);
			c = commentDAO.getComment(13);
			assertNull(c);
	}
	
	@Test
	public void testGetCommentListByNewsId() throws DAOException {
			List<Long> l = commentDAO.getCommentsIdsListByNewsId(6, 1, 0);
			assertNotNull(l);
			assertFalse(l.isEmpty());
			commentDAO.deleteComment(9);
			commentDAO.deleteComment(10);
			l = commentDAO.getCommentsIdsListByNewsId(6,1,0);
			assertTrue(l.isEmpty());
	}
			
	
}


