package by.epam.feliks.kaminski.newsmanagement.service.impl;

import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import by.epam.feliks.kaminski.newsmanagement.dao.IDAOAuthor;
import by.epam.feliks.kaminski.newsmanagement.dao.IDAOComment;
import by.epam.feliks.kaminski.newsmanagement.dao.IDAONews;
import by.epam.feliks.kaminski.newsmanagement.dao.IDAOTag;
import by.epam.feliks.kaminski.newsmanagement.dao.IDAOUser;
import by.epam.feliks.kaminski.newsmanagement.entity.Comment;
import by.epam.feliks.kaminski.newsmanagement.exceptions.DAOException;
import by.epam.feliks.kaminski.newsmanagement.exceptions.ServiceException;

@RunWith(MockitoJUnitRunner.class)
public class ServiceExtendedTest {

	@Mock
	private IDAOAuthor authorDAO;
	@Mock
	private IDAOComment commentDAO;
	@Mock
	private IDAONews newsDAO;
	@Mock
	private IDAOTag tagDAO;
	@Mock
	private IDAOUser userDAO;
	
	
	@InjectMocks private ServiceExtended extendedService;
	
	@Test
	public void addNewsWithDependenciesTest() throws DAOException,ServiceException{
		
		long idN=123L;
		long idA=234L;
		String title = "T";
		String short_text = "LT";
		String full_text = "FT";
		
		List <Long> tagsIds = new ArrayList<Long>();
		
		tagsIds.add(456L);
		tagsIds.add(567L);
		tagsIds.add(987L);
		

		when(newsDAO.addNews(title, short_text, full_text)).thenReturn(idN);
		
		extendedService.addNewsWithDependencies(idA, title, short_text, full_text, tagsIds);
		
		
		
		verify(newsDAO).addNews(title, short_text, full_text);
		verify(newsDAO).addNewsAuthor(idN, idA);
		verify(newsDAO).addNewsTag(idN, 456L);
		verify(newsDAO).addNewsTag(idN, 567L);
		verify(newsDAO).addNewsTag(idN, 987L);

	}
	
	@Test
	public void deleteNewsWithDependenciesTest() throws DAOException,ServiceException{
		long newsId = 123L;
		long authorId=234L;
		List <Long> tagsIds = new ArrayList<Long>();
		
		tagsIds.add(456L);
		tagsIds.add(567L);
		tagsIds.add(987L);
		
		when(newsDAO.getAuthorIdByNewsId(newsId)).thenReturn(authorId);
		doNothing().when(newsDAO).deleteNewsAuthor(newsId, authorId);
		when(newsDAO.getTagIdListByNewsId(newsId)).thenReturn(tagsIds);
		doNothing().when(newsDAO).deleteNewsTag(anyLong(),anyLong());

		doNothing().when(commentDAO).deleteComment(newsId);

		extendedService.deleteNewsWithDependencies(newsId);
		
		verify(newsDAO).getAuthorIdByNewsId(newsId);
		verify(newsDAO).deleteNewsAuthor(newsId, authorId);
		verify(newsDAO).getTagIdListByNewsId(newsId);
		verify(newsDAO).deleteNewsTag(newsId, 456L);
		verify(newsDAO).deleteNewsTag(newsId, 567L);
		verify(newsDAO).deleteNewsTag(newsId, 987L);
		verify(newsDAO).deleteNews(newsId);
		verifyNoMoreInteractions(newsDAO);
	}
	
	@Test
	public void deleteAuthorWithDependenciesTest() throws DAOException,ServiceException{
		long authorId = 123L;
		List <Long> newsIds = new ArrayList<Long>();
		
		newsIds.add(456L);
		newsIds.add(567L);
		
		when(newsDAO.getNewsIdListByAuthorId(authorId)).thenReturn(newsIds);
		doNothing().when(newsDAO).deleteNews(anyLong());
		doNothing().when(authorDAO).deleteAuthor(authorId);

		extendedService.deleteAuthorWithDependencies(authorId);
		
		verify(newsDAO).getNewsIdListByAuthorId(authorId);
		verify(newsDAO).deleteNews(456L);
		verify(newsDAO).deleteNews(567L);
		verify(authorDAO).deleteAuthor(authorId);
		verifyNoMoreInteractions(authorDAO);
	}

	@Test
	public void deleteTagWithDependenciesTest() throws DAOException,ServiceException{
		
		long tagId = 123L;
		List <Long> newsIds = new ArrayList<Long>();
		
		newsIds.add(456L);
		newsIds.add(567L);
		newsIds.add(987L);
		
		when(newsDAO.getNewsIdListByTagId(tagId)).thenReturn(newsIds);
		doNothing().when(newsDAO).deleteNewsTag(anyLong(),anyLong());
		doNothing().when(tagDAO).deleteTag(tagId);

		extendedService.deleteTagWithDependencies(tagId);
		
		verify(newsDAO).getNewsIdListByTagId(tagId);
		verify(newsDAO).deleteNewsTag(456L, tagId);
		verify(newsDAO).deleteNewsTag(567L, tagId);
		verify(newsDAO).deleteNewsTag(987L, tagId);
		verify(tagDAO).deleteTag(tagId);
		verifyNoMoreInteractions(tagDAO);
		verifyNoMoreInteractions(newsDAO);
	}
	
	@Test
	public void deleteCommentsByNewsIdTest() throws DAOException,ServiceException{
		long newsId =123L;
		List <Comment> comments = new ArrayList<Comment>();
		
		Comment comment1 = mock(Comment.class);
		Comment comment2 = mock(Comment.class);
		
		comments.add(comment1);
		comments.add(comment2);
		
		when(commentDAO.getCommentsIdsListByNewsId(newsId, 1, 0)).thenReturn(comments);
		doNothing().when(commentDAO).deleteComment(anyLong());
		
		extendedService.deleteCommentsByNewsId(newsId);

		verify(commentDAO).getCommentsIdsListByNewsId(newsId, 1, 0);
		verify(commentDAO).deleteComment(323);
		verify(commentDAO).deleteComment(324);
		verifyNoMoreInteractions(commentDAO);
	}
	
	@Test
	public void deleteUserWithDependenciesTest() throws DAOException,ServiceException{
		long userId=123L;
		extendedService.deleteUserWithDependencies(userId);
		verify(userDAO).deleteUserRole(userId);
		verify(userDAO).deleteUser(userId);
		verifyNoMoreInteractions(userDAO);
	}
}
