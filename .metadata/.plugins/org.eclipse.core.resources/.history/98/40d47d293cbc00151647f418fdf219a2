package by.epam.feliks.kaminski.newsmanagement.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import by.epam.feliks.kaminski.newsmanagement.dao.IDAOAuthor;
import by.epam.feliks.kaminski.newsmanagement.entity.Author;
import by.epam.feliks.kaminski.newsmanagement.exceptions.DAOException;
public class DAOAuthor implements IDAOAuthor {
	private static final String ADDAUTHOR = "insert into AUTHOR (AUTHOR_ID, AUTHOR_NAME) values (seq_author_id.nextval,?)";
	private static final String EDITAUTHOR = "update AUTHOR set AUTHOR_NAME=?, EXPIRED=? where AUTHOR_ID=?";
	private static final String GETAUTHORBYID = "select AUTHOR_ID, AUTHOR_NAME, EXPIRED from AUTHOR where AUTHOR_ID=?";
	private static final String GETAUTHORBYNAME = "select AUTHOR_ID, AUTHOR_NAME, EXPIRED from AUTHOR where AUTHOR_NAME=?";
	private static final String DELETEAUTHOR = "delete from AUTHOR where AUTHOR_ID=?";
	private static final String GETAUTHORLIST = "select AUTHOR_ID, AUTHOR_NAME "
											  + "from "
											  + "( select AUTHOR_ID, AUTHOR_NAME, ROW_NUMBER() OVER (ORDER BY AUTHOR_NAME) n "
											  + "  from AUTHOR "
											  + "  where EXPIRED is null "
											  + ") sml "
											  + "where sml.n between ? and decode (?,0,sml.n,?) "
											  + "order by AUTHOR_NAME";

	// "select AUTHOR_ID, AUTHOR_NAME from AUTHOR where EXPIRED is null and (? is null or AUTHOR_NAME>?) and rownum<=decode(?,0,rownum,?) order by AUTHOR_NAME";
	// "select AUTHOR_ID, AUTHOR_NAME from AUTHOR where EXPIRED is null and AUTHOR_NAME>? and rownum<=? order by AUTHOR_NAME";
	// "select AUTHOR_ID, AUTHOR_NAME from (select AUTHOR_ID, AUTHOR_NAME,"
	// +
	// " ROW_NUMBER() OVER (ORDER BY AUTHOR_NAME) n from AUTHOR where EXPIRED is null and AUTHOR_NAME>?) sml where sml.n<=? order by AUTHOR_NAME";

	// "select AUTHOR_ID, AUTHOR_NAME from AUTHOR where EXPIRED is null and AUTHOR_ID>? and rownum<=? order by AUTHOR_ID"

	private ADAOKey key;
	
	public DAOAuthor(ADAOKey key){
		this.key=key;
	}
	
	public long addAuthor(String name) throws DAOException {
		Connection con = key.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		long id=0;
		try {
			ps = con.prepareStatement(ADDAUTHOR, new String[] { "AUTHOR_ID" });
			ps.setString(1, name);
			ps.executeUpdate();
			rs = ps.getGeneratedKeys();
			if (rs.next()) {
				id=rs.getLong(1);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			key.releaseDAOKey(con, ps, rs);
		}
		return id;
	}

	public void editAuthor(Author author) throws DAOException {
		Connection con = key.getConnection();
		PreparedStatement ps = null;
		try {
			ps = con.prepareStatement(EDITAUTHOR);
			ps.setLong(3, author.getId());
			ps.setString(1, author.getName());
			ps.setTimestamp(2, author.getExpired());
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			key.releaseDAOKey(con, ps, null);
		}
	}

	public Author getAuthor(long authorId) throws DAOException {
		Connection con = key.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		Author author = null;
		try {
			ps = con.prepareStatement(GETAUTHORBYID);
			ps.setLong(1, authorId);
			rs = ps.executeQuery();
			if (rs.next()) {
				author = new Author(rs.getLong("AUTHOR_ID"),
						rs.getString("AUTHOR_NAME"));
				author.setExpired(rs.getTimestamp("EXPIRED"));
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			key.releaseDAOKey(con, ps, rs);
		}
		return author;
	}

	public Author getAuthor(String name) throws DAOException {
		Connection con = key.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		Author author = null;
		try {
			ps = con.prepareStatement(GETAUTHORBYNAME);
			ps.setString(1, name);
			rs = ps.executeQuery();
			if (rs.next()) {
				author = new Author(rs.getLong("AUTHOR_ID"),
						rs.getString("AUTHOR_NAME"));
				author.setExpired(rs.getTimestamp("EXPIRED"));
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			key.releaseDAOKey(con, ps, rs);
		}
		return author;
	}

	public void deleteAuthor(long authorId) throws DAOException {
		Connection con = key.getConnection();
		PreparedStatement ps = null;
		try {
			ps = con.prepareStatement(DELETEAUTHOR);
			ps.setLong(1, authorId);
			ps.executeUpdate();
			ps.close();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			key.releaseDAOKey(con, ps, null);
		}
	}

	public List<Author> getAuthorList(long authurNFirst,
			long authurNLast) throws DAOException {
		Connection con = key.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		Author author = null;
		List<Author> authorList = new ArrayList<Author>();
		try {
			ps = con.prepareStatement(GETAUTHORLIST);
			ps.setLong(1, authurNFirst);
			ps.setLong(2, authurNLast);
			ps.setLong(3, authurNLast);
			rs = ps.executeQuery();
			while (rs.next()) {
				author = new Author(rs.getLong("AUTHOR_ID"),
						rs.getString("AUTHOR_NAME"));
				// author.setExpired(rs.getTimestamp("EXPIRED"));
				authorList.add(author);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			key.releaseDAOKey(con, ps, rs);
		}
		return authorList;
	}
}
