package by.epam.feliks.kaminski.newsmanagement.service.impl;

import java.util.List;

import org.apache.log4j.Logger;

import by.epam.feliks.kaminski.newsmanagement.App;
import by.epam.feliks.kaminski.newsmanagement.dao.IDAONews;
import by.epam.feliks.kaminski.newsmanagement.entity.News;
import by.epam.feliks.kaminski.newsmanagement.entity.SeachCriteria;
import by.epam.feliks.kaminski.newsmanagement.exceptions.DAOException;
import by.epam.feliks.kaminski.newsmanagement.exceptions.ServiceException;
import by.epam.feliks.kaminski.newsmanagement.service.IServiceNews;

public class ServiceNews implements IServiceNews{
    public static Logger logger = Logger.getLogger(App.class.getName());
	private IDAONews newsDAO;

	public ServiceNews(IDAONews newsDAO){
		this.newsDAO=newsDAO;
	}

	public long addNews(String title, String short_text, String full_text) throws ServiceException{
		try {
			return newsDAO.addNews(title, short_text, full_text);
		} catch (DAOException e){
			logger.error(e);
			throw(new ServiceException(e));
		}
	}

	public void editNews(News news) throws ServiceException{
		try {
			newsDAO.editNews(news);
		} catch (DAOException e){
			logger.error(e);
			throw(new ServiceException(e));
		}
	}

	public News getNews(long newsId) throws ServiceException{
		try {
			return newsDAO.getNews(newsId);
		} catch (DAOException e){
			logger.error(e);
			throw(new ServiceException(e));
		}
	}

	public void deleteNews(long newsId) throws ServiceException{
		try {
			newsDAO.deleteNews(newsId);
		} catch (DAOException e){
			logger.error(e);
			throw(new ServiceException(e));
		}
	}

	public List<News> getBestNewsList(long newsNFirst, long newsNLast) throws ServiceException{
		try {
			return newsDAO.getBestNewsList(newsNFirst, newsNLast);
		} catch (DAOException e){
			logger.error(e);
			throw(new ServiceException(e));
		}
	}
	
	public long getBestNewsCount(long newsNFirst, long newsNLast) throws DAOException;
	

	public List<News> getNewsListByCriteria(SeachCriteria SC,long newsNFirst,
			long newsNLast) throws ServiceException{
		try {
			return newsDAO.getNewsListByCriteria(SC,newsNFirst,newsNLast);
		} catch (DAOException e){
			logger.error(e);
			throw(new ServiceException(e));
		}
	}
	
	//News_author
	
	public void addNews_author(long newsId, long authorId) throws ServiceException{
		try {
			newsDAO.addNews_author(newsId, authorId);
		} catch (DAOException e){
			logger.error(e);
			throw(new ServiceException(e));
		}
	}

	public long getAuthorIdByNewsId(long newsId) throws ServiceException{
		try {
			return newsDAO.getAuthorIdByNewsId(newsId);
		} catch (DAOException e){
			logger.error(e);
			throw(new ServiceException(e));
		}
	}

	public void deleteNews_author(long newsId, long authorId) throws ServiceException{
		try {
			newsDAO.deleteNews_author(newsId, authorId);
		} catch (DAOException e){
			logger.error(e);
			throw(new ServiceException(e));
		}
	}

	public List<Long> getNewsIdListByAuthorId( long authorId) throws ServiceException{
		try {
			return newsDAO.getNewsIdListByAuthorId(authorId);
		} catch (DAOException e){
			logger.error(e);
			throw(new ServiceException(e));
		}
	}
	
	//News_tag
	
	public void addNews_tag(long newsId, long tagId) throws ServiceException{
		try {
			newsDAO.addNews_tag(newsId, tagId);
		} catch (DAOException e){
			logger.error(e);
			throw(new ServiceException(e));
		}
	}

	public List<Long> getNewsIdListByTagId(long tagId) throws ServiceException{
		try {
			return newsDAO.getNewsIdListByTagId(tagId);
		} catch (DAOException e){
			logger.error(e);
			throw(new ServiceException(e));
		}
	}

	public void deleteNews_tag(long newsId, long tagId) throws ServiceException{
		try {
			newsDAO.deleteNews_tag(newsId, tagId);
		} catch (DAOException e){
			logger.error(e);
			throw(new ServiceException(e));
		}
	}

	public List<Long> getTagIdListByNewsId(long newsId) throws ServiceException{
		try {
			return newsDAO.getTagIdListByNewsId(newsId);
		} catch (DAOException e){
			logger.error(e);
			throw(new ServiceException(e));
		}
	}
	
}
