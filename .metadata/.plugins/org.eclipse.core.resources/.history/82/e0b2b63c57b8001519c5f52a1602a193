package by.epam.feliks.kaminski.newsmanagement.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import by.epam.feliks.kaminski.newsmanagement.dao.IDAOComment;
import by.epam.feliks.kaminski.newsmanagement.entity.Comment;
import by.epam.feliks.kaminski.newsmanagement.exceptions.DAOException;

public class DAOComment implements IDAOComment{
	private static final String ADD_COMMENT = "insert into COMMENTS (COMMENT_ID,NEWS_ID,COMMENT_TEXT) values (seq_comments_id.nextval,?,?)";
	private static final String EDIT_COMMENT = "update COMMENTS set COMMENT_TEXT=? where COMMENT_ID=?";
	private static final String GET_COMMENT = "select NEWS_ID,COMMENT_TEXT,CREATION_DATE from COMMENTS where COMMENT_ID=?";
	private static final String DELETE_COMMENT = "delete from COMMENTS where COMMENT_ID=?";
	private static final String GET_COMMENT_LIST_BY_NEWS_ID = "select COMMENT_ID,COMMENT_TEXT,CREATION_DATE "
													   + "from "
													   + "( select COMMENT_ID,COMMENT_TEXT,CREATION_DATE,ROW_NUMBER() OVER (ORDER BY CREATION_DATE) n "
													   + "  from COMMENTS "
													   + "  where NEWS_ID=? "
													   + ") sml "
													   + "where sml.n between ? and decode (?,0,sml.n,?) "
													   + "order by CREATION_DATE desc";

	private ADAOKey key;
	
	public DAOComment(ADAOKey key){
		this.key=key;
	}
	
	public long addComment(long news_id, String comment_text) throws DAOException {
		Connection con = key.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		long id = 0;
		try {
			ps = con.prepareStatement(ADD_COMMENT, new String[] { "COMMENT_ID",
					"CREATION_DATE" });
			ps.setLong(1, news_id);
			ps.setString(2, comment_text);
			ps.executeUpdate();
			rs = ps.getGeneratedKeys();
			if (rs.next()) {
				id=rs.getLong(1);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			key.releaseDAOKey(con, ps, rs);
		}
		return id;
	}

	public void editComment(Comment comment) throws DAOException {
		Connection con = key.getConnection();
		PreparedStatement ps = null;
		try {
			ps = con.prepareStatement(EDIT_COMMENT);
			ps.setLong(2, comment.getId());
			ps.setString(1, comment.getComment_text());
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			key.releaseDAOKey(con, ps, null);
		}
	}

	public Comment getComment(long comment_id) throws DAOException {
		Connection con = key.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		Comment comment = null;
		try {
			ps = con.prepareStatement(GET_COMMENT);
			ps.setLong(1, comment_id);
			rs = ps.executeQuery();
			if (rs.next()) {
				comment = new Comment(comment_id, rs.getLong("NEWS_ID"),
						rs.getString("COMMENT_TEXT"),
						rs.getTimestamp("CREATION_DATE"));
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			key.releaseDAOKey(con, ps, rs);
		}
		return comment;
	}

	public void deleteComment(long comment_id) throws DAOException {
		Connection con = key.getConnection();
		PreparedStatement ps = null;
		try {
			ps = con.prepareStatement(DELETE_COMMENT);
			ps.setLong(1, comment_id);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			key.releaseDAOKey(con, ps, null);
		}
	}

	public List<Comment> getCommentsIdListByNewsId(long news_id,
			long commentNFirst, long commentNLast) throws DAOException {
		Connection con = key.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		Comment comment = null;
		List<Comment> commentList = new ArrayList<Comment>();
		try {
			ps = con.prepareStatement(GET_COMMENT_LIST_BY_NEWS_ID);
			ps.setLong(1, news_id);
			ps.setLong(2, commentNFirst);
			ps.setLong(3, commentNLast);
			ps.setLong(4, commentNLast);
			rs = ps.executeQuery();
			while (rs.next()) {
				comment = new Comment(rs.getLong("COMMENT_ID"), news_id,
						rs.getString("COMMENT_TEXT"),
						rs.getTimestamp("CREATION_DATE"));
				commentList.add(comment);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			key.releaseDAOKey(con, ps, rs);
		}
		return commentList;
	}

}
