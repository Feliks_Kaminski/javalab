package by.epam.feliks.kaminski.newsmanagement.service.impl;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import by.epam.feliks.kaminski.newsmanagement.dao.IDAOAuthor;
import by.epam.feliks.kaminski.newsmanagement.dao.IDAOComment;
import by.epam.feliks.kaminski.newsmanagement.dao.IDAONews;
import by.epam.feliks.kaminski.newsmanagement.dao.IDAOTag;
import by.epam.feliks.kaminski.newsmanagement.exceptions.DAOException;
import by.epam.feliks.kaminski.newsmanagement.exceptions.ServiceException;

@RunWith(MockitoJUnitRunner.class)
public class ServiceExtendedTest {

	@Mock
	private IDAOAuthor authorDAO;
	@Mock
	private IDAOComment commentDAO;
	@Mock
	private IDAONews newsDAO;
	@Mock
	private IDAOTag tagDAO;
	
	@InjectMocks private ServiceExtended extendedService;
	
	@Test
	public void addNewsWithDependencies() throws DAOException,ServiceException{
		
		long idN=123L;
		long idA=234L;
		String title = "T";
		String short_text = "LT";
		String full_text = "FT";
		
		List <Long> tagsIds = new ArrayList<Long>();
		
		tagsIds.add(456L);
		tagsIds.add(567L);
		tagsIds.add(987L);
		
		
		
		extendedService.addNewsWithDependencies(idA, title, short_text, full_text, tagsIds);
		
		
		when(newsDAO.addNews(title, short_text, full_text)).thenReturn(idA);
		
		verify(newsDAO).addNews(title, short_text, full_text);
		verify(newsDAO).addNews_author(idN, idA);
		verify(newsDAO).addNews_tag(idN, 456L);
		verify(newsDAO).addNews_tag(idN, 567L);
		verify(newsDAO).addNews_tag(idN, 987L);

	}
	
	@Test
	public void deleteNewsWithDependencies() throws DAOException,ServiceException{
		long news_id = 123L;
		long author_id=234L;
		List <Long> tagsIds = new ArrayList<Long>();
		
		tagsIds.add(456L);
		tagsIds.add(567L);
		tagsIds.add(987L);
		
		when(newsDAO.getAuthorIdByNewsId(news_id)).thenReturn(author_id);
		doNothing().when(newsDAO).deleteNews_author(news_id, author_id);
		when(newsDAO.getTagIdListByNewsId(news_id)).thenReturn(tagsIds);
		doNothing().when(newsDAO).deleteNews_tag(anyLong(),anyLong());

		doNothing().when(commentDAO).deleteComment(news_id);

		extendedService.deleteNewsWithDependencies(news_id);
		
		verify(newsDAO).getAuthorIdByNewsId(news_id);
		verify(newsDAO).deleteNews_author(news_id, author_id);
		verify(newsDAO).getTagIdListByNewsId(news_id);
		verify(newsDAO).deleteNews_tag(news_id, 456L);
		verify(newsDAO).deleteNews_tag(news_id, 567L);
		verify(newsDAO).deleteNews_tag(news_id, 987L);
		verify(newsDAO).deleteNews(news_id);
		verifyNoMoreInteractions(newsDAO);
		verifyNoMoreInteractions(commentDAO);
	}
	
	@Test
	public void deleteAuthorWithDependencies() throws DAOException,ServiceException{
		long author_id = 123L;
		List <Long> newsIds = new ArrayList<Long>();
		
		newsIds.add(456L);
		newsIds.add(567L);
		
		when(newsDAO.getNewsIdListByAuthorId(author_id)).thenReturn(newsIds);
		doNothing().when(newsDAO).deleteNews(anyLong());
		doNothing().when(authorDAO).deleteAuthor(author_id);

		extendedService.deleteAuthorWithDependencies(author_id);
		
		verify(newsDAO).getNewsIdListByAuthorId(author_id);
		verify(newsDAO).deleteNews(456L);
		
		verify(newsDAO).deleteNews(567L);
		verify(authorDAO).deleteAuthor(author_id);
		verifyNoMoreInteractions(authorDAO);
	}

	@Test
	public void deleteTagWithDependencies() throws DAOException,ServiceException{
		
		long tag_id = 123L;
		List <Long> newsIds = new ArrayList<Long>();
		
		newsIds.add(456L);
		newsIds.add(567L);
		newsIds.add(987L);
		
		when(newsDAO.getNewsIdListByTagId(tag_id)).thenReturn(newsIds);
		doNothing().when(newsDAO).deleteNews_tag(anyLong(),anyLong());
		doNothing().when(tagDAO).deleteTag(tag_id);

		extendedService.deleteTagWithDependencies(tag_id);
		
		verify(newsDAO).getNewsIdListByTagId(tag_id);
		verify(newsDAO).deleteNews_tag(456L, tag_id);
		verify(newsDAO).deleteNews_tag(567L, tag_id);
		verify(newsDAO).deleteNews_tag(987L, tag_id);
		verify(tagDAO).deleteTag(tag_id);
		verifyNoMoreInteractions(tagDAO);
		verifyNoMoreInteractions(newsDAO);
	}
	
	@Test
	public void deleteCommentsByNewsIdTest() throws DAOException,ServiceException{
		long news_id =123L;
		List <Long> commentsIds = new ArrayList<Long>();
		
		commentsIds.add(323L);
		commentsIds.add(324L);
		
		when(commentDAO.getCommentsIdsListByNewsId(news_id, 1, 0)).thenReturn(commentsIds);
		doNothing().when(commentDAO).deleteComment(anyLong());
		
		extendedService.deleteCommentsByNewsId(news_id);

		verify(commentDAO).getCommentsIdsListByNewsId(news_id, 1, 0);
		verify(commentDAO).deleteComment(323);
		verify(commentDAO).deleteComment(324);
		verifyNoMoreInteractions(commentDAO);
	}
	

}
