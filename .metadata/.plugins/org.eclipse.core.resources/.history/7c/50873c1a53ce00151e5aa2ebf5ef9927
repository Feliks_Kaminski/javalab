package by.epam.feliks.kaminski.newsmanagement.dao;

import java.util.List;

import by.epam.feliks.kaminski.newsmanagement.entity.News;
import by.epam.feliks.kaminski.newsmanagement.entity.SeachCriteria;
import by.epam.feliks.kaminski.newsmanagement.exceptions.DAOException;



/**
 * This is an interface for operations on News entity.
 * 
 * @author Feliks_Kaminski
 *
 */
public interface IDAONews {
	
	/**
	 * The method inserts a News entity and returns it's id.
	 * 
	 * @param title
	 * @param short_text
	 * @param full_text
	 * @return news id
	 * @throws DAOException
	 */
	public long addNews(String title, String short_text, String full_text) throws DAOException;

	/**
	 * The method gets a News entity and updates it's fields using it's id.
	 * 
	 * @param News entity
	 * @throws DAOException
	 */
	public void editNews(News news) throws DAOException;

	/**
	 * The method returns a News entity using it's id.
	 * 
	 * @param newsId
	 * @return News entity
	 * @throws DAOException
	 */
	public News getNews(long newsId) throws DAOException;

	/**
	 * The method removes a News using it's id.
	 * 
	 * @param newsId
	 * @throws DAOException
	 */
	public void deleteNews(long newsId) throws DAOException;

	/**
	 * The method returns a list of News entities from list of all news ordered by comment count desc and creationDate desc
	 * using the positions of the first and last news. 0 in second param means the last position in list of all news.
	 * 
	 * @param newsNFirst
	 * @param newsNLast
	 * @return list of News
	 * @throws DAOException
	 */
	public List<News> getBestNewsList(long newsNFirst, long newsNLast) throws DAOException;
	
	
	/**
	 * The method returns the count of News entities from list of all news ordered by comment count desc and creationDate desc
	 * 
	 * @return count of News
	 * @throws DAOException
	 */
	public long getBestNewsCount() throws DAOException;
	
	/**
	 * The method returns a list of News entities from list of all news ordered by count of comments desc and creationDate desc
	 * Requires number of the first and last (0 - News at the end of the list) News number you want.
	 * 
	 * @param SearchCriteria entity
	 * @param newsNFirst
	 * @param newsNLast
	 * @return list of News
	 * @throws DAOException
	 */
	public List<News> getNewsListByCriteria(SeachCriteria SC,long newsNFirst,
			long newsNLast) throws DAOException;
	
	
	/**
	 * The method returns the count of News entities from list of all news ordered by count of comments desc and creationDate desc
	 * 
	 * @param SearchCriteria entity
	 * @return count of News
	 * @throws DAOException
	 */
	public long getNewsByCriteriaCount(SeachCriteria SC) throws DAOException;
	
	//NewsAuthor
	
	/**
	 * This method adds dependency between News and Author
	 * 
	 * @param newsId
	 * @param authorId
	 * @throws DAOException
	 */
	public void addNewsAuthor(long newsId, long authorId) throws DAOException;

	/**
	 * This method gets Author id by News id
	 * 
	 * @param newsId
	 * @return Author id
	 * @throws DAOException
	 */
	public long getAuthorIdByNewsId(long newsId) throws DAOException;

	/**
	 * This method deletes dependency between News and Author
	 * 
	 * @param newsId
	 * @param authorId
	 * @throws DAOException
	 */
	public void deleteNewsAuthor(long newsId) throws DAOException;

	/**
	 * This method returns list of News entity by Author id
	 * 
	 * @param authorId
	 * @return list of News
	 * @throws DAOException
	 */
	public List<Long> getNewsIdListByAuthorId(long authorId) throws DAOException;
	
	//News_Tag
	
	/**
	 * This method adds dependency between News and Tag
	 * 
	 * @param newsId
	 * @param tagId
	 * @throws DAOException
	 */
	public void addNewsTag(long newsId, long.. tagIds) throws DAOException;

	
	/**
	 * This method checks dependent between news and tag
	 * 
	 * @param newsId
	 * @param tagId
	 * @return true if combination exists
	 * @throws DAOException
	 */
	public boolean getNewsTagExistance(long newsId,long tagId) throws DAOException;
	
	/**
	 * This method gets News ids by Tag id
	 * 
	 * @param tagId
	 * @return list of News ids
	 * @throws DAOException
	 */
	public List<Long> getNewsIdListByTagId(long tagId) throws DAOException;

	/**
	 * The method removes a News-Tag dependency from the ds using it's ids.
	 * 
	 * @param newsId
	 * @param tagId
	 * @throws DAOException
	 */
	public void deleteNewsTag(long newsId, long tagId) throws DAOException;

	/**
	 * The method removes News-Tag dependencies from the ds using news id.
	 * 
	 * @param newsId
	 * @throws DAOException
	 */
	public void deleteNewsTagByNewsId(long newsId) throws DAOException;
	
	/**
	 * The method removes News-Tag dependencies from the ds using tag id.
	 * 
	 * @param tagId
	 * @throws DAOException
	 */
	public void deleteNewsTagByTagId(long tagId) throws DAOException;
	
	
	/**
	 * This method returns list of Tags ids by News id
	 * 
	 * @param newsId
	 * @return List of Tag ids
	 * @throws DAOException
	 */
	public List<Long> getTagIdListByNewsId(long newsId) throws DAOException;
}
