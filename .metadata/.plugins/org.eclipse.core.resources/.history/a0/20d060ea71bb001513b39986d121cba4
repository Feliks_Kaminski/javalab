package by.epam.feliks.kaminski.newsmanagement.dao;

import java.util.List;

import by.epam.feliks.kaminski.newsmanagement.entity.Author;
import by.epam.feliks.kaminski.newsmanagement.exceptions.DAOException;

/**
 * This is an interface for basic one-step operations with defined data storage (ds) on Author entity.
 * 
 * @author Feliks_Kaminski
 *
 */
public interface IDAOAuthor {
		/**
		 * The method inserts an Author entity into the ds and returns it's id.
		 * 
		 * @param name of the Author
		 * @return Author's id
		 * @throws DAOException
		 */
	public long addAuthor(String name) throws DAOException;

		/**
		 * The method gets an Author entity and updates it's fields in the ds using it's id.
		 * 
		 * @param Author entity
		 * @throws DAOException
		 */
	public void editAuthor(Author author) throws DAOException;

		/**
		 * The method returns an Author entity using it's id.
		 * 
		 * @param author_id
		 * @throws DAOException
		 */
	public Author getAuthor(long author_id) throws DAOException;

		/**
		 * The method returns an Author entity using it's unique name.
		 * 
		 * @param name
		 * @return
		 * @throws DAOException
		 */
	public Author getAuthor(String name) throws DAOException;

		/**
		 * The method removes an Author from the ds using it's id.
		 * 
		 * @param author_id
		 * @throws DAOException
		 */
	public void deleteAuthor(long author_id) throws DAOException;

		/**
		 * The method returns a list of Author entities.
		 * Requires 2 numbers.
		 * The first number is a position of the 1st Author in the list of non-expired Authors ordered by name.
		 * The second number is a position of the last author in list. 0 means that it will be the last Author in list.
		 * getAuthorList(1, 0) returns list of all the non-expired Authors.
		 * 
		 * @param position of the first Author in the ds
		 * @param position of the last Author in the ds. 0 means the position of the last Author in the ds.
		 * @return list with Author entities
		 * @throws DAOException
		 */
	public List<Author> getAuthorList(long authurNFirst, long authurNLast) throws DAOException;
}
