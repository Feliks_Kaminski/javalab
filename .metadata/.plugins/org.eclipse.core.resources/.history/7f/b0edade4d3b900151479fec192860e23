package by.epam.feliks.kaminski.newsmanagement.dao.impl;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import by.epam.feliks.kaminski.newsmanagement.dao.IDAOUser;
import by.epam.feliks.kaminski.newsmanagement.entity.Tag;
import by.epam.feliks.kaminski.newsmanagement.entity.User;
import by.epam.feliks.kaminski.newsmanagement.exceptions.DAOException;

public class DAOUser implements IDAOUser{
	private static final String ADD_USER = "insert into USERS (USER_ID,USER_NAME,LOGIN,PASSWORD) values (seq_user_id.nextval,?,?,?)";
	private static final String UPDATE_USER = "update USERS set USER_NAME=?,LOGIN=?,PASSWORD=? where USER_ID=?";
	private static final String GET_USER_BY_ID = "select USER_NAME,LOGIN,PASSWORD from USERS where USERS_ID=?";
	private static final String GET_USER = "select USERS_ID,USER_NAME from USERS where LOGIN=? and PASSWORD=?";
	private static final String DELETE_USER = "delete from USERS where USERS_ID=?";
	
	private static final String ADD_USER_ROLE = "insert into ROLES (USER_ID,ROLE_NAME) values (?,?)";
	private static final String UPDATE_USER_ROLE = "update ROLES set ROLE_NAME where USER_ID=?";
	private static final String GET_USER_ROLE = "select ROLE_NAME from ROLES where USER_ID=?";
	private static final String DELETE_USER_ROLE = "delete from ROLES where USERS_ID=?";
	
	private ADAOKey key;
	
	public DAOUser(ADAOKey key){
		this.key=key;
	}
	
	public long addUser(String userName, String login, String password) throws DAOException{
		Connection con = key.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		long id = 0;
		try {
			ps = con.prepareStatement(ADD_USER, new String[] { "USER_ID" });
			ps.setString(1, userName);
			ps.setString(2, login);
			ps.setString(3, password);
			ps.executeUpdate();
			rs = ps.getGeneratedKeys();
			if (rs.next()) {
				id=rs.getLong(1);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			key.releaseDAOKey(con, ps, rs);
		}
		return id;
	}
	public void editUser(User user) throws DAOException{
		Connection con = key.getConnection();
		PreparedStatement ps = null;
		try {
			ps = con.prepareStatement(UPDATE_USER);
			ps.setString(1, user.getName());
			ps.setString(2, user.getLogin());
			ps.setString(3, user.getPassword());
			ps.setLong(4, user.getId());
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			key.releaseDAOKey(con, ps, null);
		}
	}
	
	public User getUser(String login, String password) throws DAOException{
		Connection con = key.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		User user = null;
		try {
			ps = con.prepareStatement(GET_USER);
			ps.setString(1, login);
			ps.setString(2, password);
			rs = ps.executeQuery();
			if (rs.next()) {
				user = new User(rs.getLong("USERS_ID"), rs.getString("USER_NAME"), login, password);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			key.releaseDAOKey(con, ps, rs);
		}
		return user;
	}


	public User getUserById(long userId) throws DAOException{
		Connection con = key.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		User user = null;
		try {
			ps = con.prepareStatement(GET_USER_BY_ID);
			ps.setLong(1, userId);
			rs = ps.executeQuery();
			if (rs.next()) {
				user = new User(userId, rs.getString("USER_NAME"), rs.getString("LOGIN"), rs.getString("PASSWORD"));
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			key.releaseDAOKey(con, ps, rs);
		}
		return user;
	}
	
	public void deleteUser(long userId) throws DAOException{
		Connection con = key.getConnection();
		PreparedStatement ps = null;
		try {
			ps = con.prepareStatement(DELETE_USER);
			ps.setLong(1, userId);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			key.releaseDAOKey(con, ps, null);
		}
	}
	

	
	public void addUserRole(long userId,String role) throws DAOException{
		Connection con = key.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = con.prepareStatement(ADD_USER_ROLE);
			ps.setLong(1, userId);
			ps.setString(2, role);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			key.releaseDAOKey(con, ps, rs);
		}
	}
	
	public void updateUserRole(long userId,String role) throws DAOException{
		Connection con = key.getConnection();
		PreparedStatement ps = null;
		try {
			ps = con.prepareStatement(UPDATE_USER_ROLE);
			ps.setLong(1, userId);
			ps.setString(2, role);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			key.releaseDAOKey(con, ps, null);
		}
	}
	
	public String getUserRole(long userId) throws DAOException{
		Connection con = key.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String role = null;
		try {
			ps = con.prepareStatement(GET_USER_ROLE);
			ps.setLong(1, userId);
			rs = ps.executeQuery();
			if (rs.next()) {
				role =rs.getString("ROLE_NAME");
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			key.releaseDAOKey(con, ps, rs);
		}
		return role;
	}
	
	public void deleteUserRole(long userId) throws DAOException{
		Connection con = key.getConnection();
		PreparedStatement ps = null;
		try {
			ps = con.prepareStatement(DELETE_USER_ROLE);
			ps.setLong(1, userId);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			key.releaseDAOKey(con, ps, null);
		}
	}
	
}
