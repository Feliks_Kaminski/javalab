package by.epam.feliks.kaminski.newsmanagement.dao.impl;

import by.epam.feliks.kaminski.newsmanagement.entity.Author;
import by.epam.feliks.kaminski.newsmanagement.entity.News;
import by.epam.feliks.kaminski.newsmanagement.entity.SeachCriteria;
import by.epam.feliks.kaminski.newsmanagement.entity.Tag;
import by.epam.feliks.kaminski.newsmanagement.exceptions.DAOException;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.beans.factory.annotation.Autowired;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations= {"classpath:test_spring.xml"})
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class })


public class DAONewsTest {

	@Autowired
	DAONews newsDAO;
	@Autowired
	DAOComment commentDAO;
	
    @DatabaseSetup({"classpath:NewsTestDS.xml"})
    @DatabaseTearDown(value = {"classpath:NewsTestDS.xml"},type = DatabaseOperation.DELETE_ALL)  
    @Test
	public void testAddNews() throws DAOException {
			String title = "T1";
			String shortText = "ST1";
			String fullText = "FT1";
			long nId = newsDAO.addNews("T1", "ST1", "FT1");
			News news = newsDAO.getNews(nId);
			
			assertEquals(title, news.getTitle());
			assertEquals(shortText, news.getShort_text());
			assertEquals(fullText, news.getFull_text());
	}
    
    @DatabaseSetup({"classpath:NewsTestDS.xml"})
    @DatabaseTearDown(value = {"classpath:NewsTestDS.xml"},type = DatabaseOperation.DELETE_ALL)  
    @Test
	public void testEditNews() throws DAOException {
			assertNotNull(newsDAO.getNews(11));
			newsDAO.deleteNews(11);
			assertNull(newsDAO.getNews(11));
	}
    
    
	
    @DatabaseSetup({"classpath:NewsTestDS.xml"})
    @DatabaseTearDown(value = {"classpath:NewsTestDS.xml"},type = DatabaseOperation.DELETE_ALL)  
    @Test
	public void testGetNews() throws DAOException {
			assertNotNull(newsDAO.getNews(11));
			newsDAO.deleteNews(11);
			assertNull(newsDAO.getNews(11));
	}
    
    
    @DatabaseSetup({"classpath:NewsTestDS.xml","classpath:AuthorTestDS.xml",
    	"classpath:TagTestDS.xml","classpath:CommentTestDS.xml"})
    @DatabaseTearDown(value = {"classpath:NewsTestDS.xml","classpath:AuthorTestDS.xml",
    		"classpath:News_AuthorTestDS.xml","classpath:TagTestDS.xml",
    		"classpath:News_TagTestDS.xml","classpath:CommentTestDS.xml"}
    	,type = DatabaseOperation.DELETE_ALL) 
    @Test
	public void testDeleteNews() throws DAOException {
				assertNotNull(newsDAO.getNews(11));
				for(int i=1;i<5;i++){
					switch (i) {
					case 2:
						commentDAO.deleteComment(13);
						newsDAO.addNews_tag(11, 5);
						break;
					case 3:
						newsDAO.deleteNews_tag(11, 5);
						newsDAO.addNews_author(11, 16);
						break;
					case 4:
						newsDAO.deleteNews_author(11, 16);
						break;
					default:
						break;
					}
					
					try {
							newsDAO.deleteNews(11);
							if (4>i){
								fail("News has been deleted before dependencies!");
							}
					} catch (DAOException e){
						
					} catch (Exception e){
						fail(e.toString());
					}
				}
				
				assertNull(newsDAO.getNews(11));
	}

    
    @DatabaseSetup({"classpath:NewsTestDS.xml","classpath:AuthorTestDS.xml","classpath:News_AuthorTestDS.xml","classpath:TagTestDS.xml","classpath:News_TagTestDS.xml"})
    @DatabaseTearDown(value = {"classpath:NewsTestDS.xml","classpath:AuthorTestDS.xml","classpath:News_AuthorTestDS.xml","classpath:TagTestDS.xml","classpath:News_TagTestDS.xml"}
    	,type = DatabaseOperation.DELETE_ALL) 
    @Test
	public void testGetBestNewsList() throws DAOException {
			assertEquals(8, newsDAO.getBestNewsList(1, 0).size());
			assertEquals(6, newsDAO.getBestNewsList(2, 7).size());
	}
    
	
    @DatabaseSetup({"classpath:NewsTestDS.xml","classpath:AuthorTestDS.xml","classpath:News_AuthorTestDS.xml","classpath:TagTestDS.xml","classpath:News_TagTestDS.xml"})
    @DatabaseTearDown(value = {"classpath:NewsTestDS.xml","classpath:AuthorTestDS.xml","classpath:News_AuthorTestDS.xml","classpath:TagTestDS.xml","classpath:News_TagTestDS.xml"}
    	,type = DatabaseOperation.DELETE_ALL)  
    @Test
	public void testGetNewsListByCriteria() throws DAOException {
    	List<Tag> tagList = new ArrayList<Tag>();
    	tagList.add(new Tag(10,"TestTag10"));
    	tagList.add(new Tag(9,"TestTag9"));
    	SeachCriteria sc;
    	Author a = new Author(11, "TestAuthor11");
			sc = new SeachCriteria(null,tagList);
			assertEquals(6, newsDAO.getNewsListByCriteria(sc).size());
			sc.setAuhor(a);
			assertEquals(2, newsDAO.getNewsListByCriteria(sc).size());
	}
	
	//News_author
    @DatabaseSetup({"classpath:NewsTestDS.xml","classpath:AuthorTestDS.xml"})
    @DatabaseTearDown(value = {"classpath:NewsTestDS.xml","classpath:AuthorTestDS.xml","classpath:News_AuthorTestDS.xml"}
    	,type = DatabaseOperation.DELETE_ALL)
    @Test
	public void testAddNews_author() throws DAOException {
			assertEquals(newsDAO.getAuthorIdByNewsId(10),0);
			newsDAO.addNews_author(10,16);
			assertEquals(newsDAO.getAuthorIdByNewsId(10),16);
	}

    @DatabaseSetup({"classpath:NewsTestDS.xml","classpath:AuthorTestDS.xml","classpath:News_AuthorTestDS.xml"})
    @DatabaseTearDown(value = {"classpath:NewsTestDS.xml","classpath:AuthorTestDS.xml","classpath:News_AuthorTestDS.xml"}
    	,type = DatabaseOperation.DELETE_ALL)  
    @Test
	public void testGetNews_author() throws DAOException {
			assertTrue(newsDAO.getNews_author(10, 16));
			newsDAO.deleteNews_author(10,16);
			assertFalse(newsDAO.getNews_author(10, 16));
	}

    @DatabaseSetup({"classpath:NewsTestDS.xml","classpath:AuthorTestDS.xml","classpath:News_AuthorTestDS.xml"})
    @DatabaseTearDown(value = {"classpath:NewsTestDS.xml","classpath:AuthorTestDS.xml","classpath:News_AuthorTestDS.xml"}
    	,type = DatabaseOperation.DELETE_ALL)
    @Test
	public void testDeleteNews_author() throws DAOException {
			assertTrue(newsDAO.getNews_author(10, 16));
			newsDAO.deleteNews_author(10,16);
			assertFalse(newsDAO.getNews_author(10, 16));
	}
    
    @DatabaseSetup({"classpath:NewsTestDS.xml","classpath:AuthorTestDS.xml","classpath:News_AuthorTestDS.xml"})
    @DatabaseTearDown(value = {"classpath:NewsTestDS.xml","classpath:AuthorTestDS.xml","classpath:News_AuthorTestDS.xml"}
    	,type = DatabaseOperation.DELETE_ALL)
    @Test
	public void testGetNewsIdListByAuthor() throws DAOException {
			long authorId = 16;
			assertEquals(4,newsDAO.getNewsIdListByAuthor(authorId).size());
			newsDAO.deleteNews_author(10, authorId);
			newsDAO.deleteNews_author(11, authorId);
			assertEquals(2,newsDAO.getNewsIdListByAuthor(authorId).size());
	}
	
	//News_tag
	@DatabaseSetup({"classpath:NewsTestDS.xml","classpath:TagTestDS.xml"})
    @DatabaseTearDown(value = {"classpath:NewsTestDS.xml","classpath:TagTestDS.xml","classpath:News_TagTestDS.xml"}
    	,type = DatabaseOperation.DELETE_ALL)
    @Test
	public void testAddNews_tag() throws DAOException {
		assertFalse(newsDAO.getNews_tag(10, 6));
		newsDAO.addNews_tag(10,6);
		assertTrue(newsDAO.getNews_tag(10, 6));
	}
	
	
    @DatabaseSetup({"classpath:NewsTestDS.xml","classpath:TagTestDS.xml","classpath:News_TagTestDS.xml"})
    @DatabaseTearDown(value = {"classpath:NewsTestDS.xml","classpath:TagTestDS.xml","classpath:News_TagTestDS.xml"}
    	,type = DatabaseOperation.DELETE_ALL)
    @Test
	public void testGetNews_tag() throws DAOException {
			assertTrue(newsDAO.getNews_tag(10, 6));
			newsDAO.deleteNews_tag(10,6);
			assertFalse(newsDAO.getNews_tag(10, 6));
	}
	
    @DatabaseSetup({"classpath:NewsTestDS.xml","classpath:TagTestDS.xml","classpath:News_TagTestDS.xml"})
    @DatabaseTearDown(value = {"classpath:NewsTestDS.xml","classpath:TagTestDS.xml","classpath:News_TagTestDS.xml"}
    	,type = DatabaseOperation.DELETE_ALL)
    @Test
	public void testDeleteNews_tag() throws DAOException {
			assertTrue(newsDAO.getNews_tag(10, 6));
			newsDAO.deleteNews_tag(10,6);
			assertFalse(newsDAO.getNews_tag(10, 6));
	}

    

    @DatabaseSetup({"classpath:NewsTestDS.xml","classpath:TagTestDS.xml","classpath:News_TagTestDS.xml"})
    @DatabaseTearDown(value = {"classpath:NewsTestDS.xml","classpath:TagTestDS.xml","classpath:News_TagTestDS.xml"}
    	,type = DatabaseOperation.DELETE_ALL)
    @Test
	public void testGetTagIdListByNewsId() throws DAOException {
			List<Long> l = newsDAO.getTagIdListByNewsId(10);
			assertNotNull(l);
			assertEquals(l.size(),3);
			newsDAO.deleteNews_tag(10,5);
			newsDAO.deleteNews_tag(10,6);
			newsDAO.deleteNews_tag(10,7);
			l = newsDAO.getTagIdListByNewsId(10);
			assertEquals(l.size(),0);
	}
}
