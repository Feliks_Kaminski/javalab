package by.epam.feliks.kaminski.newsmanagement.dao.impl;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;



import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import by.epam.feliks.kaminski.newsmanagement.dao.IDAONews;
import by.epam.feliks.kaminski.newsmanagement.entity.Author;
import by.epam.feliks.kaminski.newsmanagement.entity.News;
import by.epam.feliks.kaminski.newsmanagement.entity.SeachCriteria;
import by.epam.feliks.kaminski.newsmanagement.entity.Tag;
import by.epam.feliks.kaminski.newsmanagement.exceptions.DAOException;


@Transactional(propagation = Propagation.MANDATORY)
public class DAONews implements IDAONews {
	private static final String ADD_NEWS="insert into NEWS (NEWS_ID,TITLE,SHORT_TEXT,FULL_TEXT,MODIFICATION_DATE) values (seq_news_id.nextval,?,?,?,sysdate)";
	private static final String EDIT_NEWS="update NEWS set TITLE=?,SHORT_TEXT=?,FULL_TEXT=?,MODIFICATION_DATE=sysdate where NEWS_ID=?";
	private static final String GET_NEWS="select TITLE,SHORT_TEXT,FULL_TEXT,CREATION_DATE,MODIFICATION_DATE from NEWS where NEWS_ID=?";
	private static final String DELETE_NEWS="delete from NEWS where NEWS_ID=?";
	private static final String GET_NEWS_BY_COMMENTS=" select NEWS_ID,TITLE,SHORT_TEXT,FULL_TEXT, "+
													 "	CREATION_DATE,MODIFICATION_DATE,comment_count, news_count from  "+
												     "    (select NEWS_ID,TITLE,SHORT_TEXT,FULL_TEXT,CREATION_DATE,MODIFICATION_DATE,comment_count,  "+
												 	 "      ROW_NUMBER() OVER (ORDER BY comment_count desc, CREATION_DATE desc) numb,COUNT(*) OVER () news_count "+ 
												     "      from  "+
													 "	     (select n.NEWS_ID,n.TITLE,n.SHORT_TEXT,n.FULL_TEXT,n.CREATION_DATE,n.MODIFICATION_DATE,  "+
												     "         nvl((select count(*) from COMMENTS cm where cm.NEWS_ID=n.NEWS_ID),0) comment_count  "+
												     "	           from NEWS n  "+
													 "	     )sml  "+
													 "    )big  "+
													 " where numb between ? and decode (?,0,numb,?)  "+
													 " order by comment_count desc,CREATION_DATE desc";
	private static final String GET_NEWS_BY_CRITERIA=
	  		"select NEWS_ID,TITLE,SHORT_TEXT,FULL_TEXT,CREATION_DATE,MODIFICATION_DATE,quant,news_count,numb ,comm_q "+
			"from(select n.NEWS_ID,n.TITLE,n.SHORT_TEXT,n.FULL_TEXT,n.CREATION_DATE,n.MODIFICATION_DATE,sml.quant,COUNT(*) OVER () news_count, "+
			"      ROW_NUMBER() OVER (ORDER BY quant desc,comm_q desc, CREATION_DATE desc) numb ,comm_q "+
			"       from (select nt.NEWS_ID,count(*) quant,(select count(*) quant from COMMENTS cm where cm.NEWS_ID=nt.NEWS_ID) comm_q "+
			"             from NEWS_TAG nt "+
			"             where nt.TAG_ID in ( "+
			"             0/*ids*/ "+
			"             ) and (?=0 or nt.NEWS_ID in  "+
			"                 (select NEWS_ID from NEWS_AUTHOR na where na.AUTHOR_ID=?)) "+
			"             group by nt.NEWS_ID "+
			"            ) sml "+
			"       join NEWS n on n.NEWS_ID = sml.NEWS_ID "+
			"     ) big "+
			"where numb between ? and decode (?,0,numb,?) "+
			"order by quant desc,comm_q desc, CREATION_DATE desc ";

	//NewsAuthor
	private static final String ADD_NEWS_AUTHOR = "insert into NEWS_AUTHOR (NEWS_ID,AUTHOR_ID) values (?,?)";
	private static final String GET_AUTHOR_BY_NEWS = "select AUTHOR_ID from NEWS_AUTHOR where NEWS_ID=?";
	private static final String DELETE_NEWS_AUTHOR_BY_NEWS_ID = "delete from NEWS_AUTHOR where NEWS_ID=?";
	private static final String GET_NEWS_LIST_BY_AUTHOR = "select NEWS_ID from NEWS_AUTHOR where AUTHOR_ID=?";
	//News_Tag
	private static final String ADD_NEWS_TAG = "insert into NEWS_TAG (NEWS_ID,TAG_ID) values (?,?)";
	private static final String DOES_NEWS_TAG_EXIST = "SELECT 1 from NEWS_TAG where NEWS_ID=? and TAG_ID=?";
	private static final String GET_NEWS_LIST_BY_TAG = "select NEWS_ID from NEWS_TAG where TAG_ID=?";
	private static final String DELETE_NEWS_TAG = "delete from NEWS_TAG where NEWS_ID=? and TAG_ID=?";
	private static final String DELETE_NEWS_TAG_BY_NEWS_ID = "delete from NEWS_TAG where NEWS_ID=?";
	private static final String DELETE_NEWS_TAG_BY_TAG_ID = "delete from NEWS_TAG where TAG_ID=?";
	private static final String GET_TAG_LIST_BY_NEWS = "select TAG_ID from NEWS_TAG where NEWS_ID=?";
	
	private ADAOKey key;
	
	public DAONews(ADAOKey key){
		this.key=key;
	}
	
	public long addNews(String title, String short_text, String full_text) throws DAOException {
		Connection con=key.getConnection();    	
        PreparedStatement ps = null;
        ResultSet rs = null;
        long id = 0;
        try {
            ps = con.prepareStatement(ADD_NEWS,new String[] { "NEWS_ID","CREATION_DATE","MODIFICATION_DATE" });
            ps.setString(1, title);
            ps.setString(2, short_text);
            ps.setString(3, full_text);
            ps.executeUpdate();
            rs = ps.getGeneratedKeys();
            rs.next();
            id=rs.getLong(1);
        } catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			key.releaseDAOKey(con, ps, rs);
		}
        return id;
	}

	public void editNews(News news) throws DAOException {
		Connection con=key.getConnection();    	
        PreparedStatement ps = null;
        try {
            ps = con.prepareStatement(EDIT_NEWS);
            ps.setLong(4, news.getId());
            ps.setString(1, news.getTitle());
            ps.setString(2, news.getShortText());
            ps.setString(3, news.getFullText());
            ps.executeUpdate();
            news.setModificationDate(getNews(news.getId()).getModificationDate());
        } catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			key.releaseDAOKey(con, ps, null);
		}
	}

	public News getNews(long newsId) throws DAOException {
		Connection con=key.getConnection();    	
        PreparedStatement ps = null;
        ResultSet rs = null;
        News news = null;
        try {
            ps = con.prepareStatement(GET_NEWS);
            ps.setLong(1, newsId);
            rs = ps.executeQuery();
            if (rs.next()) {
            	news = new News(newsId, rs.getString("TITLE"),rs.getString("SHORT_TEXT"),rs.getString("FULL_TEXT"),
            			rs.getTimestamp("CREATION_DATE"),rs.getDate("MODIFICATION_DATE"));
            }
        } catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			key.releaseDAOKey(con, ps, rs);
		}
        return news;
	}

	public void deleteNews(long newsId) throws DAOException {
		Connection con=key.getConnection();    	
        PreparedStatement ps = null;
        try {
            ps = con.prepareStatement(DELETE_NEWS);
            ps.setLong(1, newsId);
            ps.executeUpdate();
        } catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			key.releaseDAOKey(con, ps, null);
		}
	}

	public List<News> getBestNewsList(long newsNFirst,
			long newsNLast) throws DAOException {
		Connection con=key.getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        News news = null;
        List<News> newsList=new ArrayList<News>();
        try {
        	ps = con.prepareStatement(GET_NEWS_BY_COMMENTS);
        	ps.setLong(1, newsNFirst);
        	ps.setLong(2, newsNLast);
        	ps.setLong(3, newsNLast);
            rs = ps.executeQuery();
            while (rs.next()) {
            	news = new News(rs.getLong("NEWS_ID"), rs.getString("TITLE"),rs.getString("SHORT_TEXT"),
            			rs.getString("FULL_TEXT"),rs.getTimestamp("CREATION_DATE"),rs.getDate("MODIFICATION_DATE"));
            	newsList.add(news);
            }
        } catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			key.releaseDAOKey(con, ps, rs);
		}
        return newsList;
	}
	
	public long getBestNewsCount() throws DAOException {
		Connection con=key.getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        long count = 0;
        try {
        	ps = con.prepareStatement(GET_NEWS_BY_COMMENTS);
        	ps.setLong(1, 1L);
        	ps.setLong(2, 1L);
        	ps.setLong(3, 1L);
            rs = ps.executeQuery();
            if (rs.next()) {
            	count=rs.getLong("NEWS_COUNT");
            }
        } catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			key.releaseDAOKey(con, ps, rs);
		}
        return count;
	}
	
	public List<News> getNewsListByCriteria(SeachCriteria SC,long newsNFirst,
			long newsNLast) throws DAOException {
		Connection con=key.getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        News news = null;
        List<News> newsList=new ArrayList<News>();
        try {
        	StringBuilder sb=new StringBuilder("");
        	for (Tag tag : SC.getTagList()) {
				sb.append(tag.getId()+","); 
			}
        	sb.delete(sb.length()-1, sb.length());
        	
        	String newQuery=GET_NEWS_BY_CRITERIA;
        	newQuery=newQuery.replaceAll("(0/[*]ids[*]/)", sb.toString());
        	ps = con.prepareStatement(newQuery);
        	Author author=SC.getAuhor();
        	Long authorId=author==null?0:author.getId();        	
        	ps.setLong(1, authorId);
        	ps.setLong(2, authorId);
        	ps.setLong(3, newsNFirst);
        	ps.setLong(4, newsNLast);
        	ps.setLong(5, newsNLast);
            rs = ps.executeQuery();
            while (rs.next()) {
            	news = new News(rs.getLong("NEWS_ID"), rs.getString("TITLE"),rs.getString("SHORT_TEXT"),
            			rs.getString("FULL_TEXT"),rs.getTimestamp("CREATION_DATE"),rs.getDate("MODIFICATION_DATE"));
            	newsList.add(news);
            }
        } catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			key.releaseDAOKey(con, ps, rs);
		}
        return newsList;
	}
	
	public long getNewsByCriteriaCount(SeachCriteria SC) throws DAOException {
		Connection con=key.getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        long count = 0;
        try {
        	StringBuilder sb=new StringBuilder("");
        	for (Tag tag : SC.getTagList()) {
				sb.append(tag.getId()+","); 
			}
        	sb.delete(sb.length()-1, sb.length());
        	
        	String newQuery=GET_NEWS_BY_CRITERIA;
        	newQuery=newQuery.replaceAll("(0/[*]ids[*]/)", sb.toString());
        	ps = con.prepareStatement(newQuery);
        	Author author=SC.getAuhor();
        	Long authorId=author==null?0:author.getId();        	
        	ps.setLong(1, authorId);
        	ps.setLong(2, authorId);
        	ps.setLong(3, 1);
        	ps.setLong(4, 1);
        	ps.setLong(5, 1);
            rs = ps.executeQuery();
            if (rs.next()) {
            	count=rs.getLong("NEWS_COUNT");
            }
        } catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			key.releaseDAOKey(con, ps, rs);
		}
        return count;
	}
	
	//NewsAuthor
	
	public void addNewsAuthor(long newsId, long authorId) throws DAOException {
		Connection con = key.getConnection();
		PreparedStatement ps = null;
		try {
			ps = con.prepareStatement(ADD_NEWS_AUTHOR);
			ps.setLong(1, newsId);
			ps.setLong(2, authorId);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			key.releaseDAOKey(con, ps, null);
		}
	}

	public long getAuthorIdByNewsId(long newsId) throws DAOException {
		Connection con = key.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		long authorId=0;
		try {
			ps = con.prepareStatement(GET_AUTHOR_BY_NEWS);
			ps.setLong(1, newsId);
			rs = ps.executeQuery();
			if (rs.next()) {
				authorId=rs.getLong("AUTHOR_ID");
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			key.releaseDAOKey(con, ps, rs);
		}
		return authorId;
	}

	public void deleteNewsAuthor(long newsId) throws DAOException {
		Connection con = key.getConnection();
		PreparedStatement ps = null;
		try {
			ps = con.prepareStatement(DELETE_NEWS_AUTHOR_BY_NEWS_ID);
			ps.setLong(1, newsId);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			key.releaseDAOKey(con, ps, null);
		}
	}

	public List<Long> getNewsIdListByAuthorId(long authorId) throws DAOException {
		Connection con = key.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<Long> newsList = new ArrayList<Long>();
		try {
			ps = con.prepareStatement(GET_NEWS_LIST_BY_AUTHOR);
			ps.setLong(1, authorId);
			rs = ps.executeQuery();
			while (rs.next()) {
				newsList.add(rs.getLong("NEWS_ID"));
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			key.releaseDAOKey(con, ps, rs);
		}
		return newsList;
	}

	
	//News_Tag
	
	public void addNewsTag(long newsId, long.. tagIds) throws DAOException {
		Connection con = key.getConnection();
		PreparedStatement ps = null;
		try {
			ps = con.prepareStatement(ADD_NEWS_TAG);
			ps.setLong(1, newsId);
			for(long tagId:tagIds){
				ps.setLong(2, tagId);
				ps.executeUpdate();
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			key.releaseDAOKey(con, ps, null);
		}
	}
	
	public boolean getNewsTagExistance(long newsId,long tagId) throws DAOException {
		Connection con = key.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		boolean exists = false;
		try {
			ps = con.prepareStatement(DOES_NEWS_TAG_EXIST);
			ps.setLong(1, newsId);
			ps.setLong(2, tagId);
			rs = ps.executeQuery();
			if (rs.next()) {
				exists = true;
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			key.releaseDAOKey(con, ps, rs);
		}
		return exists;
	}

	public List<Long> getNewsIdListByTagId(long tagId) throws DAOException {
		Connection con = key.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<Long> newsList = new ArrayList<Long>();
		try {
			ps = con.prepareStatement(GET_NEWS_LIST_BY_TAG);
			ps.setLong(1, tagId);
			rs = ps.executeQuery();
			while (rs.next()) {
				newsList.add(rs.getLong("NEWS_ID"));
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			key.releaseDAOKey(con, ps, rs);
		}
		return newsList;
	}

	public void deleteNewsTag(long newsId, long tagId) throws DAOException {
		Connection con = key.getConnection();
		PreparedStatement ps = null;
		try {
			ps = con.prepareStatement(DELETE_NEWS_TAG);
			ps.setLong(1, newsId);
			ps.setLong(2, tagId);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			key.releaseDAOKey(con, ps, null);
		}
	}

	public void deleteNewsTagByNewsId(long newsId) throws DAOException {
		Connection con = key.getConnection();
		PreparedStatement ps = null;
		try {
			ps = con.prepareStatement(DELETE_NEWS_TAG_BY_NEWS_ID);
			ps.setLong(1, newsId);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			key.releaseDAOKey(con, ps, null);
		}
	}
	
	
	public void deleteNewsTagByTagId(long tagId) throws DAOException {
		Connection con = key.getConnection();
		PreparedStatement ps = null;
		try {
			ps = con.prepareStatement(DELETE_NEWS_TAG_BY_TAG_ID);
			ps.setLong(1, tagId);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			key.releaseDAOKey(con, ps, null);
		}
	}
	
	public List<Long> getTagIdListByNewsId(long newsId) throws DAOException {
		Connection con = key.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<Long> tagList = new ArrayList<Long>();
		try {
			ps = con.prepareStatement(GET_TAG_LIST_BY_NEWS);
			ps.setLong(1, newsId);
			rs = ps.executeQuery();
			while (rs.next()) {
				tagList.add(rs.getLong("TAG_ID"));
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			key.releaseDAOKey(con, ps, rs);
		}
		return tagList;
	}
}
