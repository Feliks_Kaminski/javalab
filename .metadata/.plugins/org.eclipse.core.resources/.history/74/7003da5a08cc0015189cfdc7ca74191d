package by.epam.feliks.kaminski.newsmanagement.service.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;

import by.epam.feliks.kaminski.newsmanagement.App;
import by.epam.feliks.kaminski.newsmanagement.dao.IDAONews;
import by.epam.feliks.kaminski.newsmanagement.entity.News;
import by.epam.feliks.kaminski.newsmanagement.entity.SeachCriteria;
import by.epam.feliks.kaminski.newsmanagement.exceptions.DAOException;
import by.epam.feliks.kaminski.newsmanagement.exceptions.ServiceException;
import by.epam.feliks.kaminski.newsmanagement.service.IServiceNews;

@Transactional
public class ServiceNews implements IServiceNews{
    public static Logger logger = Logger.getLogger(App.class.getName());
	private IDAONews newsDAO;

	public ServiceNews(IDAONews newsDAO){
		this.newsDAO=newsDAO;
	}

	public long addNews(String title, String short_text, String full_text) throws ServiceException{
		try {
			return newsDAO.addNews(title, short_text, full_text);
		} catch (DAOException e){
			logger.error(e);
			throw(new ServiceException(e));
		}
	}

	public void editNews(News news) throws ServiceException{
		try {
			newsDAO.editNews(news);
		} catch (DAOException e){
			logger.error(e);
			throw(new ServiceException(e));
		}
	}

	@Transactional(readOnly=true)
	public News getNews(long newsId) throws ServiceException{
		try {
			return newsDAO.getNews(newsId);
		} catch (DAOException e){
			logger.error(e);
			throw(new ServiceException(e));
		}
	}

	public void deleteNews(long newsId) throws ServiceException{
		try {
			newsDAO.deleteNews(newsId);
		} catch (DAOException e){
			logger.error(e);
			throw(new ServiceException(e));
		}
	}

	@Transactional(readOnly=true)
	public List<News> getBestNewsList(long newsNFirst, long newsNLast) throws ServiceException{
		try {
			return newsDAO.getBestNewsList(newsNFirst, newsNLast);
		} catch (DAOException e){
			logger.error(e);
			throw(new ServiceException(e));
		}
	}

	@Transactional(readOnly=true)
	public long getBestNewsCount() throws ServiceException{
		try {
			return newsDAO.getBestNewsCount();
		} catch (DAOException e){
			logger.error(e);
			throw(new ServiceException(e));
		}
	}
	

	@Transactional(readOnly=true)
	public List<News> getNewsListByCriteria(SeachCriteria SC,long newsNFirst,
			long newsNLast) throws ServiceException{
		try {
			return newsDAO.getNewsListByCriteria(SC,newsNFirst,newsNLast);
		} catch (DAOException e){
			logger.error(e);
			throw(new ServiceException(e));
		}
	}

	@Transactional(readOnly=true)
	public long getNewsByCriteriaCount(SeachCriteria SC) throws ServiceException {
		try {
			return newsDAO.getNewsByCriteriaCount(SC);
		} catch (DAOException e){
			logger.error(e);
			throw(new ServiceException(e));
		}
	}
	//NewsAuthor
	
	public void addNewsAuthor(long newsId, long authorId) throws ServiceException{
		try {
			newsDAO.addNewsAuthor(newsId, authorId);
		} catch (DAOException e){
			logger.error(e);
			throw(new ServiceException(e));
		}
	}

	@Transactional(readOnly=true)
	public long getAuthorIdByNewsId(long newsId) throws ServiceException{
		try {
			return newsDAO.getAuthorIdByNewsId(newsId);
		} catch (DAOException e){
			logger.error(e);
			throw(new ServiceException(e));
		}
	}

	public void deleteNewsAuthor(long newsId, long authorId) throws ServiceException{
		try {
			newsDAO.deleteNewsAuthor(newsId, authorId);
		} catch (DAOException e){
			logger.error(e);
			throw(new ServiceException(e));
		}
	}

	@Transactional(readOnly=true)
	public List<Long> getNewsIdListByAuthorId( long authorId) throws ServiceException{
		try {
			return newsDAO.getNewsIdListByAuthorId(authorId);
		} catch (DAOException e){
			logger.error(e);
			throw(new ServiceException(e));
		}
	}
	
	//News_Tag
	
	public void addNewsTag(long newsId, long tagId) throws ServiceException{
		try {
			newsDAO.addNewsTag(newsId, tagId);
		} catch (DAOException e){
			logger.error(e);
			throw(new ServiceException(e));
		}
	}


	@Transactional(readOnly=true)
	public boolean getNewsTagExistance(long newsId,long tagId) throws ServiceException{
		try {
			return newsDAO.getNewsTagExistance(newsId, tagId);
		} catch (DAOException e){
			logger.error(e);
			throw(new ServiceException(e));
		}
	}

	@Transactional(readOnly=true)
	public List<Long> getNewsIdListByTagId(long tagId) throws ServiceException{
		try {
			return newsDAO.getNewsIdListByTagId(tagId);
		} catch (DAOException e){
			logger.error(e);
			throw(new ServiceException(e));
		}
	}

	public void deleteNewsTag(long newsId, long tagId) throws ServiceException{
		try {
			newsDAO.deleteNewsTag(newsId, tagId);
		} catch (DAOException e){
			logger.error(e);
			throw(new ServiceException(e));
		}
	}

	@Transactional(readOnly=true)
	public List<Long> getTagIdListByNewsId(long newsId) throws ServiceException{
		try {
			return newsDAO.getTagIdListByNewsId(newsId);
		} catch (DAOException e){
			logger.error(e);
			throw(new ServiceException(e));
		}
	}
	
}
