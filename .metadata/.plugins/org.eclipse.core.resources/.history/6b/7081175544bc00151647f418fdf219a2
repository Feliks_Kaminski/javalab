package by.epam.feliks.kaminski.newsmanagement.service.impl;

import org.apache.log4j.Logger;

import by.epam.feliks.kaminski.newsmanagement.App;
import by.epam.feliks.kaminski.newsmanagement.dao.IDAOUser;
import by.epam.feliks.kaminski.newsmanagement.entity.User;
import by.epam.feliks.kaminski.newsmanagement.exceptions.DAOException;
import by.epam.feliks.kaminski.newsmanagement.exceptions.ServiceException;
import by.epam.feliks.kaminski.newsmanagement.service.IServiceUser;

public class ServiceUser implements IServiceUser {
    public static Logger logger = Logger.getLogger(App.class.getName());
	private IDAOUser userDAO;
	
	public ServiceUser(IDAOUser userDAO){
		this.userDAO=userDAO;
	}

	public long addUser(String userName, String login, String password)
			throws ServiceException {
		try {
			return userDAO.addUser(userName, login, password);
		} catch (DAOException e){
			logger.error(e);
			throw(new ServiceException(e));
		}
	}

	public void editUser(User user) throws ServiceException {
		try {
			userDAO.editUser(user);
		} catch (DAOException e){
			logger.error(e);
			throw(new ServiceException(e));
		}
	}

	public User getUser(String login, String password) throws ServiceException {
		try {
			return userDAO.getUser(login, password);
		} catch (DAOException e){
			logger.error(e);
			throw(new ServiceException(e));
		}
	}

	public User getUserById(long userId) throws ServiceException {
		try {
			return userDAO.getUserById(userId);
		} catch (DAOException e){
			logger.error(e);
			throw(new ServiceException(e));
		}
	}

	public void deleteUser(long userId) throws ServiceException {
		try {
			userDAO.deleteUser(userId);
		} catch (DAOException e){
			logger.error(e);
			throw(new ServiceException(e));
		}
	}

	public String getUserRole(long userId) throws ServiceException {
		try {
			return userDAO.getUserRole(userId);
		} catch (DAOException e){
			logger.error(e);
			throw(new ServiceException(e));
		}
	}

	public void addUserRole(long userId, String role) throws ServiceException {
		try {
			userDAO.addUserRole(userId, role);
		} catch (DAOException e){
			logger.error(e);
			throw(new ServiceException(e));
		}
	}

	public void updateUserRole(long userId, String role) throws ServiceException {
		try {
			userDAO.updateUserRole(userId, role);
		} catch (DAOException e){
			logger.error(e);
			throw(new ServiceException(e));
		}
	}

	public void deleteUserRole(long userId) throws ServiceException {
		try {
			userDAO.deleteUserRole(userId);
		} catch (DAOException e){
			logger.error(e);
			throw(new ServiceException(e));
		}
	}

}
