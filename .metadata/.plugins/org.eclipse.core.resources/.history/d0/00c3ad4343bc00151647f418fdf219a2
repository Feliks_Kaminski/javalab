package by.epam.feliks.kaminski.newsmanagement.dao.impl;



import java.sql.Timestamp;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;

import by.epam.feliks.kaminski.newsmanagement.dao.IDAOAuthor;
import by.epam.feliks.kaminski.newsmanagement.entity.Author;
import by.epam.feliks.kaminski.newsmanagement.exceptions.DAOException;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations= {"classpath:test_spring.xml"})
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class })
@DatabaseSetup("classpath:AuthorTestDS.xml")
@DatabaseTearDown(value = "classpath:AuthorTestDS.xml",type = DatabaseOperation.DELETE_ALL)
public class DAOAuthorTest {

	@Autowired
	IDAOAuthor authorDAO;
	

    @Test
	public void testAddAuthor() throws DAOException {
			String authorName = "NewTestAuthor";
			long aId = authorDAO.addAuthor(authorName);
			Author a1 = authorDAO.getAuthor(aId);
			
			assertEquals(a1.getName(), authorName);
	}
    
    @Test
	public void testEditAuthor() throws DAOException {
			Author a1 = authorDAO.getAuthor(11);
			a1.setName("TestAuthorEdited");
			a1.setExpired(new Timestamp(1234567890L));
			authorDAO.editAuthor(a1);
			Author a2 = authorDAO.getAuthor(11);
			assertEquals(a1, a2);

	}
    
    @Test
	public void testGetAuthor() throws DAOException {
			Author a1 = authorDAO.getAuthor(11);
			Author a2 = authorDAO.getAuthor("TestAuthor11");
			assertNotNull(a1);
			assertEquals(a1, a2);
			authorDAO.deleteAuthor(11);
			a1 = authorDAO.getAuthor(11);
			assertNull(a1);
	}

    @Test
	public void testDeleteAuthor() throws DAOException {
			Author a = authorDAO.getAuthor(11);
			assertNotNull(a);
			authorDAO.deleteAuthor(11);
			a = authorDAO.getAuthor(11);
			assertNull(a);
	}

    @Test
	public void testGetAuthorList() throws DAOException {
			List<Author> l = authorDAO.getAuthorList(1, 0);
			assertNotNull(l);
			assertFalse(l.isEmpty());
			authorDAO.deleteAuthor(11);
			authorDAO.deleteAuthor(14);
			authorDAO.deleteAuthor(16);
			l = authorDAO.getAuthorList(1, 2);
			assertTrue(l.isEmpty());
	}
    
    @Test
    @Transactional(readOnly=true)
	public void testAddAuthorTR() throws DAOException {
			String authorName = "NewTestAuthor";
			long aId = authorDAO.addAuthor(authorName);
			Author a1 = authorDAO.getAuthor(aId);
			
			assertEquals(a1.getName(), authorName);
	}
	
}
