package by.epam.feliks.kaminski.newsmanagement.service.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;

import by.epam.feliks.kaminski.newsmanagement.App;
import by.epam.feliks.kaminski.newsmanagement.dao.IDAOAuthor;
import by.epam.feliks.kaminski.newsmanagement.dao.IDAOComment;
import by.epam.feliks.kaminski.newsmanagement.dao.IDAONews;
import by.epam.feliks.kaminski.newsmanagement.dao.IDAOTag;
import by.epam.feliks.kaminski.newsmanagement.dao.IDAOUser;
import by.epam.feliks.kaminski.newsmanagement.entity.Comment;
import by.epam.feliks.kaminski.newsmanagement.exceptions.DAOException;
import by.epam.feliks.kaminski.newsmanagement.exceptions.ServiceException;
import by.epam.feliks.kaminski.newsmanagement.service.IServiceExtended;

@Transactional
public class ServiceExtended implements IServiceExtended {
    public static Logger logger = Logger.getLogger(App.class.getName());
	private IDAOAuthor authorDAO;
	private IDAOComment commentDAO;
	private IDAONews newsDAO;
	private IDAOTag tagDAO;
	private IDAOUser userDAO;
	
	public void setAuthorDAO(IDAOAuthor authorDAO) {
		this.authorDAO = authorDAO;
	}
	public void setCommentDAO(IDAOComment commentDAO) {
		this.commentDAO = commentDAO;
	}
	public void setNewsDAO(IDAONews newsDAO) {
		this.newsDAO = newsDAO;
	}
	public void setTagDAO(IDAOTag tagDAO) {
		this.tagDAO = tagDAO;
	}
	
	public void setUserDAO(IDAOUser userDAO) {
		this.userDAO = userDAO;
	}
	
	
	
	
	public long addNewsWithDependencies(long authorId,String title, String short_text, 
			String full_text, List<Long> tags) throws ServiceException {
		long idN;
		try {
			idN=newsDAO.addNews(title, short_text, full_text);
			newsDAO.addNewsAuthor(idN, authorId);
			for (long tagId:tags){
				newsDAO.addNewsTag(idN, tagId);
			}
		} catch (DAOException e){
			logger.error(e);
			throw(new ServiceException(e));
		}
		return idN;
	}
	
	public void deleteNewsWithDependencies(long newsId) throws ServiceException{
		try {
			long authorId = newsDAO.getAuthorIdByNewsId(newsId);
			newsDAO.deleteNewsAuthor(newsId, authorId);
			
			List <Long> tagsIds= newsDAO.getTagIdListByNewsId(newsId);
			for (long tagId:tagsIds){
				newsDAO.deleteNewsTag(newsId, tagId);
			}
			
			this.deleteCommentsByNewsId(newsId);
			
			newsDAO.deleteNews(newsId);
		} catch (DAOException e){
			logger.error(e);
			throw(new ServiceException(e));
		}
	}
	
	public void deleteAuthorWithDependencies(long authorId) throws ServiceException{
		try {
			List <Long> newsIds = newsDAO.getNewsIdListByAuthorId(authorId);
			for (long newsId:newsIds){
				this.deleteNewsWithDependencies(newsId);
			}
			
			authorDAO.deleteAuthor(authorId);
		} catch (DAOException e){
			logger.error(e);
			throw(new ServiceException(e));
		}
	}

	public void deleteTagWithDependencies(long tagId) throws ServiceException {
		try {
			
			List <Long> newsIds= newsDAO.getNewsIdListByTagId(tagId);
			for (long newsId:newsIds){
				newsDAO.deleteNewsTag(newsId, tagId);
			}
			
			tagDAO.deleteTag(tagId);
		} catch (DAOException e){
			logger.error(e);
			throw(new ServiceException(e));
		}
	}
	
	
	public void deleteUserWithDependencies(long userId) throws ServiceException {
		try {
			userDAO.deleteUserRole(userId);
			userDAO.deleteUser(userId);
		} catch (DAOException e){
			logger.error(e);
			throw(new ServiceException(e));
		}
	}
	
}
