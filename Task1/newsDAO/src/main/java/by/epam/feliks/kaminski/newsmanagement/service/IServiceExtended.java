package by.epam.feliks.kaminski.newsmanagement.service;

import java.util.List;

import by.epam.feliks.kaminski.newsmanagement.exceptions.ServiceException;

/**
 * This Service is used for providing complex multiDAO commands.
 * 
 * @author Feliks_Kaminski
 *
 */
public interface IServiceExtended {
	
	/**
	 * This method creates News entity with Author and Tag dependencies in one step and returns it's id.
	 * 
	 * @param authorId
	 * @param title
	 * @param short_text
	 * @param full_text
	 * @param tags
	 * @return News id
	 * @throws ServiceException
	 */
	public long addNewsWithDependencies(long authorId,String title, String short_text, 
			String full_text,List<Long> tags) throws ServiceException;
	
	/**
	 * The method deletes a News entity with all dependent Comment entities and Tag and Author dependencies.
	 * 
	 * @param newsId
	 * @throws ServiceException
	 */
	public void deleteNewsWithDependencies(long newsId) throws ServiceException;
	


	/**
	 * This method deletes Tag entity and NewsTag dependencies.
	 * 
	 * @param tagId
	 * @throws ServiceException
	 */
	public void deleteTagWithDependencies(long tagId) throws ServiceException;
	
	
	
	/**
	 * This method deletes User entity and his role.
	 * 
	 * @param userId
	 * @throws ServiceException
	 */
	public void deleteUserWithDependencies(long userId) throws ServiceException;
}
