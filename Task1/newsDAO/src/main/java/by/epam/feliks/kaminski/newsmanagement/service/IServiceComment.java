package by.epam.feliks.kaminski.newsmanagement.service;

import java.util.List;

import by.epam.feliks.kaminski.newsmanagement.entity.Comment;
import by.epam.feliks.kaminski.newsmanagement.exceptions.ServiceException;

/**
 * This is an interface for operations on Comment entity.
 * 
 * @author Feliks_Kaminski
 */
public interface IServiceComment {
	/**
	 * The method inserts Comment entity and returns it's id.
	 * 
	 * @param newsId
	 * @param comment_text
	 * @return id of the inserted comment
	 * @throws ServiceException
	 */
	public long addComment(long newsId, String comment_text) throws ServiceException;
	
	/**
	 * The method gets a Comment entity to update it's fields using it's id.
	 * 
	 * @param Comment entity
	 * @throws ServiceException
	 */
	public void editComment(Comment comment) throws ServiceException;
	
	/**
	 * The method returns a Comment entity using it's id.
	 * 
	 * @param commentId
	 * @return Comment entity
	 * @throws ServiceException
	 */
	public Comment getComment(long commentId) throws ServiceException;
	
	/**
	 * The method removes a Comment entity using it's id.
	 * 
	 * @param commentId
	 * @throws ServiceException
	 */
	public void deleteComment(long commentId) throws ServiceException;

	/**
	 * The method removes a Comment entities using News id.
	 * 
	 * @param newsId
	 * @throws ServiceException
	 */
	public void deleteCommentsByNewsId(long newsId) throws ServiceException;
	
	
	/**
	 * The method returns a list of Comment entities.
	 * Requires 3 numbers.
	 * The first number is a News entity's id.
	 * The second number is a position of the 1st Comment in the list ordered by creation date desc.
	 * The third number is a position of the last Comment in list. 0 means that it will be the last Comment in list.
	 * getCommentsIdsListByNewsId(x,1, 0) returns list of all Comments of the News with id = x.
	 * 
	 * @param newsId
	 * @param commentNFirst
	 * @param commentNLast
	 * @return list with comment entities
	 * @throws ServiceException
	 */
	public List<Comment> getCommentsIdsListByNewsId(long newsId,
			long commentNFirst, long commentNLast) throws ServiceException;
}
