package by.epam.feliks.kaminski.newsmanagement.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;



import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import by.epam.feliks.kaminski.newsmanagement.dao.IDAOComment;
import by.epam.feliks.kaminski.newsmanagement.entity.Comment;
import by.epam.feliks.kaminski.newsmanagement.exceptions.DAOException;


@Transactional(propagation = Propagation.MANDATORY)
public class DAOComment implements IDAOComment{
	private static final String ADD_COMMENT = "insert into COMMENTS (COMMENT_ID,NEWS_ID,COMMENT_TEXT) values (seq_comments_id.nextval,?,?)";
	private static final String EDIT_COMMENT = "update COMMENTS set COMMENT_TEXT=? where COMMENT_ID=?";
	private static final String GET_COMMENT = "select NEWS_ID,COMMENT_TEXT,CREATION_DATE from COMMENTS where COMMENT_ID=?";
	private static final String DELETE_COMMENT = "delete from COMMENTS where COMMENT_ID=?";
	private static final String DELETE_COMMENTS_BY_NEWS_ID = "delete from COMMENTS where NEWS_ID=?";
	private static final String GET_COMMENT_LIST_BY_NEWS_ID =   "select COMMENT_ID,COMMENT_TEXT,CREATION_DATE "
															   + "from "
															   + "( select COMMENT_ID,COMMENT_TEXT,CREATION_DATE,ROW_NUMBER() OVER (ORDER BY CREATION_DATE) n "
															   + "  from COMMENTS "
															   + "  where NEWS_ID=? "
															   + ") sml "
															   + "where sml.n between ? and decode (?,0,sml.n,?) "
															   + "order by CREATION_DATE desc";

	private ADAOKey key;
	
	public DAOComment(ADAOKey key){
		this.key=key;
	}
	
	public long addComment(long newsId, String comment_text) throws DAOException {
		Connection con = key.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		long id = 0;
		try {
			ps = con.prepareStatement(ADD_COMMENT, new String[] { "COMMENT_ID",
					"CREATION_DATE" });
			ps.setLong(1, newsId);
			ps.setString(2, comment_text);
			ps.executeUpdate();
			rs = ps.getGeneratedKeys();
			if (rs.next()) {
				id=rs.getLong(1);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			key.releaseDAOKey(con, ps, rs);
		}
		return id;
	}

	public void editComment(Comment comment) throws DAOException {
		Connection con = key.getConnection();
		PreparedStatement ps = null;
		try {
			ps = con.prepareStatement(EDIT_COMMENT);
			ps.setLong(2, comment.getId());
			ps.setString(1, comment.getCommentText());
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			key.releaseDAOKey(con, ps, null);
		}
	}

	public Comment getComment(long commentId) throws DAOException {
		Connection con = key.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		Comment comment = null;
		try {
			ps = con.prepareStatement(GET_COMMENT);
			ps.setLong(1, commentId);
			rs = ps.executeQuery();
			if (rs.next()) {
				comment = new Comment(commentId, rs.getLong("NEWS_ID"),
						rs.getString("COMMENT_TEXT"),
						rs.getTimestamp("CREATION_DATE"));
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			key.releaseDAOKey(con, ps, rs);
		}
		return comment;
	}

	public void deleteComment(long commentId) throws DAOException {
		Connection con = key.getConnection();
		PreparedStatement ps = null;
		try {
			ps = con.prepareStatement(DELETE_COMMENT);
			ps.setLong(1, commentId);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			key.releaseDAOKey(con, ps, null);
		}
	}

	public void deleteCommentsByNewsId(long newsId) throws DAOException {
		Connection con = key.getConnection();
		PreparedStatement ps = null;
		try {
			ps = con.prepareStatement(DELETE_COMMENTS_BY_NEWS_ID);
			ps.setLong(1, newsId);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			key.releaseDAOKey(con, ps, null);
		}
	}
	
	public List<Comment> getCommentsIdsListByNewsId(long newsId,
			long commentNFirst, long commentNLast) throws DAOException {
		Connection con = key.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<Comment> commentsList = new ArrayList<Comment>();
		try {
			ps = con.prepareStatement(GET_COMMENT_LIST_BY_NEWS_ID);
			ps.setLong(1, newsId);
			ps.setLong(2, commentNFirst);
			ps.setLong(3, commentNLast);
			ps.setLong(4, commentNLast);
			rs = ps.executeQuery();
			while (rs.next()) {
				commentsList.add(new Comment(rs.getLong("COMMENT_ID"), newsId, 
						rs.getString("COMMENT_TEXT"),rs.getTimestamp("CREATION_DATE")) );
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			key.releaseDAOKey(con, ps, rs);
		}
		return commentsList;
	}

}
