package by.epam.feliks.kaminski.newsmanagement.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import by.epam.feliks.kaminski.newsmanagement.dao.IDAOAuthor;
import by.epam.feliks.kaminski.newsmanagement.entity.Author;
import by.epam.feliks.kaminski.newsmanagement.exceptions.DAOException;


@Transactional(propagation = Propagation.MANDATORY)
public class DAOAuthor implements IDAOAuthor {
	private static final String ADD_AUTHOR = "insert into AUTHOR (AUTHOR_ID, AUTHOR_NAME) values (seq_author_id.nextval,?)";
	private static final String EDIT_AUTHOR = "update AUTHOR set AUTHOR_NAME=?, EXPIRED=? where AUTHOR_ID=?";
	private static final String GET_AUTHOR_BY_ID = "select AUTHOR_ID, AUTHOR_NAME, EXPIRED from AUTHOR where AUTHOR_ID=?";
	private static final String GET_AUTHOR_BY_NAME = "select AUTHOR_ID, AUTHOR_NAME, EXPIRED from AUTHOR where AUTHOR_NAME=?";
	private static final String DELETE_AUTHOR = "delete from AUTHOR where AUTHOR_ID=?";
	private static final String GET_AUTHOR_LIST =   "select AUTHOR_ID, AUTHOR_NAME "+
													"from AUTHOR "+
													"where EXPIRED is null "+
													"order by AUTHOR_NAME ";

	private ADAOKey key;
	
	public DAOAuthor(ADAOKey key){
		this.key=key;
	}
	
	public long addAuthor(String name) throws DAOException {
		Connection con = key.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		long id=0;
		try {
			ps = con.prepareStatement(ADD_AUTHOR, new String[] { "AUTHOR_ID" });
			ps.setString(1, name);
			ps.executeUpdate();
			rs = ps.getGeneratedKeys();
			if (rs.next()) {
				id=rs.getLong(1);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			key.releaseDAOKey(con, ps, rs);
		}
		return id;
	}

	public void editAuthor(Author author) throws DAOException {
		Connection con = key.getConnection();
		PreparedStatement ps = null;
		try {
			ps = con.prepareStatement(EDIT_AUTHOR);
			ps.setLong(3, author.getId());
			ps.setString(1, author.getName());
			ps.setTimestamp(2, author.getExpired());
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			key.releaseDAOKey(con, ps, null);
		}
	}

	public Author getAuthor(long authorId) throws DAOException {
		Connection con = key.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		Author author = null;
		try {
			ps = con.prepareStatement(GET_AUTHOR_BY_ID);
			ps.setLong(1, authorId);
			rs = ps.executeQuery();
			if (rs.next()) {
				author = new Author(rs.getLong("AUTHOR_ID"),
						rs.getString("AUTHOR_NAME"));
				author.setExpired(rs.getTimestamp("EXPIRED"));
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			key.releaseDAOKey(con, ps, rs);
		}
		return author;
	}

	public Author getAuthor(String name) throws DAOException {
		Connection con = key.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		Author author = null;
		try {
			ps = con.prepareStatement(GET_AUTHOR_BY_NAME);
			ps.setString(1, name);
			rs = ps.executeQuery();
			if (rs.next()) {
				author = new Author(rs.getLong("AUTHOR_ID"),
						rs.getString("AUTHOR_NAME"));
				author.setExpired(rs.getTimestamp("EXPIRED"));
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			key.releaseDAOKey(con, ps, rs);
		}
		return author;
	}

	public void deleteAuthor(long authorId) throws DAOException {
		Connection con = key.getConnection();
		PreparedStatement ps = null;
		try {
			ps = con.prepareStatement(DELETE_AUTHOR);
			ps.setLong(1, authorId);
			ps.executeUpdate();
			ps.close();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			key.releaseDAOKey(con, ps, null);
		}
	}

	public List<Author> getAuthorList() throws DAOException {
		Connection con = key.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		Author author = null;
		List<Author> authorList = new ArrayList<Author>();
		try {
			ps = con.prepareStatement(GET_AUTHOR_LIST);
			rs = ps.executeQuery();
			while (rs.next()) {
				author = new Author(rs.getLong("AUTHOR_ID"),
						rs.getString("AUTHOR_NAME"));
				// author.setExpired(rs.getTimestamp("EXPIRED"));
				authorList.add(author);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			key.releaseDAOKey(con, ps, rs);
		}
		return authorList;
	}
}
