package by.epam.feliks.kaminski.newsmanagement.entity;

import java.util.ArrayList;
import java.util.List;

public class SeachCriteria {
	private Author author;
	private List<Tag> tagList = new ArrayList<Tag>();
	
	public SeachCriteria(Author author,List<Tag> tagList){
		this.author=author;
		this.tagList=tagList;
	}
	
	
	
	public Author getAuhor(){
		return author;
	}
	
	public void setAuhor(Author author){
		this.author=author;
	}
	

	
	public void addTag(Tag tag){
		tagList.add(tag);
	}

	public void setTagList(List<Tag> tagList){
		this.tagList=tagList;
	}
	
	public List<Tag> getTagList(){
		return tagList;
	}
}
