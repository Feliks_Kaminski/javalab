package by.epam.feliks.kaminski.newsmanagement.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import by.epam.feliks.kaminski.newsmanagement.exceptions.DAOException;



/**
 * This package access class is used for current realization to provide other classes 
 * with connection and proper closing of it and it's dependent objects.
 * 
 * @author Feliks_Kaminski
 *
 */
abstract class ADAOKey {
	
	/**
	 * This method closes Connection, Statement and ResultSet.
	 * If you don't create ResultSett you have to send null.
	 * 
	 * @param con
	 * @param ps
	 * @param rs or null
	 * @throws DAOException
	 */
	abstract void releaseDAOKey(Connection con,PreparedStatement ps,ResultSet rs) throws DAOException;

	/**
	 * This method returns an Oracle Connection
	 * 
	 * @return Connection
	 */
	abstract Connection getConnection();
}
