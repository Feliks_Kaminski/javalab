package by.epam.feliks.kaminski.newsmanagement.service.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;

import by.epam.feliks.kaminski.newsmanagement.App;
import by.epam.feliks.kaminski.newsmanagement.dao.IDAOTag;
import by.epam.feliks.kaminski.newsmanagement.entity.Tag;
import by.epam.feliks.kaminski.newsmanagement.exceptions.DAOException;
import by.epam.feliks.kaminski.newsmanagement.exceptions.ServiceException;
import by.epam.feliks.kaminski.newsmanagement.service.IServiceTag;


@Transactional
public class ServiceTag implements IServiceTag{
    public static Logger logger = Logger.getLogger(App.class.getName());
	private IDAOTag tagDAO;
	
	public ServiceTag(IDAOTag tagDAO){
		this.tagDAO=tagDAO;
	}
	
	public long addTag(String name) throws ServiceException{
		try {
			return tagDAO.addTag(name);
		} catch (DAOException e){
			logger.error(e);
			throw(new ServiceException(e));
		}
	}
	
	public void editTag(Tag tag) throws ServiceException{
		try {
			tagDAO.editTag(tag);;
		} catch (DAOException e){
			logger.error(e);
			throw(new ServiceException(e));
		}
	}

	@Transactional(readOnly=true)
	public Tag getTag(String name) throws ServiceException{
		try {
			return tagDAO.getTag(name);
		} catch (DAOException e){
			logger.error(e);
			throw(new ServiceException(e));
		}
	}

	@Transactional(readOnly=true)
	public Tag getTag(long tagId) throws ServiceException{
		try {
			return tagDAO.getTag(tagId);
		} catch (DAOException e){
			logger.error(e);
			throw(new ServiceException(e));
		}
	}

	public void deleteTag(long tagId) throws ServiceException{
		try {
			tagDAO.deleteTag(tagId);;
		} catch (DAOException e){
			logger.error(e);
			throw(new ServiceException(e));
		}
	}

	@Transactional(readOnly=true)
	public List<Tag> getTagList() throws ServiceException{
		try {
			return tagDAO.getTagList();
		} catch (DAOException e){
			logger.error(e);
			throw(new ServiceException(e));
		}
	}


}
