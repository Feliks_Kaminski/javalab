package by.epam.feliks.kaminski.newsmanagement.service.impl;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import by.epam.feliks.kaminski.newsmanagement.dao.IDAOUser;
import by.epam.feliks.kaminski.newsmanagement.entity.User;
import by.epam.feliks.kaminski.newsmanagement.exceptions.DAOException;
import by.epam.feliks.kaminski.newsmanagement.exceptions.ServiceException;

@RunWith(MockitoJUnitRunner.class)
public class ServiceUserTest {

	@Mock
	private IDAOUser userDAO;
	
	@Mock
	private User user;
	
	@InjectMocks private ServiceUser userService;

	@Test
	public void addUserTest() throws DAOException,ServiceException {
		long userId = 5;
		String userName="I";
		String login="II";
		String password="my";
		when(userDAO.addUser(userName, login, password)).thenReturn(userId);
		
		assertEquals(userId, userService.addUser(userName, login, password));
		verify(userDAO).addUser(userName, login, password);
		verifyNoMoreInteractions(userDAO);
	}
	
	@Test
	public void editUserTest() throws DAOException,ServiceException {
		userService.editUser(user);
		verify(userDAO).editUser(user);
		verifyNoMoreInteractions(userDAO);
		verifyNoMoreInteractions(user);
	}
	
	@Test
	public void getUserTest() throws DAOException,ServiceException {

		String login="II";
		String password="my";
		when(userDAO.getUser(login, password)).thenReturn(user);
		
		assertEquals(user, userService.getUser(login, password));
		verify(userDAO).getUser(login, password);
		verifyNoMoreInteractions(userDAO);
		verifyNoMoreInteractions(user);
	}
	
	@Test
	public void getUserByIdTest() throws DAOException,ServiceException {
		long userId = 5;
		when(userDAO.getUserById(userId)).thenReturn(user);
		
		assertEquals(user, userService.getUserById(userId));
		verify(userDAO).getUserById(userId);
		verifyNoMoreInteractions(userDAO);
		verifyNoMoreInteractions(user);
	}
	
	@Test
	public void deleteUserTest() throws DAOException,ServiceException {
		long userId=3L;
		userService.deleteUser(userId);
		verify(userDAO).deleteUser(userId);
		verifyNoMoreInteractions(userDAO);
	}
	
	@Test
	public void getUserRoleTest() throws DAOException,ServiceException {
		long userId=3L;
		String role = "ADM";
		when(userDAO.getUserRole(userId)).thenReturn(role);
		assertEquals(role, userService.getUserRole(userId)); 
		verify(userDAO).getUserRole(userId);
		verifyNoMoreInteractions(userDAO);
	}
	
	@Test
	public void addUserRoleTest() throws DAOException,ServiceException {
		long userId=3L;
		String role = "ADM";
		userService.addUserRole(userId, role);
		verify(userDAO).addUserRole(userId, role);
		verifyNoMoreInteractions(userDAO);
	}
	
	@Test
	public void updateUserRoleTest() throws DAOException,ServiceException {
		long userId=3L;
		String role = "ADM";
		userService.updateUserRole(userId, role);
		verify(userDAO).updateUserRole(userId, role);
		verifyNoMoreInteractions(userDAO);
	}
	
	@Test
	public void deleteUserRoleTest() throws DAOException,ServiceException {
		long userId=3L;
		userService.deleteUserRole(userId);
		verify(userDAO).deleteUserRole(userId);
		verifyNoMoreInteractions(userDAO);
	}
	
}
