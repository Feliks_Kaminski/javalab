package by.epam.feliks.kaminski.newsmanagement.service.impl;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import by.epam.feliks.kaminski.newsmanagement.dao.IDAOTag;
import by.epam.feliks.kaminski.newsmanagement.entity.Tag;
import by.epam.feliks.kaminski.newsmanagement.exceptions.DAOException;
import by.epam.feliks.kaminski.newsmanagement.exceptions.ServiceException;

@RunWith(MockitoJUnitRunner.class)

public class ServiceTagTest {
	@Mock
	private IDAOTag tagDAO;
	
	@Mock
	private Tag tag;
	
	@InjectMocks private ServiceTag tagService;
	
	@Test
	public void addTagTest() throws DAOException,ServiceException {
		long tId= 123L;
		String name = "NewTag";
		
		when(tagDAO.addTag(name)).thenReturn(tId);
		
		assertEquals(tId,tagService.addTag(name));
		
		verify(tagDAO).addTag(name);
		verifyNoMoreInteractions(tagDAO);
	}
	
	@Test
	public void editTagTest() throws DAOException,ServiceException {
		
		doNothing().when(tagDAO).editTag(tag);
		
		tagService.editTag(tag);
		
		verify(tagDAO).editTag(tag);
		verifyNoMoreInteractions(tagDAO);
		verifyNoMoreInteractions(tag);
	}

	@Test
	public void getTagTest() throws DAOException,ServiceException {
		long tId= 123L;
		String name = "Tag";
		
		when(tagDAO.getTag(name)).thenReturn(tag);
		when(tagDAO.getTag(tId)).thenReturn(tag);
		
		assertEquals(tag,tagService.getTag(tId));
		assertEquals(tag,tagService.getTag(name));
		
		verify(tagDAO).getTag(tId);
		verify(tagDAO).getTag(name);
		verifyNoMoreInteractions(tagDAO);
		verifyNoMoreInteractions(tag);
	}
	
	@Test
	public void deleteTagTest() throws DAOException,ServiceException {
		long tId= 123L;
		
		doNothing().when(tagDAO).deleteTag(tId);
		
		tagService.deleteTag(tId);
		
		verify(tagDAO).deleteTag(tId);
		verifyNoMoreInteractions(tagDAO);
	}

	@Test
	public void getTagListTest() throws DAOException,ServiceException {
		List <Tag> tList = mock(List.class);

		when(tagDAO.getTagList()).thenReturn(tList);
		
		assertTrue(tagService.getTagList()==tList);
		
		verify(tagDAO).getTagList();
		verifyNoMoreInteractions(tagDAO);
		verifyNoMoreInteractions(tList);
	}
	
}
