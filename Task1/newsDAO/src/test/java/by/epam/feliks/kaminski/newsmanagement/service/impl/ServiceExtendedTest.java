package by.epam.feliks.kaminski.newsmanagement.service.impl;

import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import by.epam.feliks.kaminski.newsmanagement.dao.IDAOAuthor;
import by.epam.feliks.kaminski.newsmanagement.dao.IDAOComment;
import by.epam.feliks.kaminski.newsmanagement.dao.IDAONews;
import by.epam.feliks.kaminski.newsmanagement.dao.IDAOTag;
import by.epam.feliks.kaminski.newsmanagement.dao.IDAOUser;
import by.epam.feliks.kaminski.newsmanagement.exceptions.DAOException;
import by.epam.feliks.kaminski.newsmanagement.exceptions.ServiceException;

@RunWith(MockitoJUnitRunner.class)
public class ServiceExtendedTest {

	@Mock
	private IDAOAuthor authorDAO;
	@Mock
	private IDAOComment commentDAO;
	@Mock
	private IDAONews newsDAO;
	@Mock
	private IDAOTag tagDAO;
	@Mock
	private IDAOUser userDAO;
	
	
	@InjectMocks private ServiceExtended extendedService;
	
	@Test
	public void addNewsWithDependenciesTest() throws DAOException,ServiceException{
		
		long idN=123L;
		long idA=234L;
		String title = "T";
		String short_text = "LT";
		String full_text = "FT";
		
		List <Long> tagsIds = new ArrayList<Long>();
		
		tagsIds.add(456L);
		tagsIds.add(567L);
		tagsIds.add(987L);
		

		when(newsDAO.addNews(title, short_text, full_text)).thenReturn(idN);
		
		extendedService.addNewsWithDependencies(idA, title, short_text, full_text, tagsIds);
		
		
		
		verify(newsDAO).addNews(title, short_text, full_text);
		verify(newsDAO).addNewsAuthor(idN, idA);
		verify(newsDAO).addNewsTag(idN, tagsIds);

	}
	
	@Test
	public void deleteNewsWithDependenciesTest() throws DAOException,ServiceException{
		long newsId = 123L;		
		
		doNothing().when(newsDAO).deleteNewsAuthor(newsId);
		doNothing().when(newsDAO).deleteNewsTagByNewsId(anyLong());

		doNothing().when(commentDAO).deleteComment(newsId);

		extendedService.deleteNewsWithDependencies(newsId);
		
		verify(newsDAO).deleteNewsAuthor(newsId);
		verify(newsDAO).deleteNewsTagByNewsId(newsId);
		verify(newsDAO).deleteNews(newsId);
		verifyNoMoreInteractions(newsDAO);
	}
	


	@Test
	public void deleteTagWithDependenciesTest() throws DAOException,ServiceException{
		
		long tagId = 123L;
		
		doNothing().when(newsDAO).deleteNewsTagByTagId(anyLong());
		doNothing().when(tagDAO).deleteTag(tagId);
		

		extendedService.deleteTagWithDependencies(tagId);
		
		verify(newsDAO).deleteNewsTagByTagId(tagId);
		verify(tagDAO).deleteTag(tagId);
		verifyNoMoreInteractions(tagDAO);
		verifyNoMoreInteractions(newsDAO);
	}
	
	
	@Test
	public void deleteUserWithDependenciesTest() throws DAOException,ServiceException{
		long userId=123L;
		extendedService.deleteUserWithDependencies(userId);
		verify(userDAO).deleteUserRole(userId);
		verify(userDAO).deleteUser(userId);
		verifyNoMoreInteractions(userDAO);
	}
}
