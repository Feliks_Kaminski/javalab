package by.epam.feliks.kaminski.newsmanagement.dao.impl;


import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import by.epam.feliks.kaminski.newsmanagement.dao.IDAOUser;
import by.epam.feliks.kaminski.newsmanagement.entity.User;
import by.epam.feliks.kaminski.newsmanagement.exceptions.DAOException;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations= {"classpath:test_spring.xml"})
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class })
@DatabaseSetup("classpath:UserTestDS.xml")
@DatabaseTearDown(value = "classpath:UserTestDS.xml",type = DatabaseOperation.DELETE_ALL)
public class DAOUserTest {
	@Autowired
	IDAOUser userDAO;
	
	@Test
	public void addUserTest() throws DAOException {
		String userName="I";
		String login="II";
		String password="my";
		long userId = userDAO.addUser(userName, login, password);
		User user = userDAO.getUserById(userId);
		assertEquals(userName, user.getName());
		assertEquals(login, user.getLogin());
		assertEquals(password, user.getPassword());
	}

	@Test
	public void editUserTest() throws DAOException {
		long userId=1L;
		String userName="I";
		String login="II";
		String password="my";
		User user = userDAO.getUserById(userId);
		assertNotEquals(userName, user.getName());
		assertNotEquals(login, user.getLogin());
		assertNotEquals(password, user.getPassword());
		user.setName(userName);
		user.setLogin(login);
		user.setPassword(password);
		userDAO.editUser(user);
		user = userDAO.getUserById(userId);
		assertEquals(userName, user.getName());
		assertEquals(login, user.getLogin());
		assertEquals(password, user.getPassword());
		
	}

	@Test
	public void getUserTest() throws DAOException {
		long userId=3L;
		User user = userDAO.getUserById(userId);
		User user1 = userDAO.getUser(user.getLogin(), user.getPassword());
		assertEquals(user,user1);
	}

	@Test
	public void getUserByIdTest() throws DAOException {
		long userId=3L;
		User user = userDAO.getUserById(userId);
		userDAO.deleteUser(userId);
		assertNotNull(user);
		user = userDAO.getUserById(userId);
		assertNull(user);
	}

	@Test
	public void deleteUserTest() throws DAOException {
		long userId=3L;
		User user = userDAO.getUserById(userId);
		userDAO.deleteUser(userId);
		assertNotNull(user);
		user = userDAO.getUserById(userId);
		assertNull(user);
	}

	@Test
	public void getUserRoleTest() throws DAOException {
		long userId=1;
		String role = userDAO.getUserRole(userId);
		assertEquals("ADM", role);
		userDAO.deleteUserRole(userId);
		role=userDAO.getUserRole(userId);
		assertNull(role);
	}

	@Test
	public void addUserRoleTest() throws DAOException {
		String role="ADM";
		long userId=3;
		userDAO.addUserRole(userId, role);
		String roleGetted = userDAO.getUserRole(userId);
		assertEquals(role, roleGetted);
	}

	@Test
	public void updateUserRoleTest() throws DAOException {
		long userId=1;
		String role = userDAO.getUserRole(userId);
		assertEquals("ADM", role);
		userDAO.updateUserRole(userId, "USR");
		role=userDAO.getUserRole(userId);
		assertEquals(role, "USR");
	}

	@Test
	public void deleteUserRoleTest() throws DAOException {
		long userId=1;
		String role = userDAO.getUserRole(userId);
		assertEquals("ADM", role);
		userDAO.deleteUserRole(userId);
		role=userDAO.getUserRole(userId);
		assertNull(role);
	}
	
	
}
