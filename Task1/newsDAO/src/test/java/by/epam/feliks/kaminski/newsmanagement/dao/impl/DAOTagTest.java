package by.epam.feliks.kaminski.newsmanagement.dao.impl;


import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;

import by.epam.feliks.kaminski.newsmanagement.dao.IDAOTag;
import by.epam.feliks.kaminski.newsmanagement.entity.Tag;
import by.epam.feliks.kaminski.newsmanagement.exceptions.DAOException;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations= {"classpath:test_spring.xml"})
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class })
@DatabaseSetup("classpath:TagTestDS.xml")
@DatabaseTearDown(value = "classpath:TagTestDS.xml",type = DatabaseOperation.DELETE_ALL)

public class DAOTagTest {

	@Autowired
	IDAOTag tagDAO;

    
    @Test
	public void addTagTest() throws DAOException {
			String tagName = "NewTestTAG";
			long tId = tagDAO.addTag(tagName);
			Tag t1 = tagDAO.getTag(tId);
			
			assertEquals(t1.getName(), tagName);
	}
    
    @Test
	public void editTagTest() throws DAOException {
			Tag t1 = tagDAO.getTag(3);
			t1.setName("TestTagEdited");
			tagDAO.editTag(t1);
			Tag t2 = tagDAO.getTag(3);
			assertEquals(t1, t2);
	}
    
    @Test
	public void getTagTest() throws DAOException {
			Tag t1 = tagDAO.getTag(3);
			Tag t2 = tagDAO.getTag("TestTag3");
			assertNotNull(t1);
			assertEquals(t1, t2);
			tagDAO.deleteTag(3);
			t1 = tagDAO.getTag(11);
			assertNull(t1);
	}
    
    @Test
	public void deleteTagTest() throws DAOException {
			Tag t = tagDAO.getTag(3);
			assertNotNull(t);
			tagDAO.deleteTag(3);
			t = tagDAO.getTag(3);
			assertNull(t);
	}
    
    @Test
	public void getTagListTest() throws DAOException {
			List<Tag> l = tagDAO.getTagList();
			assertNotNull(l);
			assertEquals(6,l.size());
			tagDAO.deleteTag(3);
			tagDAO.deleteTag(5);
			l = tagDAO.getTagList();
			assertEquals(4, l.size());
	}
    
}
