package by.epam.feliks.kaminski.newsmanagement.service.impl;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import by.epam.feliks.kaminski.newsmanagement.dao.IDAOComment;
import by.epam.feliks.kaminski.newsmanagement.entity.Comment;
import by.epam.feliks.kaminski.newsmanagement.exceptions.DAOException;
import by.epam.feliks.kaminski.newsmanagement.exceptions.ServiceException;

@RunWith(MockitoJUnitRunner.class)
public class ServiceCommentTest {

	@Mock
	private IDAOComment commentDAO;
	
	@Mock
	private Comment comment;
	
	@InjectMocks private ServiceComment commentService;
	
	@Test
	public void addCommentTest() throws DAOException,ServiceException {
		long cId= 123L;
		long nId= 32L;
		String name = "NewComment";
		
		when(commentDAO.addComment(nId,name)).thenReturn(cId);
		
		assertEquals(cId,commentService.addComment(nId,name));
		
		verify(commentDAO).addComment(nId,name);
		verifyNoMoreInteractions(commentDAO);
	}
	
	@Test
	public void editCommentTest() throws DAOException,ServiceException {
		doNothing().when(commentDAO).editComment(comment);
		
		commentDAO.editComment(comment);
		
		verify(commentDAO).editComment(comment);
		verifyNoMoreInteractions(commentDAO);
		verifyNoMoreInteractions(comment);
	}
	
	@Test
	public void getCommentTest() throws DAOException,ServiceException {
		long cId= 123L;
		
		when(commentDAO.getComment(cId)).thenReturn(comment);
		
		assertEquals(comment,commentService.getComment(cId));
		
		verify(commentDAO).getComment(cId);
		verifyNoMoreInteractions(commentDAO);
		verifyNoMoreInteractions(comment);
	}
	
	@Test
	public void deleteCommentTest() throws DAOException,ServiceException {
		long cId= 123L;
		
		doNothing().when(commentDAO).deleteComment(cId);
		
		commentDAO.deleteComment(cId);
		
		verify(commentDAO).deleteComment(cId);
		verifyNoMoreInteractions(commentDAO);
		verifyNoMoreInteractions(comment);
	}
	
	@Test
	public void deleteCommentsByNewsIdTest() throws DAOException,ServiceException {
		long nId= 123L;
		
		doNothing().when(commentDAO).deleteCommentsByNewsId(nId);
		
		commentDAO.deleteCommentsByNewsId(nId);
		
		verify(commentDAO).deleteCommentsByNewsId(nId);
		verifyNoMoreInteractions(commentDAO);
		verifyNoMoreInteractions(comment);
	}
	
	
	@Test
	public void getCommentListByNewsIdTest() throws DAOException,ServiceException {
		List <Comment> cList= mock(List.class);
		long newsId = 123L;
		long firstComment = 1L;
		long lastComment = 5L;
		

		when(commentDAO.getCommentsIdsListByNewsId(newsId, firstComment, lastComment)).thenReturn(cList);
		
		assertTrue(commentService.getCommentsIdsListByNewsId(newsId, firstComment, lastComment)==cList);
		
		verify(commentDAO).getCommentsIdsListByNewsId(newsId, firstComment, lastComment);
		verifyNoMoreInteractions(commentDAO);
		verifyNoMoreInteractions(cList);
	}
	

}
